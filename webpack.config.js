"use strict";

const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ngAnnotatePlugin = require("ng-annotate-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {

    // Mode
    mode: "development",

    // Devtool
    devtool: "eval-source-map",

    // Entry
    entry: {
        app: [
            path.resolve(__dirname, "app/app.tsx")
        ]
    },

    // Output
    output: {
        filename: "[name].bundle.js",
        publicPath: "/",
        path: path.resolve(__dirname, "dist")
    },

    // Optimization
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: "vendor",
                    chunks: "all"
                }
            }
        }
    },

    // Dev Server
    devServer: {
        historyApiFallback: true,
        contentBase: "./dist",
        port: 3001,
        hot: true
    },

    // Resolve
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"]
    },

    // Module loaders
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: [/node_modules/, /env\.js/],
                use: [
                    {
                        loader: "babel-loader",
                        query: {
                            presets: ["@babel/preset-env"]
                        }
                    }
                ]
            },
            {
                test: /\.tsx?$/,
                use: [
                    { loader: "ts-loader" }
                ]
            },
            {
                test: /\.html$/,
                use: [
                    { loader: "html-loader" }
                ]
            },
            {
                test: /\.scss$/,
                exclude: [/external\.scss/],
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" },
                    { loader: "postcss-loader" },
                    { loader: "sass-loader" }
                ]
            },
            {
                test: /external\.scss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    "css-loader",
                    "sass-loader"
                ]
            },

            {
                test: /\.css$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" }
                ]
            },
            {
                test: /\.png$/,
                use: [
                    { loader: "file-loader?name=./assets/[name].[ext]" }
                ]
            },
            {
                test: /\.gif$/,
                use: [
                    { loader: "file-loader?name=./assets/[name].[ext]" }
                ]
            },
            {
                test: /\.ico$/,
                use: [
                    { loader: "file-loader?name=./assets/[name].[ext]" }
                ]
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [
                    { loader: "url-loader?limit=10000&minetype=application/font-woff&name=./assets/[name].[ext]" }
                ]
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [
                    { loader: "file-loader?name=./assets/[name].[ext]" }
                ]
            }
        ]
    },

    // Plugins
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
            "moment": "moment",
            "window.moment": "moment",
            _: "lodash"
        }),

        // new webpack.SourceMapDevToolPlugin({
        //     filename: "[file].map"
        // }),

        new HtmlWebpackPlugin({
            title: "Maineve",
            template: "app/index.html",
            inject: "body",
            hash: false
        }),

        new MiniCssExtractPlugin({
            filename: "[name].bundle.css"
        }),

        new ngAnnotatePlugin({
            add: true
        }),

        new CopyWebpackPlugin([
            { from: "app/env.js", to: "./" }
        ]),

        new webpack.HotModuleReplacementPlugin()
    ]
};
