const pxtoremConfig = {
    rootValue: 16,
    unitPrecision: 5,
    propList: ['*'],
    selectorBlackList: [],
    replace: true,
    mediaQuery: true,
    minPixelValue: 0
};


module.exports = {
    plugins: [
        require('autoprefixer'),
        require('postcss-pxtorem')(pxtoremConfig),
        require('postcss-preset-env'),
        require('postcss-utilities'),
    ]
}
