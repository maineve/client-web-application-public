"use strict";

const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ngAnnotatePlugin = require("ng-annotate-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CompressionPlugin = require("compression-webpack-plugin");

module.exports = {

    // Mode
    mode: "production",

    // Performance
    performance: {
        hints: "warning",
        maxEntrypointSize: 1000000
    },

    // Entry
    entry: {
        app: [
            path.resolve(__dirname, "app/app.tsx")
        ]
    },

    // Output
    output: {
        filename: "[name].[hash].js",
        publicPath: "/",
        path: path.resolve(__dirname, "dist")
    },

    // Optimization
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    name: "vendor",
                    chunks: "all"
                }
            }
        }
    },

    // Resolve
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"]
    },

    // Module loaders
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: [/node_modules/, /env\.js/],
                use: [
                    {
                        loader: "babel-loader",
                        query: {
                            presets: ["@babel/preset-env"]
                        }
                    }
                ]
            },
            {
                test: /\.tsx?$/,
                use: [
                    { loader: "ts-loader" }
                ]
            },
            {
                test: /\.html$/,
                use: [
                    { loader: "html-loader" }
                ]
            },
            {
                test: /\.scss$/,
                exclude: [/external\.scss/],
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" },
                    { loader: "postcss-loader" },
                    { loader: "sass-loader" }
                ]
            },
            {
                test: /external\.scss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.css$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" }
                ]
            },
            {
                test: /\.png$/,
                use: [
                    { loader: "file-loader?name=./assets/[name].[ext]" }
                ]
            },
            {
                test: /\.gif$/,
                use: [
                    { loader: "file-loader?name=./assets/[name].[ext]" }
                ]
            },
            {
                test: /\.ico$/,
                use: [
                    { loader: "file-loader?name=./assets/[name].[ext]" }
                ]
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [
                    { loader: "url-loader?limit=10000&minetype=application/font-woff&name=./assets/[name].[ext]" }
                ]
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [
                    { loader: "file-loader?name=./assets/[name].[ext]" }
                ]
            }
        ]
    },

    // Plugins
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
            "moment": "moment",
            "window.moment": "moment",
            _: "lodash"
        }),

        new HtmlWebpackPlugin({
            title: "Maineve",
            template: "app/index.html",
            inject: "body",
            minify: {
                collapseWhitespace: false
            },
            hash: false
        }),

        new MiniCssExtractPlugin({
            filename: "[name].[hash].css",
            chunks: "all"
        }),

        new ngAnnotatePlugin({
            add: true
        }),

        new CompressionPlugin({
            filename: "[path][query]",
            algorithm: "gzip",
            test: /\.js$/,
            threshold: 0,
            minRatio: 0.8
        })
    ]
};
