export const token = localStorage.getItem("token");
export const dateFormat = "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]";
