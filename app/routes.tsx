import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { default as Login } from "./react/Anon/containers/Login";
import { default as ForgotPassword } from "./react/Anon/containers/ForgotPassword";
import { default as ChangePassword } from "./react/Anon/containers/ChangePassword";
import { default as AccountConfirmation } from "./react/Anon/containers/AccountConfirmation";
import { default as CreateAccount } from "./react/Anon/containers/CreateAccount";
import { default as Spaces } from "./react/Spaces/containers/Spaces";
import { default as Calendar } from "./react/Calendar/containers/Calendar";
import { default as People } from "./react/People/containers/People";
import { default as Subscription } from "./react/Subscription/containers/Subscription";
import { default as Locked} from "./react/Locked/containers/Locked";
import { default as NotFoundRedirect } from "./react/Generic/components/NotFoundRedirect";

export const routes = {
    login: "/",
    forgot: "/forgot",
    reset: "/reset",
    confirm: "/confirm",
    signup: "/signup",
    dashboard: "/dashboard",
    spaces: "/spaces",
    people: "/people",
    account: "/account",
    locked: "/locked",
};

export const publicRoutes = [
    routes.login,
    routes.forgot,
    routes.reset,
    routes.confirm,
    routes.signup,
];

export const adminRoutes = [
    routes.spaces,
    routes.people,
];

export const ownerRoutes = [
    routes.account,
];

export const Routes: React.FC = () => (
    <Router>
        <Switch>

            // Public Routes
            <Route path={ routes.login } component={ Login } exact={ true } />
            <Route path={ routes.forgot } component={ ForgotPassword } />
            <Route path={ routes.reset } component={ ChangePassword } />
            <Route path={ routes.confirm } component={ AccountConfirmation } />
            <Route path={ routes.signup } component={ CreateAccount } />

            // Private Routes
            <Route path={ routes.dashboard } component={ Calendar } />
            <Route path={ routes.spaces } component={ Spaces } />
            <Route path={ routes.people } component={ People } />
            <Route path={ routes.account } component={ Subscription } />
            <Route path={ routes.locked } component={ Locked } />

            // Other Routes
            <Route path="*" component={ NotFoundRedirect } />

        </Switch>
    </Router>
);
