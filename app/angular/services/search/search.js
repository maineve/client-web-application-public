'use strict';

import angular from 'angular';

import SearchService from './search.service';

export default angular
    .module('services.searchService', [])
    .service('searchService', SearchService);
