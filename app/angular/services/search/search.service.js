'use strict';

/**
 *  Reservations Service
 *

 */
export default function searchService(Restangular) {
    'ngInject';

    let searches = {};

    searches = Restangular.service('search');

    return searches;
}
