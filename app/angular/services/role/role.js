'use strict';

import angular from 'angular';

import RoleService from './role.service';

export default angular
    .module('components.role', [])
    .service('roleService', RoleService);
