'use strict';

/**
 *  Roles Service
 */
export default function roleService(Restangular) {
    'ngInject';

    let role = {};

    role = Restangular.service('roles');

    return role;
}
