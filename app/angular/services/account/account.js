'use strict';

import angular from 'angular';

import AccountService from './account.service';

export default angular
    .module('components.account', [])
    .service('accountService', AccountService);
