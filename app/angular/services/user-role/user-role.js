'use strict';

import angular from 'angular';

import UserRoleService from './user-role.service';

export default angular
    .module('shared.userRole', [])
    .service('userRoleService', UserRoleService);
