'use strict';

/**
 *  Reservations Service
 *

 */
export default function userRoleService() {
    'ngInject';

    const USER = 'user';
    const ADMIN = 'admin';
    const OWNER = 'owner';

    let role = {};

    /**
     *  User permissions
     *
     *  Hint: This grabs the user permissions based on their current account
     */
    role.getUserRole = function (user) {
        let account = {};

        account = _.find(user.users_accounts, (acc) => {
            return acc.account_id === user.current_account_id;
        });

        return account.role.name;
    };

    /**
     *  Authenticated as a user
     */
    role.is_user = function (user) {
        return role.getUserRole(user) === USER;
    };

    /**
     *  Authenticated as an administrator
     */
    role.is_admin = function (user) {
        return role.getUserRole(user) === ADMIN;
    };

    /**
     *  Authenticated as account owner
     */
    role.is_owner = function (user) {
        return role.getUserRole(user) === OWNER;
    };

    return role;
}
