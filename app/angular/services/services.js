'use strict';

import angular from 'angular';

import Account from './account/account';
import Role from './role/role';
import Search from './search/search';
import UserRole from './user-role/user-role';

export default angular
    .module('app.services', [
        Account.name,
        Role.name,
        Search.name,
        UserRole.name
    ]);
