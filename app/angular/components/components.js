'use strict';

import angular from 'angular';

import AccountForm from './account-form/account-form';
import Building from './building/building';
import BuildingForm from './building-form/building-form';
import Dashboard from './dashboard/dashboard';
import Invitation from './invitation/invitation';
import Reservation from './reservation/reservation';
import Room from './room/room';
import RoomForm from './room-form/room-form';
import Subscription from './subscription/subscription';
import SubscriptionForm from './subscription-form/subscription-form';
import User from './user/user';

export default angular
    .module('app.components', [
        AccountForm.name,
        Building.name,
        BuildingForm.name,
        Dashboard.name,
        Invitation.name,
        Reservation.name,
        Room.name,
        RoomForm.name,
        Subscription.name,
        SubscriptionForm.name,
        User.name,
    ]);
