'use strict';

import angular from 'angular';

import RoomFormController from './room-form.controller';

import "../../../assets/images/icons/building.svg";

export default angular
    .module('components.roomForm', [])
    .controller('roomFormController', RoomFormController);
