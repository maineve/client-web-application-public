'use strict';

/**
 *  Rooms Dialog Controller
 */
export default function roomFormController($rootScope, roomService, Restangular, roomData, buildingData, title) {
    'ngInject';

    let vm = this;

    vm.isLoading = false;
    vm.room = (!_.isEmpty(roomData)) ? angular.copy(roomData) : {};
    vm.room.building_id = buildingData.id;
    vm.messages = null;
    vm.title = title;
    vm.buttonText = 'Save';

    vm.newRoom = function () {
        vm.isLoading = true;
        roomService
            .post(vm.room)
            .then(function (response) {
                $rootScope.$broadcast('roomAdded', response);
                vm.isLoading = false;
            }, function (error) {
                vm.messages = { info: error.data.errors, type: error.data.success, button: true };
            });
    };

    vm.editRoom = function () {
        vm.isLoading = true;
        Restangular
            .one('rooms/' + vm.room.id)
            .customPUT(vm.room)
            .then(function (response) {
                $rootScope.$broadcast('roomUpdated', response);
                vm.isLoading = false;
            }, function (error) {
                vm.messages = { info: error.data.errors, type: error.data.success, button: true };
            });
    };

    vm.submitForm = function () {
        (_.isEmpty(roomData)) ? vm.newRoom() : vm.editRoom();
    };

    vm.isNewRoom = function () {
        return _.isEmpty(roomData);
    }
}
