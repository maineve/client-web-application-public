'use strict';

import accountFormTemplate from '../account-form/account-form.template.html';
import subscriptionFormTemplate from '../subscription-form/subscription-form.template.html';
import confirmTemplate from '../confirm/confirm.template.html';

/**
 *  Subscription controller
 */
export default function subscriptionController(
    $scope,
    $rootScope,
    $http,
    accountService,
    roomService,
    subscriptionService,
    ngDialog
) {
    'ngInject';

    let vm = this;

    /**
     *  Loading flag
     */
    vm.isLoading = false;

    /**
     *  Setup model for subscriptions
     */
    vm.initiated = false;
    vm.dialog = {};
    vm.timezones = [];
    vm.account = {};
    vm.profile = {};
    vm.subService = subscriptionService;

    /**
     * Initiate subscription data
     */
    vm.initiate = function() {

        const profile = JSON.parse(localStorage.getItem("profile"));

        vm.profile = profile;
        $rootScope.user = profile;

        $http
            .get(`${ __env.apiUrl }/accounts/${ vm.profile.current_account_id }`)
            .then(response => {
                vm.account = response.data.data;
            })
            .finally(() => {
                vm.setInitiated();
            });
    };

    vm.getInitiated = function() {
        return vm.initiated;
    };

    vm.setInitiated = function() {
        vm.initiated = !vm.initiated;
    };

    /**
     *  Add a new subscription.
     */
    vm.addSubscription = function () {
        vm.dialog = ngDialog.open({
            template: subscriptionFormTemplate,
            plain: true,
            controller: 'subscriptionFormController',
            controllerAs: 'subscriptionFormCtrl',
            resolve: {
                accountData: function () {
                    'ngInject';
                    return vm.account;
                },
                roomData: function () {
                    'ngInject';
                    return roomService.getList();
                }
            }
        });
    };

    /**
     *  Re-activate subscription
     */
    vm.reactivateSubscription = function () {
        vm.isLoading = true;

        accountService
            .one(vm.profile.current_account_id)
            .customPUT({}, 'subscription')
            .then((response) => {
                vm.account = response.plain();
            }, (error) => {
                console.log(error);
            })
            .finally(() => {
                vm.isLoading = false;
            });
    };

    /**
     *  Cancel account subscription.
     *
     *  Note: Account will be active until the end of the current paid billing
     *        period.
     */
    vm.cancelSubscription = function () {
        $scope.confirmButtonText = 'Yes, Cancel Account';
        $scope.confirmText = 'Are you sure you would like to cancel your subscription? Your account will remain active until the end of the current billing period.';

        ngDialog.openConfirm({
            scope: $scope,
            template: confirmTemplate,
            plain: true
        }).then(() => {
            vm.isLoading = true;
            return accountService.one(vm.profile.current_account_id).customDELETE('subscription');
        }).then((response) => {
            vm.account = response.plain();
        }).catch(() => {
            console.log('Error cancelling account.');
        }).finally(() => {
            vm.isLoading = false;
        });
    };

    /**
     *  Edit Account Info.
     */
    vm.editAccountInfo = function () {
        vm.dialog = ngDialog.open({
            template: accountFormTemplate,
            plain: true,
            controller: 'accountFormController',
            controllerAs: 'accountFormCtrl',
            resolve: {
                accountData: function () {
                    'ngInject';
                    return vm.account;
                }
            }
        });
    };

    /**
     *  Close subscription box when successful
     */
    $scope.$on('subscriptionUpdated', (ev, data) => {
        vm.account = data.plain();
        vm.dialog.close();
    });

    /**
     *  Close dialog box when account is updated
     */
    $scope.$on('accountUpdated', (ev, data) => {
        vm.account.account = _.extend(vm.account.account, data.plain());
        vm.dialog.close();
    });
}
