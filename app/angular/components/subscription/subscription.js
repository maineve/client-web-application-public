'use strict';

import angular from 'angular';

import SubscriptionService from './subscription.service';
import SubscriptionController from './subscription.controller';

export default angular
    .module('components.subscription', [])
    .service('subscriptionService', SubscriptionService)
    .controller('subscriptionController', SubscriptionController);
