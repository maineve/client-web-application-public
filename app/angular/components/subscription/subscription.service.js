class SubscriptionService {
    constructor() { }

    /**
     *  Is the account active
     *
     *  Return: Bool
     */
    active(account) {
        return moment(account.account.active_until) > moment();
    }

    /**
     *  Is the account currently in a trial period
     *
     *  Return: Bool
     */
    trialing(account) {
        return this.active(account) && !this.hasSub(account);
    }

    /**
     *  Has the accounts trial expired and they haven't setup a
     *  new subscription
     *
     *  Return: Bool
     */
    trialExpired(account) {
        return !this.active(account) && !this.hasSub(account);
    }

    /**
     *  Has a subscription been cancelled but is active until
     *  the end of the current billing cycle.
     *
     *  Return: Bool
     */
    canceled(account) {
        return this.getSub(account)[0].cancel_at_period_end;
    }

    /**
     *  Get the subscription information for the current account
     *
     *  Return: Array
     */
    getSub(account) {
        return account.stripe.subscriptions.data;
    }

    /**
     *  Does the current account have a subscription
     *
     *  Return: Bool
     */
    hasSub(account) {
        return account.stripe.subscriptions.total_count > 0;
    }
}

export default SubscriptionService;
