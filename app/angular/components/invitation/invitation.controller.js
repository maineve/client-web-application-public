'use strict';

/**
 *  Invitations Controller
 */
export default function invitationController($rootScope, $timeout, Restangular) {
    'ngInject';

    let vm = this;

    vm.isLoading = false;

    vm.messageText = '';
    vm.messageClasses = '';
    vm.messageSuccess = false;
    vm.messageShow = false;

    vm.messageInline = false;
    vm.inviteUser = {};

    vm.sendInvite = function () {
        vm.isLoading = true;

        Restangular
            .all('users/invite')
            .post(vm.inviteUser)
            .then(function (response) {
                vm.inviteUser = {};
                vm.isLoading = false;
                $rootScope.$broadcast('invitationSent');
            }, function (error) {
                vm.messageText = error.data.message;
                vm.messageClasses = 'messages--button-popup';
                vm.messageSuccess = false;
                vm.messageShow = true;
                vm.isLoading = false;
            });
    };

    vm.resendInvite = function (inviteeEmail) {
        let data = { email: inviteeEmail };

        Restangular.all('users/invite').post(data).then((response) => {
            vm.messageInline = true;
            vm.text = response.message;

            $timeout(() => {
                vm.messageInline = false;
            }, 1500);
        }, (error) => {
            console.log(error);
        });
    };

}
