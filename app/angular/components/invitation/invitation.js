'use strict';

import angular from 'angular';

import InvitationController from './invitation.controller';

import "../../../assets/images/icons/invite.svg";

export default angular
    .module('components.invitation', [])
    .controller('invitationController', InvitationController);
