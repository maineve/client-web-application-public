'use strict';

/**
 *  Buildings Controller
 */
export default function buildingFormController($rootScope, Restangular, buildingService, buildingData, title) {
    'ngInject';

    let vm = this;

    /**
     *  Setting up the Model
     */
    vm.isLoading = false;
    vm.building = (!_.isEmpty(buildingData)) ? angular.copy(buildingData) : {};
    vm.messages = null;
    vm.title = title;
    vm.buttonText = 'Save';

    /**
     *  Add new building to the database
     */
    vm.newBuilding = function () {
        vm.isLoading = true;
        buildingService
            .post(vm.building)
            .then(function (response) {
                $rootScope.$broadcast('buildingAdded', response);
                vm.isLoading = false;
            }, function (error) {
                vm.messages = { info: error.data.errors, type: error.data.success, button: true };
            });
    };

    /**
     *  Edit existing building
     */
    vm.editBuilding = function () {
        vm.isLoading = true;
        Restangular
            .one('buildings/' + vm.building.id)
            .customPUT(vm.building)
            .then(function (response) {
                $rootScope.$broadcast('buildingUpdated', response);
                vm.isLoading = false;
            }, function (error) {
                console.log(error);
            });
    };

    /**
     *  Chooses which form action to take
     */
    vm.submitForm = function () {
        (_.isEmpty(buildingData)) ? vm.newBuilding() : vm.editBuilding();
    };
}
