'use strict';

import angular from 'angular';

import BuildingFormController from './building-form.controller';

import "../../../assets/images/icons/building.svg";

export default angular
    .module('components.buildingForm', [])
    .controller('buildingFormController', BuildingFormController);
