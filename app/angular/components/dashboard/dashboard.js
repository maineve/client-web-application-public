'use strict';

import angular from 'angular';

import DashboardController from './dashboard.controller';
import DashboardService from './dashboard.service';

export default angular
    .module('components.dashboard', [])
    .controller('dashboardController', DashboardController)
    .service('dashboardService', DashboardService);
