'use strict';

/**
 *  Dashboard controller
 *
 *  @User
 */
export default function dashboardController(
    $scope,
    $http,
    dashboardService,
    reservationService,
    __env
) {

    'ngInject';

    let vm = this;

    /**
     *  Loading flag
     */
    vm.loading = false;

    /**
     *  Setting up model
     */
    vm.initiated = false;
    vm.dashboardCal = {};
    vm.dialog = {};
    vm.calEvent = {};
    vm.showReservationForm = false;
    vm.sources = [];
    vm.profile = {};
    vm.account = {};
    vm.reservations = [];
    vm.users = [];
    vm.rooms = [];
    vm.buildings = [];
    vm.apiUrl = __env.apiUrl;

    vm.selectedUser = null;
    vm.selectedRoom = null;
    vm.selectedBuilding = null;

    /**
     * Initialize Calendar Data
     */
    vm.calendarInit = function() {

        const profile = JSON.parse(localStorage.getItem("profile"));

        vm.profile = profile;
        vm.account = profile.users_accounts.find(ua => ua.account_id === profile.current_account_id).account;

        const start = moment().startOf('month').format();
        const end = moment().endOf('month').format();

        $http
            .get(`${ __env.apiUrl }/reservations?range[start]=${ start }&range[end]=${ end }`)
            .then(response => {
                vm.reservations = response.data.data;
                vm.sources = dashboardService.fullCalify(vm.reservations);

                return $http.get(`${ __env.apiUrl }/buildings`);
            })
            .then(response => {
                vm.buildings = response.data.data;

                return $http.get(`${ __env.apiUrl }/rooms`);
            })
            .then(response => {
                vm.rooms = response.data.data;
            })
            .finally(() => {
                vm.setInitiated();
            });
    };

    vm.getInitiated = function() {
        return vm.initiated;
    };

    vm.setInitiated = function() {
        vm.initiated = !vm.initiated;
    };

    /**
     *  Open edit reservation form
     */
    vm.toggleReservationForm = function(calEvent = {}) {

        vm.showReservationForm = !vm.showReservationForm;

        if (vm.showReservationForm) {
            vm.calEvent = {
                id: typeof calEvent.id === "string" ? calEvent.original_id : calEvent.id,
                start: calEvent.start || moment().utc().startOf('hour'),
                end: calEvent.end || moment().utc().startOf('hour').add(1, 'hour'),
                recurrence_id: calEvent.recurrence_id,
                room_id: calEvent.room,
            };
        }
    };

    /**
     *  Get reservations for the current month
     */
    vm.getReservations = function() {
        reservationService
            .getList({ 'user_id': vm.selectedUser, 'room_id': vm.selectedRoom })
            .then((response) => {
                vm.sources = dashboardService.fullCalify(response);
            }, (error) => {
                console.log(error);
            });
    };

    /**
     *  Get Reservation
     */
    vm.getReservation = function(id) {
        return _.find(vm.reservations, (reservation) => {
            return reservation.id === id;
        });
    };

    /**
     *  Get Reservation
     */
    vm.getReservation = function(id) {
        return _.find(vm.reservations, (reservation) => {
            return reservation.id === id;
        });
    };

    /**
     *  Get Reservations for selected month
     */
    vm.getCurrentReservations = function() {
        let startOfMonth = vm.dashboardCal.calendar.getDate().startOf('month').format();
        let endOfMonth = vm.dashboardCal.calendar.getDate().endOf('month').format();

        reservationService
            .getList({
                'range[start]': startOfMonth,
                'range[end]': endOfMonth,
                'user_id': vm.selectedUser,
                'room_id': vm.selectedRoom
            })
            .then((response) => {
                vm.sources = dashboardService.fullCalify(response);
            }, (error) => {
                console.log(error);
            });
    };

    /**
     *  Get the token for the current users current account
     *
     *  Hint: Grabbing this for the ICS calendar export.
     */
    vm.getToken = function() {
        return _.find(vm.profile.users_accounts, (users_account) => {
            return users_account.account_id === vm.profile.current_account_id;
        });
    };

    /**
     *  Check which view the calendar is on.
     */
    vm.whichCalView = function() {
        return vm.dashboardCal.name;
    };

    /**
     *  Filter active
     */
    vm.activeFilter = function() {
        return vm.selectedUser !== null || vm.selectedRoom !== null;
    };

    /**
     *  Clear all filters
     */
    vm.clearFilters = function() {
        vm.selectedUser = null;
        vm.selectedRoom = null;
        vm.selectedBuilding = null;
        vm.getCurrentReservations();
    };

    /**
     *  When the reservation has been added
     */
    $scope.$on('reservationAdded', () => {
        if (!_.isEmpty(vm.dialog)) {
            vm.getCurrentReservations();
            vm.dialog.close();
        }
    });

    /**
     *  When the reservation has been edited
     */
    $scope.$on('reservationEdited', () => {
        if (!_.isEmpty(vm.dialog)) {
            vm.getCurrentReservations();
            vm.dialog.close();
        }
    });

    /**
     *  When reservation has been deleted
     */
    $scope.$on('reservationDeleted', () => {
        if (!_.isEmpty(vm.dialog)) {
            vm.getCurrentReservations();
            vm.dialog.close();
        }
    });

    $scope.$on('reservationsCurrentUser', (event, data) => {
        vm.selectedUser = data.id;
        vm.getCurrentReservations();
    });

    $scope.$on('newReservationBtnClick', () => {
        vm.toggleReservationForm();
    });

    $scope.$on('searchReservationClick', (event, data) => {
        vm.toggleReservationForm(data);
    });

}
