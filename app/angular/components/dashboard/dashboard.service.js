"use strict";

import { dateFormat } from "../../../app.constants";

/**
 *  Reservations Service
 */
export default function dashboardService(
    roomService,
    userService,
    reservationService
) {

    "ngInject";

    let dashboard = {};

    /**
     *  Full calendar format for events
     *
     *  Hint: The data returns from the API is not in Full Calendar format,
     *        this function goes through each even event and makes an array
     *        of objects that full calendar will understand.
     */
    dashboard.fullCalify = function(data) {
        var reservations = [];

        _.each(data, reservation => {
            reservations.push(
                dashboard.createFullCalendarSource(reservation)
            );
        });

        return reservations;
    };

    dashboard.createFullCalendarSource = function(reservation) {
        return {
            id: reservation.id || reservation.recurrence_id,
            title: reservation.title,
            start: reservation.startdate,
            end: reservation.enddate,
            original_start: reservation.startdate,
            original_end: reservation.enddate,
            room: reservation.room_id,
            original_id: reservation.original_id,
            recurrence_id: reservation.recurrence_id,
            recurrence_key: reservation.recurrence_key,
            details: reservation.details,
            repeat: dashboard.isRepeatedEvent(reservation),
            users: reservation.users,
            conflicts: reservation.conflicts
        }
    };

    dashboard.isRepeatedEvent = function(reservation) {
        return reservation.recurrence_id !== null;
    };

    dashboard.roomData = function() {
        return roomService.getList({ active: true });
    };

    dashboard.userData = function() {
        return userService.getList();
    };

    dashboard.reservationData = function(event, isNew) {
        if (isNew) {
            return {
                new: isNew,
                date: event.utc()
            };
        } else if (event.repeat) {
            return reservationService
                .one(event.original_id)
                .get()
                .then(response => {
                    return {
                        title: response.title,
                        room_id: response.room_id,
                        startdate: event.start.format(dateFormat),
                        enddate: event.end.format(dateFormat),
                        original_startdate: event.start.format(dateFormat),
                        original_enddate: event.end.format(dateFormat),
                        details: response.details,
                        users: response.users,
                        original_id: event.original_id,
                        recurrence_id: event.recurrence_id,
                        recurrence_key: event.recurrence_key,
                        recurrence: response.recurrence
                    };
                });
        } else {
            return reservationService
                .one(event.id)
                .get()
                .then(response => {
                    if (response.original_id) {
                        return reservationService.one(response.original_id).get().then(originalReservation => {
                            response.recurrence = originalReservation.recurrence;
                            return response.plain();
                        });
                    } else {
                        return response.plain();
                    }
                });
        }
    };

    return dashboard;
}
