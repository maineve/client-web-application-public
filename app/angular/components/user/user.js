'use strict';

import angular from 'angular';

import UserController from './user.controller';
import UserService from './user.service';

export default angular
    .module('components.user', [])
    .controller('userController', UserController)
    .service('userService', UserService);
