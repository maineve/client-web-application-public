'use strict';

/**
 *  Users Service
 */
export default function userService($rootScope, Restangular) {
    'ngInject';

    let user = {};

    user = Restangular.service('users');

    /**
     *  Return a list of the active users
     */
    user.activeUsers = function (users) {
        return _.filter(users, function (user) {
            return user.user_id !== null;
        });
    };

    /**
     *  Grab the user profile
     */
    user.profile = function () {
        return $rootScope.user.profile;
    };

    return user;
}
