'use strict';

import invitationFormTemplate from '../invitation/invitation.template.html';
import confirmTemplate from '../confirm/confirm.template.html';

/**
 *  Users Controller
 */
export default function userController(
    $scope,
    $http,
    userService,
    userRoleService,
    ngDialog
) {

    'ngInject';

    let vm = this;

    /**
     *  Setup model for users list
     */
    vm.initiated = false;
    vm.users = [];
    vm.roles = [];
    vm.currentUser = {};
    vm.userRole = userRoleService;

    /**
     * Initiate user data
     */
    vm.initiate = function() {

        const profile = JSON.parse(localStorage.getItem("profile"));

        vm.currentUser = profile;

        userService
            .getList()
            .then(response => {
                vm.users = response;
                return $http.get(`${ __env.apiUrl }/roles`);
            })
            .then(response => {
                vm.roles = response.data.data;
            })
            .finally(() => {
                vm.setInitiated();
            });
    };

    vm.getInitiated = function() {
        return vm.initiated;
    };

    vm.setInitiated = function() {
        vm.initiated = !vm.initiated;
    };

    /**
     *  Update the users role
     */
    vm.updateUserRole = function (user, roleId) {

        user['role_id'] = roleId;

        user
            .put()
            .then(() => {
                return userService.getList();
            })
            .then((response) => {
                vm.users = response;
            })
            .catch((error) => {
                console.log(error);
            });
    };

    /**
     *  Open user invitation form
     *
     *  Hint: Fires ngDialog to open a invitation email form.
     */
    vm.openInviteForm = function () {
        vm.dialog = ngDialog.open({
            template: invitationFormTemplate,
            plain: true,
            controller: 'invitationController',
            controllerAs: 'invitationCtrl'
        });
    };

    /**
     *  Delete the reservation
     */
    vm.deleteUser = function (id) {

        $scope.confirmButtonText = 'Remove User';
        $scope.confirmText = 'Are you sure you would like to remove this user from the account? Use the invite form to invite them back later.';

        ngDialog.openConfirm({
            scope: $scope,
            template: confirmTemplate,
            plain: true
        }).then(() => {
            return userService.one(id).remove();
        }).then(() => {
            return userService.getList();
        }).then((response) => {
            vm.users = response;
        });
    };

    /**
     *  Are there any pending invitations?
     */
    vm.checkPendingInvitations = function () {

        let hasActiveInvites = false;

        vm.users.map((user) => {
            if (user.user_id === null) {
                hasActiveInvites = true;
            }
        });

        return hasActiveInvites;
    };

    /**
     *  Once invitation has been sent
     */
    $scope.$on('invitationSent', (ev) => {

        vm.dialog.close();

        userService.getList().then((response) => {
            vm.users = response;
        }, (error) => {
            console.log(error);
        });
    });
}
