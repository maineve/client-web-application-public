'use strict';

import buildingFormTemplate from '../building-form/building-form.template.html';
import confirmTemplate from '../confirm/confirm.template.html';

/**
 *  Buildings Controller
 */
export default function buildingController($scope, $rootScope, $http, ngDialog, buildingService) {
    'ngInject';

    let vm = this;

    /**
     *  Setup our Model
     */
    vm.initiated = false;
    vm.buildings = [];
    vm.dialog = {};

    /**
     *  Get Building
     */
    vm.spacesInit = function () {
        $http
            .get(__env.apiUrl + "/buildings")
            .then(response => {
                vm.buildings = response.data.data;
            })
            .finally(() => {
                vm.setInitiated();
            });
    };

    vm.getInitiated = function() {
        return vm.initiated;
    };

    vm.setInitiated = function() {
        vm.initiated = !vm.initiated;
    };

    /**
     *  Open dialog box for new building
     */
    vm.newBuildingForm = function () {
        vm.dialog = ngDialog.open({
            template: buildingFormTemplate,
            plain: true,
            controller: 'buildingFormController',
            controllerAs: 'buildingFormCtrl',
            resolve: {
                buildingData: function () {
                    'ngInject';
                    return {};
                },
                title: function () {
                    'ngInject';
                    return 'Add new building';
                }
            }
        });
    };

    /**
     *  Open dialog box for editing building
     */
    vm.editBuildingForm = function (id) {
        vm.dialog = ngDialog.open({
            template: buildingFormTemplate,
            plain: true,
            controller: 'buildingFormController',
            controllerAs: 'buildingFormCtrl',
            resolve: {
                buildingData: function () {
                    'ngInject';
                    return _.find(vm.buildings, { id: id });
                },
                title: function () {
                    'ngInject';
                    let buildingName = _.find(vm.buildings, { id: id });
                    return 'Edit building: ' + buildingName.name;
                }
            }
        });
    };

    /**
     *  Delete building
     *
     *  Note: Deleting a building will also deleted all associated rooms
     *        and reservations.
     */
    vm.deleteBuilding = function (id) {
        $scope.confirmButtonText = 'Yes, Delete';
        $scope.confirmText = 'Are you sure you would like to delete this building? Deleting this building will also delete all of its rooms and associated bookings. This action cannot be undone.';

        ngDialog.openConfirm({
            scope: $scope,
            template: confirmTemplate,
            plain: true,
        }).then(() => {
            vm.buildings = _.without(vm.buildings, _.find(vm.buildings, { id: id }));
            return buildingService.one(id).remove();
        }).then((response) => {
            $rootScope.$broadcast('buildingDeleted', response);
        }).catch(() => {
            console.log('Cancelled');
        });
    };

    /**
     *  Actions to take when building is added.
     */
    $scope.$on('buildingAdded', function (event, building) {
        vm.dialog.close();
        vm.buildings.push(building);
    });

    /**
     *  Actions to take when building is updated.
     */
    $scope.$on('buildingUpdated', function (event, building) {
        vm.dialog.close();
        vm.buildings = _.without(vm.buildings, _.find(vm.buildings, { id: building.id }));
        vm.buildings.push(building);
    });

    /**
     *  Actions to take when room is added.
     */
    $scope.$on('roomAdded', function (event, room) {
        ngDialog.close(ngDialog.getOpenDialogs()[0]);
        vm.building = _.find(vm.buildings, { id: room.building_id });
        vm.building.rooms.push(room);
    });

    /**
     *  Actions to take when room is updated.
     */
    $scope.$on('roomUpdated', function (event, room) {
        ngDialog.close(ngDialog.getOpenDialogs()[0]);
        vm.building = _.find(vm.buildings, { id: room.building_id });
        vm.building.rooms = _.without(vm.building.rooms, _.find(vm.building.rooms, { id: room.id }));
        vm.building.rooms.push(room);
    });

    /**
     *  Actions to take when room is deleted.
     */
    $scope.$on('roomDeleted', function (event, room) {
        vm.building = _.find(vm.buildings, { id: room.building_id });
        vm.building.rooms = _.without(vm.building.rooms, _.find(vm.building.rooms, { id: room.room_id }));
    });
}
