'use strict';

/**
 *  Buildings Service
 */
export default function buildingService(Restangular) {
    'ngInject';

    let building;

    building = Restangular.service('buildings');

    return building;
}
