'use strict';

import angular from 'angular';

import BuildingController from './building.controller';
import BuildingService from './building.service';

export default angular
    .module('components.building', [])
    .controller('buildingController', BuildingController)
    .service('buildingService', BuildingService);
