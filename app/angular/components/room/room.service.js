'use strict';

import moment from 'moment';

/**
 *  Rooms Service
 */
export default function roomService(Restangular) {
    'ngInject';

    let room = {};

    room = Restangular.service('rooms');

    /**
     *  Room free for booking
     *
     *  Hint: This takes the room times for each booking for a room and checks
     *        it against the reservation someone is trying to create or edit so
     *        a room doesn't become double booked.
     */
    room.isRoomFree = function (start, end, others) {
        let startTime = moment.isMoment(start) ? start : moment(start);
        let endTime = moment.isMoment(end) ? end : moment(end);
        let reservationRange = moment.range(startTime.seconds(0), endTime.seconds(0));
        let roomFree = true;

        for (var i = 0; i < others.length; i++) {
            let otherRange = moment.range(moment(others[i].startdate).add(1, 'seconds'), moment(others[i].enddate).subtract(1, 'seconds'));
            if (reservationRange.overlaps(otherRange)) roomFree = false;
        }

        return roomFree;
    };

    return room;
}
