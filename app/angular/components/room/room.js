'use strict';

import angular from 'angular';

import RoomController from './room.controller';
import RoomService from './room.service';

export default angular
    .module('components.room', [])
    .controller('roomController', RoomController)
    .service('roomService', RoomService);
