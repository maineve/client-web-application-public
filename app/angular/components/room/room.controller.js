'use strict';

import roomFormTemplate from '../room-form/room-form.template.html';
import confirmTemplate from '../confirm/confirm.template.html';

/**
 *  Rooms Controller
 */
export default function roomController($scope, $rootScope, roomService, ngDialog) {
    'ngInject';

    let vm = this;

    vm.roomDialog = {};

    /**
     *  Open a new room form
     */
    vm.newRoomForm = function (buildingData) {
        vm.roomDialog = ngDialog.open({
            template: roomFormTemplate,
            plain: true,
            controller: 'roomFormController',
            controllerAs: 'roomFormCtrl',
            resolve: {
                roomData: function () {
                    'ngInject';
                    return {};
                },
                buildingData: function () {
                    'ngInject';
                    return buildingData;
                },
                title: function () {
                    'ngInject';
                    return 'Add new space';
                }
            }
        });
    };

    /**
     *  Edit an existing room
     */
    vm.editRoomForm = function (roomData) {
        vm.roomDialog = ngDialog.open({
            template: roomFormTemplate,
            plain: true,
            controller: 'roomFormController',
            controllerAs: 'roomFormCtrl',
            resolve: {
                roomData: function () {
                    'ngInject';
                    return roomData;
                },
                buildingData: function () {
                    'ngInject';
                    return {};
                },
                title: function () {
                    'ngInject';
                    return 'Edit space: ' + roomData.name;
                }
            }
        });
    };

    /**
     *  Toggle a room active or inactive
     */
    vm.toggleRoomActive = function (roomId) {
        roomService
            .one(roomId)
            .get()
            .then((response) => {
                response.active = (response.active) ? false : true;
                return response.save();
            }).then((response) => {
                $rootScope.$broadcast('roomUpdated', response);
            }).catch((error) => {
                console.log(error);
            });
    };

    /**
     *  Delete a room
     */
    vm.deleteRoom = function (roomId, buildingId) {
        $scope.confirmButtonText = 'Yes, Delete';
        $scope.confirmText = 'Are you sure you would like to delete this room? All bookings for this room will also be deleted. This action cannot be undone.';

        ngDialog.openConfirm({
            scope: $scope,
            template: confirmTemplate,
            plain: true
        }).then(() => {
            return roomService.one(roomId).remove();
        }).then((response) => {
            let details = {
                building_id: buildingId,
                room_id: roomId
            };
            $rootScope.$broadcast('roomDeleted', details);
        }).catch(() => {
            console.log('Cancelled');
        });
    };
}
