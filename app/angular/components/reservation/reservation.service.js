'use strict';

/**
 *  Reservations Service
 */
export default function reservationService(Restangular) {
    'ngInject';

    let reservation = {};

    reservation = Restangular.service('reservations');

    return reservation;
}
