'use strict';

import angular from 'angular';
import ReservationService from './reservation.service';

export default angular
    .module('components.reservation', [])
    .service('reservationService', ReservationService);
