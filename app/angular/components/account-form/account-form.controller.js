'use strict';

/**
 *  Buildings Controller
 */
export default function accountFormController($rootScope, accountService, accountData) {
    'ngInject';

    let vm = this;

    /**
     *  Setting up the Model
     */
    vm.account = (!_.isEmpty(accountData.account)) ? angular.copy(accountData.account) : {};
    vm.messages = null;
    vm.isLoading = false;
    vm.timezones = moment.tz.names();

    /**
     *  Edit existing building
     */
    vm.updateAccount = function () {
        vm.isLoading = true;
        accountService
            .one(vm.account.id)
            .customPUT(vm.account)
            .then((response) => {
                $rootScope.$broadcast('accountUpdated', response);
            }, (error) => {
                console.log(error);
            })
            .finally(() => {
                vm.isLoading = false;
            });
    };
}
