'use strict';

import angular from 'angular';

import AccountFormController from './account-form.controller';

import "../../../assets/images/icons/notepad.svg";

export default angular
    .module('components.accountForm', [])
    .controller('accountFormController', AccountFormController);
