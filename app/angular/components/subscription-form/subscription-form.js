'use strict';

import angular from 'angular';

import SubscriptionFormService from './subscription-form.service';
import SubscriptionFormController from './subscription-form.controller';

import "../../../assets/images/icons/mastercard.svg";

export default angular
    .module('components.subscriptionForm', [])
    .service('subscriptionFormService', SubscriptionFormService)
    .controller('subscriptionFormController', SubscriptionFormController);
