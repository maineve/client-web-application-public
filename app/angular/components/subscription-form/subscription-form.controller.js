'use strict';

/**
 *  Subscriptions Form controller
 */
export default function subscriptionFormController(
    $scope,
    $rootScope,
    accountService,
    subscriptionService,
    accountData,
    roomData
) {

    'ngInject';

    let vm = this;

    vm.isLoading = false;
    vm.subService = subscriptionService;

    /**
     *  Setting up message model
     */
    vm.messageText = '';
    vm.messageClasses = '';
    vm.messageSuccess = false;
    vm.messageShow = false;

    vm.account = accountData;
    vm.rooms = roomData;
    vm.buttonText = !subscriptionService.hasSub(vm.account) ? 'Setup Subscription' : 'Update Credit Card';

    /**
     *  Setting up the model
     *
     *  Note: As noted below, Angular Payments doesn't support controllerAs so we're
     *        using the normal $scope syntax here.
     */
    $scope.number = null;
    $scope.expiry = null;
    $scope.cvc = null;

    /**
     *  Process payment for Stripe.
     *
     *  Hint: Angular Payments library is shitty and will need to be replaced in
     *        the future.  It does not support controllerAs and is generally an
     *        all around pain in the ass.
     */
    $scope.processPayment = function (status, stripe) {

        if (stripe.error) {
            vm.messageText = "There was an error with your card details.  Please ensure all fields are properly filled in.";
            vm.messageClasses = subscriptionService.hasSub(vm.account) ? 'messages--button-popup' : 'messages--button-popup-sub';
            vm.messageSuccess = false;
            vm.messageShow = true;
            vm.isLoading = false;
        } else {
            if (!subscriptionService.hasSub(vm.account)) {
                accountService
                    .one($rootScope.user.current_account_id)
                    .customPOST({ token: stripe.id, name: vm.cardholder }, 'subscription')
                    .then((response) => {
                        $scope.number = null;
                        $scope.expiry = null;
                        $scope.cvc = null;
                        $rootScope.$broadcast('subscriptionUpdated', response);
                    })
                    .catch((error) => {
                        vm.messageText = error.data.message;
                        vm.messageClasses = subscriptionService.hasSub(vm.account) ? 'messages--button-popup' : 'messages--button-popup-sub';
                        vm.messageSuccess = false;
                        vm.messageShow = true;
                    })
                    .finally(() => {
                        vm.isLoading = false;
                    });
            } else {
                accountService
                    .one($rootScope.user.current_account_id)
                    .customPUT({ token: stripe.id, name: vm.cardholder }, 'subscription')
                    .then((response) => {
                        $scope.number = null;
                        $scope.expiry = null;
                        $scope.cvc = null;
                        $rootScope.$broadcast('subscriptionUpdated', response);
                    })
                    .catch((error) => {
                        vm.messageText = error.data.message;
                        vm.messageClasses = subscriptionService.hasSub(vm.account) ? 'messages--button-popup' : 'messages--button-popup-sub';
                        vm.messageSuccess = false;
                        vm.messageShow = true;
                    })
                    .finally(() => {
                        vm.isLoading = false;
                    });
            }
        }
    };

    /**
     *  Show the wide modal or regular modal for credit card form
     */
    vm.wideForm = () => {
        const MOD = 'reservation';
        let account = vm.account;
        if (!subscriptionService.hasSub(account)) {
            return MOD;
        }
    };

    /**
     * Shows the loader for the form button
     */
    vm.showLoader = () => {
        vm.isLoading = true;
    };
}
