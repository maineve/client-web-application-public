'use strict';

import DatepickerTemplate from './datepicker.template.html';

/**
 *  MVE Date Picker directive
 */
export default function mveDatePicker($timeout) {
    'ngInject';

    var directive = {
        restrict: 'A',
        replace: true,
        scope: {
            value: '=mveDatePicker',
            disable: '=disable',
            inputlabel: '@label',
            inputtype: '@type',
            inputid: '@ident',
            inputclasses: '@classes',
            dateFormat: '@dateFormat'
        },
        template: DatepickerTemplate,
        controller: ['$scope', function ($scope) {
            $scope.eventSources = [];
        }],
        link: linkFn
    };

    return directive;

    function linkFn(scope, element, attrs) {

        const DEFAULT_DATE_FORMAT = 'MMMM DD, YYYY [at] h:mm a';

        if (attrs.autofocus) {
            element.find('input').focus();
        }

        /**
         *  Check if format is given, if not define a default format
         */
        if (!scope.dateFormat) {
            scope.dateFormat = DEFAULT_DATE_FORMAT;
        }

        /**
         *  Padding time with leading zero
         *
         *  Hint: Pads the time with leading zero so the
         *        formatting looks correct.
         */
        var pad = function (n) {
            return (n < 10) ? ("0" + n) : n;
        }

        /**
         *  Range function
         *
         *  Hint: Returns an array of values between two numbers
         */
        var range = function (min, max, step, padded) {
            step = step || 1;
            var range = [];
            for (var i = min; i <= max; i += step) {
                if (padded) {
                    range.push({ val: i, output: pad(i) });
                } else {
                    range.push({ val: i, output: i });
                }
            }
            return range;
        };

        /**
         *  Switch to twelve hour
         *
         *  Hint: Switches 24H time to 12H time
         */
        var switchTwelveHour = function (obj) {
            return parseInt(moment(obj.hours, ['H']).format('h'));
        };

        /**
         *  Datepicker Calendar configuration
         */
        var fullCalendarConfig = function (scope) {
            return {
                calendar: {
                    contentHeight: 'auto',
                    aspectRatio: 1,
                    fixedWeekCount: false,
                    header: false,
                    dayClick: function (date, jsEvent, view) {
                        scope.valueObj.date = date.toObject().date;
                        scope.valueObj.months = date.toObject().months;
                        scope.valueObj.years = date.toObject().years;

                        scope.saveDate();

                        $(this)
                            .closest('.fc-basic-view')
                            .find('.fc-day')
                            .removeClass('fc-day-active');
                        $(this)
                            .addClass('fc-day-active');
                    },
                    dayRender: function (date, cell) {
                        var format = 'MMMM-DD-YYYY';
                        if (moment(scope.valueObj).format(format) === date.format(format)) {
                            cell
                                .addClass('fc-day-active');
                        }
                    },
                    viewRender: function (view, element) {
                        scope.datePickerCalendar = view;
                    }
                }
            };
        };

        scope.datePickerCalendar = {};
        scope.datepickerConfig = fullCalendarConfig(scope);

        scope.hours = range(1, 12, 1, true);
        scope.minutes = range(0, 59, 15, true);

        scope.valueObj = moment(scope.value).toObject();
        scope.value = moment(scope.valueObj);
        scope.display = scope.value.clone().utc().format(scope.dateFormat);
        scope.ofDay = moment(scope.valueObj.hours, ['H']).format('a');
        scope.valueObj.hours = switchTwelveHour(scope.valueObj);

        scope.saveDate = function () {
            scope.valueObj.hours = moment(scope.valueObj.hours + scope.ofDay, ['ha']).format('H');
            scope.value = moment(scope.valueObj);
            scope.display = scope.value.clone().utc().format(scope.dateFormat);
            scope.valueObj.hours = switchTwelveHour(scope.valueObj);
        };

        scope.$watch('value', function (n, o) {
            if (n !== o) {
                scope.valueObj = moment(n, DEFAULT_DATE_FORMAT).toObject();
                scope.ofDay = moment(scope.valueObj.hours, ['H']).format('a');
                scope.valueObj.hours = switchTwelveHour(scope.valueObj);
                scope.display = scope.value.clone().utc().format(scope.dateFormat);

                $timeout(() => {
                    element.find('select').trigger('change');
                });
            }
        });
    }
}
