'use strict';

import angular from 'angular';

import DatepickerDirective from './datepicker.directive';

export default angular
    .module('shared.datepicker', [])
    .directive('mveDatePicker', DatepickerDirective);
