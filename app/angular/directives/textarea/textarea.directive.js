'use strict';

import TextareaTemplate from './textarea.template.html';

/**
 *  MVE Textarea Directive
 */
export default function mveTextarea() {
    'ngInject';

    return {
        restrict: 'A',
        replace: true,
        scope: {
            value: '=mveTextarea',
            disable: '=disable',
            inputlabel: '@label',
            inputtype: '@type',
            inputid: '@ident',
            inputclasses: '@classes'
        },
        template: TextareaTemplate,
        link: function (scope, element, attrs) {
            if (attrs.autofocus) {
                $(element).find('input').focus();
            }
        }
    };
}
