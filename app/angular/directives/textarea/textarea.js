'use strict';

import angular from 'angular';

import TextareaDirective from './textarea.directive';

export default angular
    .module('shared.textarea', [])
    .directive('mveTextarea', TextareaDirective);
