'use strict';

import FormsWrapperTemplate from './forms-wrapper.template.html';

/**
 *  MVE Form Wrapper Directive
 */
export default function mveFormWrapper() {
    'ngInject';

    var directive = {
        restrict: 'A',
        transclude: true,
        template: FormsWrapperTemplate,
        link: linkFn
    };

    return directive;

    function linkFn(scope, element, attrs) {
        var button = element.find('button[type=submit]');
        var buttonText = button.text();

        scope.formName = attrs.mveFormWrapper;

        scope.$watch(attrs.loadingFlag, function (newValue, oldValue) {
            if (newValue) {
                button.text(attrs.loadingText);
            } else {
                if (button.text() !== buttonText) {
                    button.text(buttonText);
                }
            }
        });
    }
}
