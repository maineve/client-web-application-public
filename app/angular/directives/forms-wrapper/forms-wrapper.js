'use strict';

import angular from 'angular';

import FormsWrapperDirective from './forms-wrapper.directive';

export default angular
    .module('shared.formsWrapper', [])
    .directive('mveFormWrapper', FormsWrapperDirective);
