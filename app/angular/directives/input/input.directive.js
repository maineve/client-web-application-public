'use strict';

import inputTemplate from './input.template.html';

/**
 *  MVE Input Directive
 */
export default function mveInput() {
    'ngInject';

    var directive = {
        restrict: 'A',
        replace: true,
        require: '?^form',
        scope: {
            value: '=mveInput',
            disable: '=disable',
            inputlabel: '@label',
            inputtype: '@type',
            inputid: '@ident',
            placeholder: '@placeholder',
            inputclasses: '@classes',
            error: '@error',
            focus: '=autofocus',
            required: '=required'
        },
        template: inputTemplate,
        link: linkFn
    };

    return directive;

    function linkFn(scope, element, attrs, formCtrl) {
        if (scope.focus) {
            $(element).find('input').focus();
        }
    }
}
