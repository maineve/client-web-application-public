'use strict';

import angular from 'angular';

import inputDirective from './input.directive';

export default angular
    .module('shared.input', [])
    .directive('mveInput', inputDirective);
