'use strict';

import angular from 'angular';

import CalendarDirective from './calendar.directive';
import uiCalendar from 'angular-ui-calendar';

export default angular
    .module('components.calendar', [
        'ui.calendar'
    ])
    .directive('mveDashboardCalendar', CalendarDirective);
