"use strict";

import CalendarTemplate from "./calendar.template.html";

/**
 *  Calendar directive
 */
export default function mveDashboardCalendar($window, reservationService, dashboardService, Restangular) {

    "ngInject";

    /**
     *  Controller function for Directive
     */
    const controller = function() {
        "ngInject";

        let vm = this;

        vm.eventSources = [];
    };

    /**
     *  Link Function for Directive
     */
    const link = (scope, element, attrs, calendarCtrl) => {

        const viewRender = (view, element) => {
            calendarCtrl.calendar = view;
        };

        const windowResize = (view) => {
            calendarCtrl.calendar = view;
            calendarCtrl.calendar.calendar.option("height", $window.innerHeight - 100);
        };

        const eventRender = (event, element, view) => {

            element.find(".fc-content").append(element.find(".fc-time"));

            element.prepend(
                `<span class="fc-drag fas fa-ellipsis-v"></span>`
            );

            element.append(
                `<div class="fc-event-loader ball-pulse">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>`
            );

            if (event.conflicts) {
                element.addClass(`fc-conflict`);
            }
        };

        const eventClick = (calEvent, jsEvent, view) => {
            calendarCtrl.toggleReservationForm({ calEvent });
        };

        const eventDrop = (event, delta, revertFunc, jsEvent, ui, view) => {

            event.className.push("fc-loading");

            reservationService
                .one(event.recurrence_id ? event.original_id : event.id)
                .get()
                .then(response => {
                    response.startdate = event.start;
                    response.enddate = event.end;

                    response.original_startdate = event.original_start;
                    response.original_enddate = event.original_end;

                    angular.forEach(response.users, function(value, key) {
                        response.users[key] = response.users[key].id;
                    });

                    if (event.recurrence_id === null) {
                        return response.put();
                    } else {
                        delete response.id
                        delete response.created_at
                        delete response.updated_at
                        delete response.account_id
                        delete response.user_id

                        response.recurrence = null;
                        response.recurrence_id = event.recurrence_id;
                        response.recurrence_key = event.recurrence_key;
                        response.original_id = event.original_id;

                        return Restangular.one('reservations', response.original_id).customPUT(response, `recurrences/${ response.recurrence_id }`);
                    }
                })
                .then(() => {
                    const options = {
                        'range[start]': moment(view.calendar.getDate()).startOf('month').format(),
                        'range[end]': moment(view.calendar.getDate()).endOf('month').format()
                    };

                    return reservationService.getList(options);
                })
                .then(data => {
                    calendarCtrl.eventSources[0] = dashboardService.fullCalify(data);
                })
                .catch(() => {
                    revertFunc();

                    if (event.conflicts) {
                        $(jsEvent.target.closest('.fc-event')).addClass('fc-conflict');
                    }
                })
                .finally(() => {
                    event.className.pop();
                    $(".fc-event").removeClass("fc-loading");
                });
        };

        const dayRender = (date, element) => {

            element.bind("dblclick", () => {
                const calEvent = {
                    start: moment(date),
                    end: moment(date).add(1, 'h'),
                };

                calendarCtrl.toggleReservationForm({ calEvent });

                scope.$apply();
            });

            element.bind("mouseenter", function() {
                element.addClass("fc-day-hover");
            });

            element.bind("mouseleave", function() {
                element.removeClass("fc-day-hover");
            });
        };

        const eventOverlap = (other, event) => {
            return other.room !== event.room;
        };

        let dragEvent;
        let dragDom;

        const eventDragStart = (event, jsEvent, ui, view) => {
            dragEvent = event;
            dragDom = jsEvent;

            if (event.conflicts) {
                $(jsEvent.target.closest('.fc-event')).removeClass('fc-conflict');
            }
        };

        const eventDragStop = (event, jsEvent, ui, view) => {
            if (event.start.unix() === dragEvent.start.unix() && event.conflicts) {
                $(dragDom.target.closest('.fc-event')).addClass('fc-conflict');
            }
        };

        calendarCtrl.dashboardCalendarConfig = {
            calendar: {
                height: $window.innerHeight - 100,
                handleWindowResize: true,
                fixedWeekCount: false,
                eventStartEditable: true,
                eventLimit: true,
                eventLimitClick: "week",
                timeFormat: "h:mma",
                timezone: calendarCtrl.account.timezone,
                dragOpacity: 1.0,
                header: false,
                nextDayThreshold: "00:00:00",
                viewRender,
                windowResize,
                eventRender,
                eventClick,
                eventDrop,
                dayRender,
                eventOverlap,
                eventDragStart,
                eventDragStop,
            }
        };

        scope.$watch(() => calendarCtrl.sources, (nextValue, prevValue) => {
            if (nextValue !== prevValue || !calendarCtrl.eventSources.length) {
                calendarCtrl.eventSources.splice(0, calendarCtrl.eventSources.length);
                calendarCtrl.eventSources.push(calendarCtrl.sources);
            }
        });
    };

    return {
        restrict: "A",
        scope: true,
        bindToController: {
            sources: "=mveDashboardCalendar",
            calendar: "=calendar",
            account: "=account",
            toggleReservationForm: "&toggleReservationForm"
        },
        controllerAs: "calendarCtrl",
        template: CalendarTemplate,
        controller,
        link,
    };
}
