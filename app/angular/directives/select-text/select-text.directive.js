'use strict';

/**
 *  MVE Select Two directive
 */
export default function mveSelectTwo($timeout) {
    'ngInject';

    var directive = {
        restrict: 'A',
        link: linkFn
    };

    return directive;

    function linkFn(scope, element, attrs) {

        $timeout(() => {
            element.on('click', function () {
                this.select();
            });
        });

    }
}
