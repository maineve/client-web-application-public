'use strict';

import angular from 'angular';

import SelectTextDirective from './select-text.directive';

export default angular
    .module('shared.selectText', [])
    .directive('mveSelectText', SelectTextDirective);
