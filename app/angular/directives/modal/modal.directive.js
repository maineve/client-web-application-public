'use strict';

import ModalTemplate from './modal.template.html';

/**
 *  MVE Model Directive
 */
export default function mveModal(ngDialog) {
    'ngInject';

    var directive = {
        restrict: 'A',
        transclude: true,
        scope: {
            title: '@mveModal',
            modifier: '@modifier',
            close: '@closeText',
            icon: '@icon'
        },
        template: ModalTemplate,
        link: linkFn
    };

    return directive;

    function linkFn(scope, element, attrs) {
        scope.icon = (scope.icon) ? scope.icon : 'assets/images/icons/calendar-3.svg';
        scope.titleIcon = (scope.icon) ? true : false;

        scope.cancel = function () {
            ngDialog.close(ngDialog.getOpenDialogs()[0]);
        };
    }
}
