'use strict';

import angular from 'angular';

import ModalDirective from './modal.directive';

export default angular
    .module('shared.modal', [])
    .directive('mveModal', ModalDirective);
