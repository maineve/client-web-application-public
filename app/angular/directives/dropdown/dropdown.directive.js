'use strict';

/**
 *  MVE DropDown
 */
export default function mveDropdown($document) {
    'ngInject';

    var directive = {
        restrict: 'A',
        scope: {
            menu: '@mveDropdown',
            toggle: '@mveDropdownToggle',
            close: '@mveDropdownClose'
        },
        link: linkFn
    };

    return directive;

    function linkFn(scope, element, attrs) {
        var dropmenu = element.find('.' + scope.menu);
        var showclass = scope.menu + '--show';

        element.find('.' + scope.toggle + ', .' + scope.close).on('click', function () {
            dropmenu.toggleClass(showclass);
        });

        element.find('li').on('click', function () {
            if (!$(this).hasClass('dropdown__list-item--dont-close')) {
                dropmenu.removeClass(showclass);
            }
        });

        $document.on('click', function (event) {
            if (!element[0].contains(event.target)) {
                dropmenu.removeClass(showclass);
            }

            if (event.target && event.target.dataset.dropdownClose) {
                dropmenu.removeClass(showclass);
            }
        });
    }
}
