'use strict';

import angular from 'angular';

import DropdownDirective from './dropdown.directive';

export default angular
    .module('shared.dropdown', [])
    .directive('mveDropdown', DropdownDirective);
