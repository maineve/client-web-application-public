import * as React from "react";
import { Provider } from "react-redux";
import ReactDOM from "react-dom";
import { store } from "../../../store";
import { default as Reservation } from "../../../react/Reservation/containers/Reservation";
import { Portal } from "../../../react/Generic/components/Portal";
import { Overlay } from "../../../react/Generic/components/Overlay";
import { ICalendarSource, ISearchSource } from "../../../store/Reservation/types";

/**
 *  MVE React Reservation Directive
 */
export default function mveReservation($timeout: any): any {
    "ngInject";

    const link = (scope: any, element: any, attrs: any): void => {

        const closeReservationForm = (): void => {
            scope.toggleReservationForm();
            scope.getCurrentReservations();
            scope.$apply();
        };

        $timeout(() => {

            let calendarSource: ICalendarSource | ISearchSource = {
                ...scope.calendarSource,
            };

            const ConnectedApp: React.FC = () => (
                <Provider store={ store }>
                    <Portal id="maineve-app" element="div">
                        <Overlay />
                        <Reservation
                            source={ calendarSource }
                            closeReservationForm={ closeReservationForm }
                        />
                    </Portal>
                </Provider>
            );

            const renderElement: Element = document.getElementById("reservation");

            scope.$watch("showReservationForm", () => {

                calendarSource = {
                    ...scope.calendarSource,
                };

                if (scope.showReservationForm) {
                    ReactDOM.render(
                        <ConnectedApp />,
                        renderElement,
                    );
                } else {
                    ReactDOM.unmountComponentAtNode(
                        renderElement,
                    );
                }
            });
        });
    };

    return {
        restrict: "E",
        scope: {
            calendarSource: "=calendarSource",
            showReservationForm: "=showReservationForm",
            toggleReservationForm: "&toggleReservationForm",
            getCurrentReservations: "&getCurrentReservations",
        },
        template: `<div id="reservation" />`,
        link,
    };
}
