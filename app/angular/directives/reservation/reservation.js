'use strict';

import angular from 'angular';

import ReservationDirective from './reservation.directive';

export default angular
    .module('shared.reservation', [])
    .directive('mveReservation', ReservationDirective);
