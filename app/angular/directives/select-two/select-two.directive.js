'use strict';

/**
 *  MVE Select Two directive
 */
export default function mveSelectTwo($timeout) {
    'ngInject';

    var directive = {
        restrict: 'A',
        link: linkFn
    };

    return directive;

    function linkFn(scope, element, attrs) {

        $timeout(() => {
            element.select2();
        });

        scope.$watch('selectedUser', (n, o) => {
            $timeout(() => {
                if (n !== o) {
                    element.trigger('change');
                }
            });
        });

    }
}
