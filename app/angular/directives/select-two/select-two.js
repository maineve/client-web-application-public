'use strict';

import angular from 'angular';

import SelectTwoDirective from './select-two.directive';

export default angular
    .module('shared.selectTwo', [])
    .directive('mveSelectTwo', SelectTwoDirective);
