'use strict';

import MessageTemplate from './message.template.html';

/**
 *  MVE Messages directive
 */
export default function mveMessage($document) {
    'ngInject';

    var directive = {
        restrict: 'A',
        scope: {
            classes: '@classes',
            message: '@message',
            success: '=success',
            show: '=showMessage',
            button: '=buttonMessage'
        },
        template: MessageTemplate,
        link: linkFn
    };

    return directive;

    function linkFn(scope, element, attrs) {

        $document.on('click', (event) => {
            if (scope.show && !scope.success && scope.button) {
                scope.$apply(() => {
                    scope.show = false;
                });
            }
        });

    }
}
