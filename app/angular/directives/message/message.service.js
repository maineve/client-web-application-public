class MessageService {
    constructor($timeout) {
        'ngInject';
        this.$timeout = $timeout;
    }

    setMessage(text, success, show, cssClass) {
        return {
            text,
            success,
            show,
            cssClass
        }
    }
}

export default MessageService;
