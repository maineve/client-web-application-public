'use strict';

import angular from 'angular';

import MessageDirective from './message.directive';
import MessageService from './message.service';

export default angular
    .module('shared.message', [])
    .directive('mveMessages', MessageDirective)
    .service('messageService', MessageService);
