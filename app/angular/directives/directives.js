'use strict';

import angular from 'angular';

import Calendar from './calendar/calendar';
import Datepicker from './datepicker/datepicker';
import Dropdown from './dropdown/dropdown';
import FormsWrapper from './forms-wrapper/forms-wrapper';
import Input from './input/input';
import Message from './message/message';
import Modal from './modal/modal';
import SelectText from './select-text/select-text';
import SelectTwo from './select-two/select-two';
import Textarea from './textarea/textarea';

import Reservation from './reservation/reservation';

export default angular
    .module('app.directives', [
        Calendar.name,
        Datepicker.name,
        Dropdown.name,
        FormsWrapper.name,
        Input.name,
        Message.name,
        Modal.name,
        SelectTwo.name,
        SelectText.name,
        Textarea.name,

        // React Directives
        Reservation.name,
    ]);
