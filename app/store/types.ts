import { RouteProps } from "react-router";
import { IAuthState, TAuthActions } from "./Auth/types";
import { IRoomState, TRoomActions } from "./Room/types";
import { IBuildingState, TBuildingActions } from "./Building/types";
import { IReservationState, TReservationActions } from "./Reservation/types";
import { IUserState, TUserActions } from "./User/types";
import { TSiteActions, ISiteState } from "./Site/types";
import { ISearchState, TSearchActions } from "./Search/types";

export interface IAppState {
    router: RouteProps;
    auth: IAuthState;
    room: IRoomState;
    building: IBuildingState;
    reservation: IReservationState;
    user: IUserState;
    search: ISearchState;
    site: ISiteState;
}

export type TAppActions =
    TAuthActions |
    TBuildingActions |
    TRoomActions |
    TReservationActions |
    TUserActions |
    TSearchActions |
    TSiteActions;

export const API_REQUEST = "@@maineve/API_REQUEST";

export interface IApiRequest<T, P = {}> {
    type: typeof API_REQUEST;
    meta: {
        request: T;
        success: T;
        failure: T;
        endpoint: string;
        init: RequestInit;
        baseurl?: string;
        payload?: P;
    };
}

export interface IJsonResponse<T = never> {
    status: "success" | "error";
    message?: string;
    data?: T;
}
