import { Reducer } from "redux";
import { IAuthState, TAuthActions } from "./types";

export const initState: IAuthState = {
    fetching: false,
    authenticated: null,
    success: null,
    error: null,
};

export const authReducer: Reducer<IAuthState, TAuthActions> = (state = initState, action) => {

    switch (action.type) {

        case "@@maineve/POST_VALIDATE_REQUEST":
            return {
                ...state,
            };
        case "@@maineve/POST_VALIDATE_SUCCESS":
            return {
                ...state,
                authenticated: true,
            };
        case "@@maineve/POST_VALIDATE_FAILURE":
            return {
                ...state,
                authenticated: false,
            };
        case "@@maineve/POST_LOGIN_REQUEST":
            return {
                ...state,
                error: null,
                success: null,
                fetching: true,
            };
        case "@@maineve/POST_LOGIN_SUCCESS":
            return {
                ...state,
                authenticated: true,
            };
        case "@@maineve/POST_LOGIN_FAILURE":
            return {
                ...state,
                fetching: false,
                authenticated: false,
                error: `The login credentials you entered could not be authenticated.`,
            };
        case "@@maineve/POST_CREATE_ACCOUNT_REQUEST":
            return {
                ...state,
                fetching: true,
                error: null,
                success: null,
            };
        case "@@maineve/POST_CREATE_ACCOUNT_SUCCESS":
            return {
                ...state,
                fetching: false,
                success: `Welcome to Maineve!  A confirmation email has been sent to you to complete the sign up process.`,
            };
        case "@@maineve/POST_CREATE_ACCOUNT_FAILURE":
            return {
                ...state,
                fetching: false,
                error: `An error occured creating a new account for Maineve. Please ensure your email isn't already associated with an account.`,
            };
        case "@@maineve/POST_FORGOT_PASSWORD_REQUEST":
            return {
                ...state,
                fetching: true,
                error: null,
                success: null,
            };
        case "@@maineve/POST_FORGOT_PASSWORD_SUCCESS":
            return {
                ...state,
                fetching: false,
                success: `An email has been sent to the email address provided with further instructions on how to reset your password.`,
            };
        case "@@maineve/POST_FORGOT_PASSWORD_FAILURE":
            return {
                ...state,
                fetching: false,
                error: `The email address you entered is not associated with an account.`,
            };
        case "@@maineve/PUT_CHANGE_PASSWORD_REQUEST":
            return {
                ...state,
                fetching: true,
                error: null,
                success: null,
            };
        case "@@maineve/PUT_CHANGE_PASSWORD_SUCCESS":
            return {
                ...state,
                fetching: false,
                success: `Your password has been changed, you can now login.`,
            };
        case "@@maineve/PUT_CHANGE_PASSWORD_FAILURE":
            return {
                ...state,
                fetching: false,
                error: `An error occrued changing your password.`,
            };
        case "@@maineve/PUT_UPDATE_PASSWORD_REQUEST":
            return {
                ...state,
                fetching: true,
                error: null,
                success: null,
            };
        case "@@maineve/PUT_UPDATE_PASSWORD_SUCCESS":
            return {
                ...state,
                fetching: false,
                success: `Your password has been updated.`,
            };
        case "@@maineve/PUT_UPDATE_PASSWORD_FAILURE":
            return {
                ...state,
                fetching: false,
                error: `An error occrued updating your password.`,
            };
        case "@@maineve/GET_ACCOUNT_CONFIRMATION_REQUEST":
            return {
                ...state,
                fetching: true,
                error: null,
                success: null,
            };
        case "@@maineve/GET_ACCOUNT_CONFIRMATION_SUCCESS":
            return {
                ...state,
                fetching: false,
                success: `Your account has been confirmed.  You can now login to Maineve.`,
            };
        case "@@maineve/GET_ACCOUNT_CONFIRMATION_FAILURE":
            return {
                ...state,
                fetching: false,
                error: `Account could not be confirmed.  Please get in touch with us if you need additional support.`,
            };
        case "@@maineve/PASSWORD_MATCH_ERROR":
            return {
                ...state,
                error: `The password and confirmation password entered do not match, please try entering your new password again.`,
            };
        default:
            return state;

    }

};
