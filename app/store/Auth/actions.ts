import { IApiRequest } from "../types";

import {
    TAuthActions,
    TAuthApiActions,
    ILogin,
    IForgotPassword,
    ICreateAccount,
    IChangePassword,
    IUpdatePassword,
} from "./types";

/**
 * Base URL for Auth API
 */
const baseUrlAuth: string = window["__env"].authUrl;

/**
 * Post Validate Action
 */
export const postValidateAction = (): IApiRequest<TAuthApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/POST_VALIDATE_REQUEST",
        success: "@@maineve/POST_VALIDATE_SUCCESS",
        failure: "@@maineve/POST_VALIDATE_FAILURE",
        baseurl: baseUrlAuth,
        endpoint: `/validate`,
        init: {
            method: "POST",
        },
    },
});

/**
 * Post Login Action
 */
export const postLoginAction = (credentials: ILogin): IApiRequest<TAuthApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/POST_LOGIN_REQUEST",
        success: "@@maineve/POST_LOGIN_SUCCESS",
        failure: "@@maineve/POST_LOGIN_FAILURE",
        baseurl: baseUrlAuth,
        endpoint: `/login`,
        init: {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ credential: credentials }),
        },
    },
});

/**
 * Post Create Account Action
 */
export const postCreateAccountAction = (data: ICreateAccount, token: string): IApiRequest<TAuthApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/POST_CREATE_ACCOUNT_REQUEST",
        success: "@@maineve/POST_CREATE_ACCOUNT_SUCCESS",
        failure: "@@maineve/POST_CREATE_ACCOUNT_FAILURE",
        endpoint: token ? `/profiles?token=${ token }` : `/profiles`,
        init: {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ data }),
        },
    },
});

/**
 * Post Forgot Password Action
 */
export const postForgotPasswordAction = (credential: IForgotPassword): IApiRequest<TAuthApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/POST_FORGOT_PASSWORD_REQUEST",
        success: "@@maineve/POST_FORGOT_PASSWORD_SUCCESS",
        failure: "@@maineve/POST_FORGOT_PASSWORD_FAILURE",
        baseurl: baseUrlAuth,
        endpoint: "/password",
        init: {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ credential }),
        },
    },
});

/**
 * Post Change Password Action
 */
export const putChangePasswordAction = (credential: IChangePassword): IApiRequest<TAuthApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/PUT_CHANGE_PASSWORD_REQUEST",
        success: "@@maineve/PUT_CHANGE_PASSWORD_SUCCESS",
        failure: "@@maineve/PUT_CHANGE_PASSWORD_FAILURE",
        baseurl: baseUrlAuth,
        endpoint: "/password",
        init: {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ credential }),
        },
    },
});

/**
 * Put Update Password Action
 */
export const putUpdatePasswordAction = (credential: IUpdatePassword): IApiRequest<TAuthApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/PUT_UPDATE_PASSWORD_REQUEST",
        success: "@@maineve/PUT_UPDATE_PASSWORD_SUCCESS",
        failure: "@@maineve/PUT_UPDATE_PASSWORD_FAILURE",
        baseurl: baseUrlAuth,
        endpoint: "/register",
        init: {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ credential }),
        },
    },
});

/**
 * Post Change Password Action
 */
export const getAccountConfirmationAction = (token: string): IApiRequest<TAuthApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/GET_ACCOUNT_CONFIRMATION_REQUEST",
        success: "@@maineve/GET_ACCOUNT_CONFIRMATION_SUCCESS",
        failure: "@@maineve/GET_ACCOUNT_CONFIRMATION_FAILURE",
        baseurl: baseUrlAuth,
        endpoint: `/confirmation?confirmation_token=${ token }`,
        init: {},
    },
});

/**
 * Check Password Match Action
 */
export const passwordMatchErrorAction = (): TAuthActions => ({
    type: "@@maineve/PASSWORD_MATCH_ERROR",
});
