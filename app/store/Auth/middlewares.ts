import { Middleware } from "redux";
import { IAppState } from "../types";
import { POST_LOGIN_SUCCESS, POST_VALIDATE_FAILURE } from "./types";

/**
 * Adds authorization token to local storage
 */
const saveAuthorizationToken: Middleware<{}, IAppState> = () => next => (action): void => {

    if (action.type !== POST_LOGIN_SUCCESS) {
        return next(action);
    }

    localStorage.setItem(`token`, action.payload.response.headers.get(`Authorization`));

    return next(action);
};

/**
 * Removes old authorization token from local storage
 */
const removeAuthorizationToken: Middleware<{}, IAppState> = () => next => (action): void => {

    if (action.type !== POST_VALIDATE_FAILURE) {
        return next(action);
    }

    localStorage.removeItem(`token`);

    return next(action);
};

export const authMiddleware: Middleware[] = [
    saveAuthorizationToken,
    removeAuthorizationToken,
];
