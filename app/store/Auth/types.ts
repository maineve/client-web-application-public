import { IJsonResponse } from "../types";

export interface IAuthState {
    fetching: boolean;
    authenticated: boolean;
    error: string;
    success: string;
}

export interface ILogin {
    email: string;
    password: string;
}

export interface ICreateAccount {
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    company: string;
}

export interface IForgotPassword {
    email: string;
}

export interface IChangePasswordValues {
    password: string;
    passwordConfirm: string;
}

export interface IChangePassword {
    password: string;
    reset_password_token: string;
}

export interface IUpdatePassword {
    password: string;
    current_password: string;
}

export interface ILoginUser {
    id: number;
    email: string;
    created_at: string;
    updated_at: string;
}

export const POST_VALIDATE_REQUEST = "@@maineve/POST_VALIDATE_REQUEST";
export const POST_VALIDATE_SUCCESS = "@@maineve/POST_VALIDATE_SUCCESS";
export const POST_VALIDATE_FAILURE = "@@maineve/POST_VALIDATE_FAILURE";

interface IPostValidateRequest {
    type: typeof POST_VALIDATE_REQUEST;
}

interface IPostValidateSuccess {
    type: typeof POST_VALIDATE_SUCCESS;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

interface IPostValidateFailure {
    type: typeof POST_VALIDATE_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const POST_LOGIN_REQUEST = "@@maineve/POST_LOGIN_REQUEST";
export const POST_LOGIN_SUCCESS = "@@maineve/POST_LOGIN_SUCCESS";
export const POST_LOGIN_FAILURE = "@@maineve/POST_LOGIN_FAILURE";

interface IPostLoginRequest {
    type: typeof POST_LOGIN_REQUEST;
}

interface IPostLoginSuccess {
    type: typeof POST_LOGIN_SUCCESS;
    payload: {
        json: IJsonResponse<ILoginUser>;
        response: Response;
    };
}

interface IPostLoginFailure {
    type: typeof POST_LOGIN_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const POST_CREATE_ACCOUNT_REQUEST = "@@maineve/POST_CREATE_ACCOUNT_REQUEST";
export const POST_CREATE_ACCOUNT_SUCCESS = "@@maineve/POST_CREATE_ACCOUNT_SUCCESS";
export const POST_CREATE_ACCOUNT_FAILURE = "@@maineve/POST_CREATE_ACCOUNT_FAILURE";

interface IPostCreateAccountRequest {
    type: typeof POST_CREATE_ACCOUNT_REQUEST;
}

interface IPostCreateAccountSuccess {
    type: typeof POST_CREATE_ACCOUNT_SUCCESS;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

interface IPostCreateAccountFailure {
    type: typeof POST_CREATE_ACCOUNT_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const POST_FORGOT_PASSWORD_REQUEST = "@@maineve/POST_FORGOT_PASSWORD_REQUEST";
export const POST_FORGOT_PASSWORD_SUCCESS = "@@maineve/POST_FORGOT_PASSWORD_SUCCESS";
export const POST_FORGOT_PASSWORD_FAILURE = "@@maineve/POST_FORGOT_PASSWORD_FAILURE";

interface IPostForgotPasswordRequest {
    type: typeof POST_FORGOT_PASSWORD_REQUEST;
}

interface IPostForgotPasswordSuccess {
    type: typeof POST_FORGOT_PASSWORD_SUCCESS;
    payload: {
        json: IJsonResponse<ILoginUser>;
        response: Response;
    };
}

interface IPostForgotPasswordFailure {
    type: typeof POST_FORGOT_PASSWORD_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const PUT_CHANGE_PASSWORD_REQUEST = "@@maineve/PUT_CHANGE_PASSWORD_REQUEST";
export const PUT_CHANGE_PASSWORD_SUCCESS = "@@maineve/PUT_CHANGE_PASSWORD_SUCCESS";
export const PUT_CHANGE_PASSWORD_FAILURE = "@@maineve/PUT_CHANGE_PASSWORD_FAILURE";

interface IPutChangePasswordRequest {
    type: typeof PUT_CHANGE_PASSWORD_REQUEST;
}

interface IPutChangePasswordSuccess {
    type: typeof PUT_CHANGE_PASSWORD_SUCCESS;
    payload: {
        json: IJsonResponse<ILoginUser>;
        response: Response;
    };
}

interface IPutChangePasswordFailure {
    type: typeof PUT_CHANGE_PASSWORD_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const PUT_UPDATE_PASSWORD_REQUEST = "@@maineve/PUT_UPDATE_PASSWORD_REQUEST";
export const PUT_UPDATE_PASSWORD_SUCCESS = "@@maineve/PUT_UPDATE_PASSWORD_SUCCESS";
export const PUT_UPDATE_PASSWORD_FAILURE = "@@maineve/PUT_UPDATE_PASSWORD_FAILURE";

interface IPutUpdatePasswordRequest {
    type: typeof PUT_UPDATE_PASSWORD_REQUEST;
}

interface IPutUpdatePasswordSuccess {
    type: typeof PUT_UPDATE_PASSWORD_SUCCESS;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

interface IPutUpdatePasswordFailure {
    type: typeof PUT_UPDATE_PASSWORD_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const GET_ACCOUNT_CONFIRMATION_REQUEST = "@@maineve/GET_ACCOUNT_CONFIRMATION_REQUEST";
export const GET_ACCOUNT_CONFIRMATION_SUCCESS = "@@maineve/GET_ACCOUNT_CONFIRMATION_SUCCESS";
export const GET_ACCOUNT_CONFIRMATION_FAILURE = "@@maineve/GET_ACCOUNT_CONFIRMATION_FAILURE";

interface IGetAccountConfirmationRequest {
    type: typeof GET_ACCOUNT_CONFIRMATION_REQUEST;
}

interface IGetAccountConfirmationSuccess {
    type: typeof GET_ACCOUNT_CONFIRMATION_SUCCESS;
    payload: {
        json: IJsonResponse<ILoginUser>;
        response: Response;
    };
}

interface IGetAccountConfirmationFailure {
    type: typeof GET_ACCOUNT_CONFIRMATION_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const PASSWORD_MATCH_ERROR = "@@maineve/PASSWORD_MATCH_ERROR";

interface IPasswordMatchError {
    type: typeof PASSWORD_MATCH_ERROR;
}

export type TAuthActions =
    IPostValidateRequest | IPostValidateSuccess | IPostValidateFailure |
    IPostLoginRequest | IPostLoginSuccess | IPostLoginFailure |
    IPostCreateAccountRequest | IPostCreateAccountSuccess | IPostCreateAccountFailure |
    IPostForgotPasswordRequest | IPostForgotPasswordSuccess | IPostForgotPasswordFailure |
    IPutChangePasswordRequest | IPutChangePasswordSuccess | IPutChangePasswordFailure |
    IPutUpdatePasswordRequest | IPutUpdatePasswordSuccess | IPutUpdatePasswordFailure |
    IGetAccountConfirmationRequest | IGetAccountConfirmationSuccess | IGetAccountConfirmationFailure |
    IPasswordMatchError;

export type TAuthApiActions =
    typeof POST_VALIDATE_REQUEST | typeof POST_VALIDATE_SUCCESS | typeof POST_VALIDATE_FAILURE |
    typeof POST_LOGIN_REQUEST | typeof POST_LOGIN_SUCCESS | typeof POST_LOGIN_FAILURE |
    typeof POST_CREATE_ACCOUNT_REQUEST | typeof POST_CREATE_ACCOUNT_SUCCESS | typeof POST_CREATE_ACCOUNT_FAILURE |
    typeof POST_FORGOT_PASSWORD_REQUEST | typeof POST_FORGOT_PASSWORD_SUCCESS | typeof POST_FORGOT_PASSWORD_FAILURE |
    typeof PUT_CHANGE_PASSWORD_REQUEST | typeof PUT_CHANGE_PASSWORD_SUCCESS | typeof PUT_CHANGE_PASSWORD_FAILURE |
    typeof PUT_UPDATE_PASSWORD_REQUEST | typeof PUT_UPDATE_PASSWORD_SUCCESS | typeof PUT_UPDATE_PASSWORD_FAILURE |
    typeof GET_ACCOUNT_CONFIRMATION_REQUEST | typeof GET_ACCOUNT_CONFIRMATION_SUCCESS | typeof GET_ACCOUNT_CONFIRMATION_FAILURE;
