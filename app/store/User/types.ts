import { IJsonResponse } from "../types";
import { IAccount } from "../Account/types";
import { IRole } from "../Role/types";

export interface IUserState {
    changingAccount: boolean;
    profile: {
        data: IUser;
        fetching: boolean;
        saving: boolean;
        removing: boolean;
    };
}

export interface IUser {
    created_at: string;
    current_account_id: number;
    email: string;
    firstname: string;
    id: number;
    image: string;
    lastname: string;
    updated_at: string;
    users_accounts?: IUserAccounts[];
}

export interface IUserAccounts {
    account_id: number;
    created_at: string;
    email: string;
    id: number;
    invited_by_id: number;
    role_id: number;
    token?: string;
    updated_at: string;
    user_id: string;
    account?: IAccount;
    role?: IRole;
}

export const GET_USER_PROFILE_REQUEST = "@@maineve/GET_USER_PROFILE_REQUEST";
export const GET_USER_PROFILE_SUCCESS = "@@maineve/GET_USER_PROFILE_SUCCESS";
export const GET_USER_PROFILE_FAILURE = "@@maineve/GET_USER_PROFILE_FAILURE";

interface IGetUserProfileRequest {
    type: typeof GET_USER_PROFILE_REQUEST;
}

interface IGetUserProfileSuccess {
    type: typeof GET_USER_PROFILE_SUCCESS;
    payload: {
        json: IJsonResponse<{ account: IAccount; profile: IUser }>;
        response: Response;
    };
}

interface IGetUserProfileFailure {
    type: typeof GET_USER_PROFILE_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const UPDATE_USER_PROFILE_REQUEST = "@@maineve/UPDATE_USER_PROFILE_REQUEST";
export const UPDATE_USER_PROFILE_SUCCESS = "@@maineve/UPDATE_USER_PROFILE_SUCCESS";
export const UPDATE_USER_PROFILE_FAILURE = "@@maineve/UPDATE_USER_PROFILE_FAILURE";

interface IUpdateUserProfileRequest {
    type: typeof UPDATE_USER_PROFILE_REQUEST;
}

interface IUpdateUserProfileSuccess {
    type: typeof UPDATE_USER_PROFILE_SUCCESS;
    payload: {
        json: IJsonResponse<{ profile: IUser }>;
        response: Response;
    };
}

interface IUpdateUserProfileFailure {
    type: typeof UPDATE_USER_PROFILE_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const CHANGING_ACCOUNT = "@@maineve/CHANGING_ACCOUNT";

interface IChangingAccount {
    type: typeof CHANGING_ACCOUNT;
}

export type TUserActions =
    IGetUserProfileRequest | IGetUserProfileSuccess | IGetUserProfileFailure |
    IUpdateUserProfileRequest | IUpdateUserProfileSuccess | IUpdateUserProfileFailure |
    IChangingAccount;

export type TUserApiActions =
    typeof GET_USER_PROFILE_REQUEST | typeof GET_USER_PROFILE_SUCCESS | typeof GET_USER_PROFILE_FAILURE |
    typeof UPDATE_USER_PROFILE_REQUEST | typeof UPDATE_USER_PROFILE_SUCCESS | typeof UPDATE_USER_PROFILE_FAILURE;
