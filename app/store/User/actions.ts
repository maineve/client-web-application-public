import { IApiRequest } from "../types";
import { TUserApiActions, IUser, TUserActions } from "./types";

/**
 * Get User Profile Action
 *
 * @param email
 */
export const getUserProfileAction = (email: string): IApiRequest<TUserApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/GET_USER_PROFILE_REQUEST",
        success: "@@maineve/GET_USER_PROFILE_SUCCESS",
        failure: "@@maineve/GET_USER_PROFILE_FAILURE",
        endpoint: `/profiles/${ email }`,
        init: {},
    },
});

/**
 * Update User Profile Action
 *
 * @param email
 */
export const updateUserProfileAction = (id: number, data: IUser): IApiRequest<TUserApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/UPDATE_USER_PROFILE_REQUEST",
        success: "@@maineve/UPDATE_USER_PROFILE_SUCCESS",
        failure: "@@maineve/UPDATE_USER_PROFILE_FAILURE",
        endpoint: `/profiles/${ id }`,
        init: {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ data }),
        },
    },
});

/**
 * Changing Account Action
 */
export const changingAccountAction = (): TUserActions => ({
    type: "@@maineve/CHANGING_ACCOUNT",
});
