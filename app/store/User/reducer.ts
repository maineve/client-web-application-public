import { IUserState, TUserActions } from "./types";
import { Reducer } from "redux";

export const initState: IUserState = {
    changingAccount: false,
    profile: {
        data: null,
        fetching: false,
        saving: false,
        removing: false,
    },
};

export const userReducer: Reducer<IUserState, TUserActions> = (state = initState, action) => {

    switch (action.type) {

        case "@@maineve/GET_USER_PROFILE_REQUEST":
            return {
                ...state,
                profile: {
                    ...state.profile,
                    fetching: true,
                },
            };
        case "@@maineve/GET_USER_PROFILE_SUCCESS":
            return {
                ...state,
                profile: {
                    ...state.profile,
                    data: action.payload.json.data.profile,
                    fetching: false,
                },
            };
        case "@@maineve/GET_USER_PROFILE_FAILURE":
            return {
                ...state,
                profile: {
                    ...state.profile,
                    fetching: false,
                },
            };
        case "@@maineve/UPDATE_USER_PROFILE_REQUEST":
            return {
                ...state,
                profile: {
                    ...state.profile,
                    saving: true,
                },
            };
        case "@@maineve/UPDATE_USER_PROFILE_SUCCESS":
            return {
                ...state,
                changingAccount: false,
                profile: {
                    ...state.profile,
                    data: {
                        ...state.profile.data,
                        ...action.payload.json.data.profile,
                    },
                    saving: false,
                },
            };
        case "@@maineve/UPDATE_USER_PROFILE_FAILURE":
            return {
                ...state,
                changingAccount: false,
                profile: {
                    ...state.profile,
                    saving: false,
                },
            };
        case "@@maineve/CHANGING_ACCOUNT":
            return {
                ...state,
                changingAccount: true,
            };
        default:
            return state;

    }

};
