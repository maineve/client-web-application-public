import { Middleware } from "redux";
import { IAppState } from "../types";
import { GET_USER_PROFILE_SUCCESS, CHANGING_ACCOUNT } from "./types";
import { setApplicationUninitializedAction } from "../Site/actions";

/**
 * Set Recurrence ID Middleware
 */
const addProfileToLocalStorage: Middleware<{}, IAppState> = store => next => (action): void => {

    if (action.type !== GET_USER_PROFILE_SUCCESS) {
        return next(action);
    }

    const result = next(action);
    const profile = store.getState().user.profile.data;

    localStorage.setItem("profile", JSON.stringify(profile));

    return result;
};

const userChangingAccount: Middleware<{}, IAppState> = store => next => (action): void => {

    if (action.type !== CHANGING_ACCOUNT) {
        return next(action);
    }

    // Stops app from reinitializing before reload
    localStorage.removeItem("profile");

    store.dispatch(setApplicationUninitializedAction());

    return next(action);
};

export const userMiddleware: Middleware[] = [
    addProfileToLocalStorage,
    userChangingAccount,
];
