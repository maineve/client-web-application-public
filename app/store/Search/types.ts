import { IJsonResponse } from "../types";
import { IReservation } from "../Reservation/types";

export interface ISearchState {
    results: {
        data: IReservation[];
        loading: boolean;
    };
}

export const GET_SEARCH_REQUEST = "@@maineve/GET_SEARCH_REQUEST";
export const GET_SEARCH_SUCCESS = "@@maineve/GET_SEARCH_SUCCESS";
export const GET_SEARCH_FAILURE = "@@maineve/GET_SEARCH_FAILURE";

interface IGetSearchRequest {
    type: typeof GET_SEARCH_REQUEST;
}

interface IGetSearchSuccess {
    type: typeof GET_SEARCH_SUCCESS;
    payload: {
        json: IJsonResponse<IReservation[]>;
        response: Response;
    };
}

interface IGetSearchFailure {
    type: typeof GET_SEARCH_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const RESET_SEARCH_RESULTS = "@@maineve/RESET_SEARCH_RESULTS";

interface IResetSearchResults {
    type: typeof RESET_SEARCH_RESULTS;
}

export type TSearchActions =
    IGetSearchRequest | IGetSearchSuccess | IGetSearchFailure |
    IResetSearchResults;

export type TSearchApiActions =
    typeof GET_SEARCH_REQUEST | typeof GET_SEARCH_SUCCESS | typeof GET_SEARCH_FAILURE;
