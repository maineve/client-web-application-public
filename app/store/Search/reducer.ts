import { ISearchState, TSearchActions } from "./types";
import { Reducer } from "redux";

export const initState: ISearchState = {
    results: {
        data: [],
        loading: false,
    },
};

export const searchReducer: Reducer<ISearchState, TSearchActions> = (state = initState, action) => {

    switch (action.type) {

        case "@@maineve/GET_SEARCH_REQUEST":
            return {
                ...state,
                results: {
                    ...state.results,
                    loading: true,
                    data: [],
                },
            };
        case "@@maineve/GET_SEARCH_SUCCESS":
            return {
                ...state,
                results: {
                    ...state.results,
                    loading: false,
                    data: action.payload.json.data,
                },
            };
        case "@@maineve/GET_SEARCH_FAILURE":
            return {
                ...state,
                results: {
                    ...state.results,
                    loading: false,
                },
            };
        case "@@maineve/RESET_SEARCH_RESULTS":
            return {
                ...state,
                results: {
                    data: [],
                    loading: false,
                },
            };
        default:
            return state;

    }
};
