import { IApiRequest } from "../types";
import { TSearchApiActions, TSearchActions } from "./types";

/**
 * Get Search Action
 *
 * @param query
 */
export const getSearchAction = (query: string): IApiRequest<TSearchApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/GET_SEARCH_REQUEST",
        success: "@@maineve/GET_SEARCH_SUCCESS",
        failure: "@@maineve/GET_SEARCH_FAILURE",
        endpoint: `/search?q=${ query }`,
        init: {},
    },
});

/**
 * Reset Search Results Action
 */
export const resetSearchResultsAction = (): TSearchActions => ({
    type: "@@maineve/RESET_SEARCH_RESULTS",
});
