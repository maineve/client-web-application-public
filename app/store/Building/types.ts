import { IRoom } from "../Room/types";
import { IJsonResponse } from "../types";

export interface IBuilding {
    id: number;
    account_id: number;
    location: string;
    name: string;
    number: number;
    rooms?: IRoom[];
    timezone?: string;
    created_at: string;
    updated_at: string;
}

export interface IBuildingState {
    fetching: boolean;
    buildings: IBuilding[];
    building: IBuilding;
}

export const GET_BUILDINGS_REQUEST = "@@maineve/GET_BUILDINGS_REQUEST";
export const GET_BUILDINGS_SUCCESS = "@@maineve/GET_BUILDINGS_SUCCESS";
export const GET_BUILDINGS_FAILURE = "@@maineve/GET_BUILDINGS_FAILURE";

interface IGetBuildingsRequest {
    type: typeof GET_BUILDINGS_REQUEST;
}

interface IGetBuildingsSuccess {
    type: typeof GET_BUILDINGS_SUCCESS;
    payload: {
        json: IJsonResponse<IBuilding[]>;
        response: Response;
    };
}

interface IGetBuildingsFailure {
    type: typeof GET_BUILDINGS_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const GET_BUILDING_REQUEST = "@@maineve/GET_BUILDING_REQUEST";
export const GET_BUILDING_SUCCESS = "@@maineve/GET_BUILDING_SUCCESS";
export const GET_BUILDING_FAILURE = "@@maineve/GET_BUILDING_FAILURE";

interface IGetBuildingRequest {
    type: typeof GET_BUILDING_REQUEST;
}

interface IGetBuildingSuccess {
    type: typeof GET_BUILDING_SUCCESS;
    payload: {
        json: IJsonResponse<IBuilding>;
        response: Response;
    };
}

interface IGetBuildingFailure {
    type: typeof GET_BUILDING_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const POST_BUILDING_REQUEST = "@@maineve/POST_BUILDING_REQUEST";
export const POST_BUILDING_SUCCESS = "@@maineve/POST_BUILDING_SUCCESS";
export const POST_BUILDING_FAILURE = "@@maineve/POST_BUILDING_FAILURE";

interface IPostBuildingRequest {
    type: typeof POST_BUILDING_REQUEST;
}

interface IPostBuildingSuccess {
    type: typeof POST_BUILDING_SUCCESS;
    payload: {
        json: IJsonResponse<IBuilding>;
        response: Response;
    };
}

interface IPostBuildingFailure {
    type: typeof POST_BUILDING_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const PUT_BUILDING_REQUEST = "@@maineve/PUT_BUILDING_REQUEST";
export const PUT_BUILDING_SUCCESS = "@@maineve/PUT_BUILDING_SUCCESS";
export const PUT_BUILDING_FAILURE = "@@maineve/PUT_BUILDING_FAILURE";

interface IPutBuildingRequest {
    type: typeof PUT_BUILDING_REQUEST;
}

interface IPutBuildingSuccess {
    type: typeof PUT_BUILDING_SUCCESS;
    payload: {
        json: IJsonResponse<IBuilding>;
        response: Response;
    };
}

interface IPutBuildingFailure {
    type: typeof PUT_BUILDING_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const DELETE_BUILDING_REQUEST = "@@maineve/DELETE_BUILDING_REQUEST";
export const DELETE_BUILDING_SUCCESS = "@@maineve/DELETE_BUILDING_SUCCESS";
export const DELETE_BUILDING_FAILURE = "@@maineve/DELETE_BUILDING_FAILURE";

interface IDeleteBuildingRequest {
    type: typeof DELETE_BUILDING_REQUEST;
}

interface IDeleteBuildingSuccess {
    type: typeof DELETE_BUILDING_SUCCESS;
    payload: {
        response: Response;
    };
}

interface IDeleteBuildingFailure {
    type: typeof DELETE_BUILDING_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export type TBuildingActions =
    IGetBuildingsRequest | IGetBuildingsSuccess | IGetBuildingsFailure |
    IGetBuildingRequest | IGetBuildingSuccess | IGetBuildingFailure |
    IPostBuildingRequest | IPostBuildingSuccess | IPostBuildingFailure |
    IPutBuildingRequest | IPutBuildingSuccess | IPutBuildingFailure |
    IDeleteBuildingRequest | IDeleteBuildingSuccess | IDeleteBuildingFailure;

export type TBuildingApiActions =
    typeof GET_BUILDINGS_REQUEST | typeof GET_BUILDINGS_SUCCESS | typeof GET_BUILDINGS_FAILURE |
    typeof GET_BUILDING_REQUEST | typeof GET_BUILDING_SUCCESS | typeof GET_BUILDING_FAILURE |
    typeof POST_BUILDING_REQUEST | typeof POST_BUILDING_SUCCESS | typeof POST_BUILDING_FAILURE |
    typeof PUT_BUILDING_REQUEST | typeof PUT_BUILDING_SUCCESS | typeof PUT_BUILDING_FAILURE |
    typeof DELETE_BUILDING_REQUEST | typeof DELETE_BUILDING_SUCCESS | typeof DELETE_BUILDING_FAILURE;
