import { IBuildingState, TBuildingActions } from "./types";
import { Reducer } from "redux";

export const initState: IBuildingState = {
    fetching: false,
    buildings: [],
    building: undefined,
};

export const buildingReducer: Reducer<IBuildingState, TBuildingActions> = (state = initState, action) => {

    switch (action.type) {

        case "@@maineve/GET_BUILDINGS_REQUEST":
            return {
                ...state,
                fetching: true,
            };
        case "@@maineve/GET_BUILDINGS_SUCCESS":
            return {
                ...state,
                fetching: false,
                buildings: action.payload.json.data,
            };
        case "@@maineve/GET_BUILDINGS_FAILURE":
            return {
                ...state,
                fetching: false,
            };
        case "@@maineve/GET_BUILDING_REQUEST":
            return {
                ...state,
                fetching: true,
            };
        case "@@maineve/GET_BUILDING_SUCCESS":
            return {
                ...state,
                fetching: false,
                building: action.payload.json.data,
            };
        case "@@maineve/GET_BUILDING_FAILURE":
            return {
                ...state,
                fetching: false,
            };
        case "@@maineve/POST_BUILDING_REQUEST":
            return {
                ...state,
                fetching: true,
            };
        case "@@maineve/POST_BUILDING_SUCCESS":
            return {
                ...state,
                fetching: false,
                building: action.payload.json.data,
            };
        case "@@maineve/POST_BUILDING_FAILURE":
            return {
                ...state,
                fetching: false,
            };
        case "@@maineve/PUT_BUILDING_REQUEST":
            return {
                ...state,
                fetching: true,
            };
        case "@@maineve/PUT_BUILDING_SUCCESS":
            return {
                ...state,
                fetching: false,
                building: action.payload.json.data,
            };
        case "@@maineve/PUT_BUILDING_FAILURE":
            return {
                ...state,
                fetching: false,
            };
        case "@@maineve/DELETE_BUILDING_REQUEST":
            return {
                ...state,
                fetching: true,
            };
        case "@@maineve/DELETE_BUILDING_SUCCESS":
            return {
                ...state,
                fetching: false,
            };
        case "@@maineve/DELETE_BUILDING_FAILURE":
            return {
                ...state,
                fetching: false,
            };
        default:
            return state;

    }

};
