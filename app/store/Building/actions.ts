import { IApiRequest } from "../types";
import { TBuildingApiActions } from "./types";

/**
 * Get Buildings Action
 */
export const getBuildingsAction = (): IApiRequest<TBuildingApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/GET_BUILDINGS_REQUEST",
        success: "@@maineve/GET_BUILDINGS_SUCCESS",
        failure: "@@maineve/GET_BUILDINGS_FAILURE",
        endpoint: "/buildings",
        init: {},
    },
});

/**
 * Get Building By ID Action
 *
 * @param id
 */
export const getBuildingAction = (id: number): IApiRequest<TBuildingApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/GET_BUILDING_REQUEST",
        success: "@@maineve/GET_BUILDING_SUCCESS",
        failure: "@@maineve/GET_BUILDING_FAILURE",
        endpoint: `/buildings/${ id }`,
        init: {},
    },
});
