import { IApiRequest } from "../types";
import { TRoomApiActions } from "./types";

/**
 * Get Rooms Action
 */
export const getRoomsAction = (): IApiRequest<TRoomApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/GET_ROOMS_REQUEST",
        success: "@@maineve/GET_ROOMS_SUCCESS",
        failure: "@@maineve/GET_ROOMS_FAILURE",
        endpoint: "/rooms",
        init: {},
    },
});

/**
 * Get Room Action
 */
export const getRoomAction = (id: number): IApiRequest<TRoomApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/GET_ROOM_REQUEST",
        success: "@@maineve/GET_ROOM_SUCCESS",
        failure: "@@maineve/GET_ROOM_FAILURE",
        endpoint: `/rooms/${ id }`,
        init: {},
    },
});
