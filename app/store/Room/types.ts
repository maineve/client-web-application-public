import { IJsonResponse } from "../types";

export interface IRoom {
    id: number;
    account_id: number;
    building_id: number;
    features: string;
    name: string;
    number: number;
    seats: number;
    active: boolean;
    created_at: string;
    updated_at: string;
}

export interface IRoomState {
    fetching: boolean;
    rooms: IRoom[];
}

export const GET_ROOMS_REQUEST = "@@maineve/GET_ROOMS_REQUEST";
export const GET_ROOMS_SUCCESS = "@@maineve/GET_ROOMS_SUCCESS";
export const GET_ROOMS_FAILURE = "@@maineve/GET_ROOMS_FAILURE";

interface IGetRoomsRequest {
    type: typeof GET_ROOMS_REQUEST;
}

interface IGetRoomsSuccess {
    type: typeof GET_ROOMS_SUCCESS;
    payload: {
        json: IJsonResponse<IRoom[]>;
        response: Response;
    };
}

interface IGetRoomsFailure {
    type: typeof GET_ROOMS_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const GET_ROOM_REQUEST = "@@maineve/GET_ROOM_REQUEST";
export const GET_ROOM_SUCCESS = "@@maineve/GET_ROOM_SUCCESS";
export const GET_ROOM_FAILURE = "@@maineve/GET_ROOM_FAILURE";

interface IGetRoomRequest {
    type: typeof GET_ROOM_REQUEST;
}

interface IGetRoomSuccess {
    type: typeof GET_ROOM_SUCCESS;
    payload: {
        json: IJsonResponse<IRoom>;
        response: Response;
    };
}

interface IGetRoomFailure {
    type: typeof GET_ROOM_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const POST_ROOM_REQUEST = "@@maineve/POST_ROOM_REQUEST";
export const POST_ROOM_SUCCESS = "@@maineve/POST_ROOM_SUCCESS";
export const POST_ROOM_FAILURE = "@@maineve/POST_ROOM_FAILURE";

interface IPostRoomRequest {
    type: typeof POST_ROOM_REQUEST;
}

interface IPostRoomSuccess {
    type: typeof POST_ROOM_SUCCESS;
    payload: {
        json: IJsonResponse<IRoom>;
        response: Response;
    };
}

interface IPostRoomFailure {
    type: typeof POST_ROOM_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const PUT_ROOM_REQUEST = "@@maineve/PUT_ROOM_REQUEST";
export const PUT_ROOM_SUCCESS = "@@maineve/PUT_ROOM_SUCCESS";
export const PUT_ROOM_FAILURE = "@@maineve/PUT_ROOM_FAILURE";

interface IPutRoomRequest {
    type: typeof PUT_ROOM_REQUEST;
}

interface IPutRoomSuccess {
    type: typeof PUT_ROOM_SUCCESS;
    payload: {
        json: IJsonResponse<IRoom>;
        response: Response;
    };
}

interface IPutRoomFailure {
    type: typeof PUT_ROOM_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const DELETE_ROOM_REQUEST = "@@maineve/DELETE_ROOM_REQUEST";
export const DELETE_ROOM_SUCCESS = "@@maineve/DELETE_ROOM_SUCCESS";
export const DELETE_ROOM_FAILURE = "@@maineve/DELETE_ROOM_FAILURE";

interface IDeleteRoomRequest {
    type: typeof DELETE_ROOM_REQUEST;
}

interface IDeleteRoomSuccess {
    type: typeof DELETE_ROOM_SUCCESS;
    payload: {
        response: Response;
    };
}

interface IDeleteRoomFailure {
    type: typeof DELETE_ROOM_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export type TRoomActions =
    IGetRoomsRequest | IGetRoomsSuccess | IGetRoomsFailure |
    IGetRoomRequest | IGetRoomSuccess | IGetRoomFailure |
    IPostRoomRequest | IPostRoomSuccess | IPostRoomFailure |
    IPutRoomRequest | IPutRoomSuccess | IPutRoomFailure |
    IDeleteRoomRequest | IDeleteRoomSuccess | IDeleteRoomFailure;

export type TRoomApiActions =
    typeof GET_ROOMS_REQUEST | typeof GET_ROOMS_SUCCESS | typeof GET_ROOMS_FAILURE |
    typeof GET_ROOM_REQUEST | typeof GET_ROOM_SUCCESS | typeof GET_ROOM_FAILURE |
    typeof POST_ROOM_REQUEST | typeof POST_ROOM_SUCCESS | typeof POST_ROOM_FAILURE |
    typeof PUT_ROOM_REQUEST | typeof PUT_ROOM_SUCCESS | typeof PUT_ROOM_FAILURE |
    typeof DELETE_ROOM_REQUEST | typeof DELETE_ROOM_SUCCESS | typeof DELETE_ROOM_FAILURE;
