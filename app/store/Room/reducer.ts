import { IRoomState, TRoomActions } from "./types";
import { Reducer } from "redux";

export const initState: IRoomState = {
    fetching: false,
    rooms: undefined,
};

export const roomReducer: Reducer<IRoomState, TRoomActions> = (state = initState, action) => {

    switch (action.type) {

        case "@@maineve/GET_ROOMS_REQUEST":
            return {
                ...state,
                rooms: undefined,
                fetching: true,
            };
        case "@@maineve/GET_ROOMS_SUCCESS":
            return {
                ...state,
                fetching: false,
                rooms: action.payload.json.data,
            };
        case "@@maineve/GET_ROOMS_FAILURE":
            return {
                ...state,
                fetching: false,
            };
        case "@@maineve/GET_ROOM_REQUEST":
            return {
                ...state,
                fetching: true,
            };
        case "@@maineve/GET_ROOM_SUCCESS":
            return {
                ...state,
                fetching: false,
                room: action.payload.json.data,
            };
        case "@@maineve/GET_ROOM_FAILURE":
            return {
                ...state,
                fetching: false,
            };
        case "@@maineve/POST_ROOM_REQUEST":
            return {
                ...state,
                fetching: true,
            };
        case "@@maineve/POST_ROOM_SUCCESS":
            return {
                ...state,
                fetching: false,
                room: action.payload.json.data,
            };
        case "@@maineve/POST_ROOM_FAILURE":
            return {
                ...state,
                fetching: false,
            };
        case "@@maineve/PUT_ROOM_REQUEST":
            return {
                ...state,
                fetching: true,
            };
        case "@@maineve/PUT_ROOM_SUCCESS":
            return {
                ...state,
                fetching: false,
                room: action.payload.json.data,
            };
        case "@@maineve/PUT_ROOM_FAILURE":
            return {
                ...state,
                fetching: false,
            };
        case "@@maineve/DELETE_ROOM_REQUEST":
            return {
                ...state,
                fetching: true,
            };
        case "@@maineve/DELETE_ROOM_SUCCESS":
            return {
                ...state,
                fetching: false,
            };
        case "@@maineve/DELETE_ROOM_FAILURE":
            return {
                ...state,
                fetching: false,
            };
        default:
            return state;

    }
};
