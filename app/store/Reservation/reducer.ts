import { IReservationState, TReservationActions } from "./types";
import { Reducer } from "redux";
import { default as moment } from "moment";

export const initState: IReservationState = {
    fetching: false,
    saving: false,
    deleting: false,
    reservationSource: null,
    reservation: null,
    reservations: [],
    timelineReservations: [],
    timelineReservationsLoading: false,
};

export const reservationReducer: Reducer<IReservationState, TReservationActions> = (state = initState, action) => {

    switch (action.type) {

        case "@@maineve/GET_RESERVATIONS_REQUEST":
            return {
                ...state,
                fetching: true,
                reservations: [],
            };
        case "@@maineve/GET_RESERVATIONS_SUCCESS":
            return {
                ...state,
                fetching: false,
                reservations: action.payload.json.data,
            };
        case "@@maineve/GET_RESERVATIONS_FAILURE":
            return {
                ...state,
                fetching: false,
            };
        case "@@maineve/GET_RESERVATION_REQUEST":
            return {
                ...state,
                fetching: true,
            };
        case "@@maineve/GET_RESERVATION_SUCCESS":
            return {
                ...state,
                fetching: false,
                reservation: {
                    ...action.payload.json.data,
                    startdate: moment.utc(action.payload.json.data.startdate),
                    enddate: moment.utc(action.payload.json.data.enddate),
                },
            };
        case "@@maineve/GET_RESERVATION_FAILURE":
            return {
                ...state,
                fetching: false,
            };
        case "@@maineve/POST_RESERVATION_REQUEST":
            return {
                ...state,
                saving: true,
                error: false,
                errorMessage: null,
            };
        case "@@maineve/POST_RESERVATION_SUCCESS":
            return {
                ...state,
                saving: false,
            };
        case "@@maineve/POST_RESERVATION_FAILURE":
            return {
                ...state,
                saving: false,
            };
        case "@@maineve/PUT_RESERVATION_REQUEST":
            return {
                ...state,
                saving: true,
            };
        case "@@maineve/PUT_RESERVATION_SUCCESS":
            return {
                ...state,
                saving: false,
            };
        case "@@maineve/PUT_RESERVATION_FAILURE":
            return {
                ...state,
                saving: false,
            };
        case "@@maineve/PUT_RECURRENCE_REQUEST":
            return {
                ...state,
                saving: true,
            };
        case "@@maineve/PUT_RECURRENCE_SUCCESS":
            return {
                ...state,
                saving: false,
            };
        case "@@maineve/PUT_RECURRENCE_FAILURE":
            return {
                ...state,
                saving: false,
            };
        case "@@maineve/DELETE_RESERVATION_REQUEST":
            return {
                ...state,
                deleting: true,
            };
        case "@@maineve/DELETE_RESERVATION_SUCCESS":
            return {
                ...state,
                deleting: false,
            };
        case "@@maineve/DELETE_RESERVATION_FAILURE":
            return {
                ...state,
                deleting: false,
            };
        case "@@maineve/DELETE_RECURRENCE_REQUEST":
            return {
                ...state,
                deleting: true,
            };
        case "@@maineve/DELETE_RECURRENCE_SUCCESS":
            return {
                ...state,
                deleting: false,
            };
        case "@@maineve/DELETE_RECURRENCE_FAILURE":
            return {
                ...state,
                deleting: false,
            };
        case "@@maineve/GET_TIMELINE_RESERVATIONS_REQUEST":
            return {
                ...state,
                timelineReservationsLoading: true,
                timelineReservations: [],
            };
        case "@@maineve/GET_TIMELINE_RESERVATIONS_SUCCESS":
            return {
                ...state,
                timelineReservationsLoading: false,
                timelineReservations: action.payload.json.data,
            };
        case "@@maineve/GET_TIMELINE_RESERVATIONS_FAILURE":
            return {
                ...state,
                timelineReservationsLoading: false,
            };
        case "@@maineve/INITIATE_NEW_RESERVATION":
            return {
                ...state,
                reservation: {
                    startdate: moment.utc(action.payload.data.date).add(9, "hour"),
                    enddate: moment.utc(action.payload.data.date).add(10, "hour"),
                    title: null,
                    details: null,
                    "room_id": null,
                },
            };
        case "@@maineve/SAVE_SOURCE_IN_STATE":
            return {
                ...state,
                reservationSource: action.payload.data,
            };
        case "@@maineve/RESET_RESERVATION":
            return {
                ...state,
                reservation: null,
                reservationSource: null,
                timelineReservations: [],
            };
        default:
            return state;

    }

};
