import { Middleware } from "redux";
import { IAppState } from "../types";

import {
    GET_RESERVATION_SUCCESS,
    GET_TIMELINE_RESERVATIONS_SUCCESS,
    SAVE_RESERVATION,
    REMOVE_RESERVATION,
    IReservation,
    IPostReservation,
    IPostRecurrence,
    ISearchSource,
} from "./types";

import { putReservationAction, postReservationAction, putRecurrenceAction, deleteReservationAction, deleteRecurrenceAction } from "./actions";

/**
 * Set Recurrence ID Middleware
 */
const setReservationRecurrence: Middleware<{}, IAppState> = store => next => (action): void => {

    if (action.type !== GET_RESERVATION_SUCCESS) {
        return next(action);
    }

    if (store.getState().reservation.reservationSource.recurrence_id) {
        action.payload.json.data = {
            ...action.payload.json.data,
            startdate: store.getState().reservation.reservationSource.start,
            enddate: store.getState().reservation.reservationSource.end,
            "recurrence_id": store.getState().reservation.reservationSource.recurrence_id,
            "original_id": action.payload.json.data.id,
            id: null,
        } as IReservation;
    }

    return next(action);
};

/**
 * Filters out currently active reservation for showing other
 * reservations in the timeline.
 */
const timelineReservationsFilter: Middleware<{}, IAppState> = store => next => (action): void => {

    if (action.type !== GET_TIMELINE_RESERVATIONS_SUCCESS) {
        return next(action);
    }

    const reservation: ISearchSource = store.getState().reservation.reservationSource;

    if (reservation) {

        if (reservation.recurrence) {
            action.payload.json.data = action.payload.json.data.filter((r: IReservation) => r.original_id !== reservation.id);
            return next(action);
        }

        if (reservation.recurrence_id) {
            action.payload.json.data = action.payload.json.data.filter((r: IReservation) => r.recurrence_id !== reservation.recurrence_id);
            return next(action);
        }

        if (reservation.id) {
            action.payload.json.data = action.payload.json.data.filter((r: IReservation) => r.id !== reservation.id);
            return next(action);
        }
    }

    return next(action);
};

/**
 * Chooses whether to post of put the reservation and prepares the
 * object data accordingly.
 */
const reservationSave: Middleware<{}, IAppState> = store => next => (action): void => {

    if (action.type !== SAVE_RESERVATION) {
        return next(action);
    }

    const result = next(action);

    if (!action.payload.data.id && !action.payload.data.recurrence_id) {

        const data: IPostReservation = {
            title: action.payload.data.title,
            "room_id": action.payload.data.room_id,
            details: action.payload.data.details,
            startdate: action.payload.data.startdate,
            enddate: action.payload.data.enddate,
            recurrence: action.payload.data.recurrence,
        };

        store.dispatch(postReservationAction(data));

    } else if (action.payload.data.recurrence_id) {

        const data: IPostRecurrence = {
            title: action.payload.data.title,
            "room_id": action.payload.data.room_id,
            details: action.payload.data.details,
            startdate: action.payload.data.startdate,
            enddate: action.payload.data.enddate,
            "original_id": action.payload.data.original_id,
            "original_startdate": store.getState().reservation.reservationSource.start.format(),
            "original_enddate": store.getState().reservation.reservationSource.end.format(),
            "recurrence_id": action.payload.data.recurrence_id,
            recurrence: null,
        };

        store.dispatch(putRecurrenceAction(action.payload.data.original_id, data));

    } else {

        const { users, ...data } = action.payload.data;

        store.dispatch(putReservationAction(data));
    }

    return result;
};

/**
 * Handles whether to delete a recurrence or reservation
 */
const reservationDelete: Middleware<{}, IAppState> = store => next => (action): void => {

    if (action.type !== REMOVE_RESERVATION) {
        return next(action);
    }

    const result = next(action);
    const reservation = action.payload.data as IReservation;

    if (reservation.recurrence_id && !action.payload.deleteAll) {
        store.dispatch(deleteRecurrenceAction(reservation.original_id, reservation.recurrence_id));
    } else if (reservation.recurrence_id && action.payload.deleteAll) {
        store.dispatch(deleteReservationAction(reservation.original_id));
    } else {
        store.dispatch(deleteReservationAction(reservation.id));
    }

    return result;
};

export const reservationMiddleware: Middleware[] = [
    setReservationRecurrence,
    timelineReservationsFilter,
    reservationSave,
    reservationDelete,
];
