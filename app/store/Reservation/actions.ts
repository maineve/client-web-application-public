import { Moment } from "moment";
import { IApiRequest } from "../types";

import {
    IReservation,
    IPostReservation,
    IPostRecurrence,
    TReservationActions,
    TReservationApiActions,
    ICalendarSource,
    ISearchSource,
} from "./types";

/**
 * Get Reservations Action
 *
 * @param rangeStart
 * @param rangeEnd
 */
export const getReservationsAction = (rangeStart: Moment, rangeEnd: Moment): IApiRequest<TReservationApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/GET_RESERVATIONS_REQUEST",
        success: "@@maineve/GET_RESERVATIONS_SUCCESS",
        failure: "@@maineve/GET_RESERVATIONS_FAILURE",
        endpoint: `/reservations?range[start]=${ rangeStart.format() }&range[end]=${ rangeEnd.format() }`,
        init: {},
    },
});

/**
 * Get Reservation Action
 *
 * @param id
 */
export const getReservationAction = (id: number): IApiRequest<TReservationApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/GET_RESERVATION_REQUEST",
        success: "@@maineve/GET_RESERVATION_SUCCESS",
        failure: "@@maineve/GET_RESERVATION_FAILURE",
        endpoint: `/reservations/${ id }`,
        init: {},
    },
});

/**
 * Create a new reservation
 *
 * @param data
 */
export const postReservationAction = (data: IPostReservation): IApiRequest<TReservationApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/POST_RESERVATION_REQUEST",
        success: "@@maineve/POST_RESERVATION_SUCCESS",
        failure: "@@maineve/POST_RESERVATION_FAILURE",
        endpoint: `/reservations`,
        init: {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ data }),
        },
    },
});

/**
 * Update an exisiting reservation
 *
 * @param data
 */
export const putReservationAction = (data: IReservation): IApiRequest<TReservationApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/PUT_RESERVATION_REQUEST",
        success: "@@maineve/PUT_RESERVATION_SUCCESS",
        failure: "@@maineve/PUT_RESERVATION_FAILURE",
        endpoint: `/reservations/${ data.id }`,
        init: {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ data }),
        },
    },
});

export const deleteReservationAction = (id: number): IApiRequest<TReservationApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/DELETE_RESERVATION_REQUEST",
        success: "@@maineve/DELETE_RESERVATION_SUCCESS",
        failure: "@@maineve/DELETE_RESERVATION_FAILURE",
        endpoint: `/reservations/${ id }`,
        init: {
            method: "DELETE",
        },
    },
});

/**
 * Update a reservation recurrence
 *
 * @param id
 * @param data
 */
export const putRecurrenceAction = (id: number, data: IPostRecurrence): IApiRequest<TReservationApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/PUT_RECURRENCE_REQUEST",
        success: "@@maineve/PUT_RECURRENCE_SUCCESS",
        failure: "@@maineve/PUT_RECURRENCE_FAILURE",
        endpoint: `/reservations/${ id }/recurrences/${ data.recurrence_id }`,
        init: {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ data }),
        },
    },
});

export const deleteRecurrenceAction = (id: number, recurrenceId: string): IApiRequest<TReservationApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/DELETE_RECURRENCE_REQUEST",
        success: "@@maineve/DELETE_RECURRENCE_SUCCESS",
        failure: "@@maineve/DELETE_RECURRENCE_FAILURE",
        endpoint: `/reservations/${ id }/recurrences/${ recurrenceId }`,
        init: {
            method: "DELETE",
        },
    },
});

/**
 * Get Timeline Reservations Action
 *
 * @param date
 * @param roomId
 */
export const getTimelineReservationsAction = (roomId: number, date?: Moment): IApiRequest<TReservationApiActions> => ({
    type: "@@maineve/API_REQUEST",
    meta: {
        request: "@@maineve/GET_TIMELINE_RESERVATIONS_REQUEST",
        success: "@@maineve/GET_TIMELINE_RESERVATIONS_SUCCESS",
        failure: "@@maineve/GET_TIMELINE_RESERVATIONS_FAILURE",
        endpoint: `/rooms/${ roomId }/reservations?range[start]=${ date.startOf("day").format() }&range[end]=${ date.endOf("day").format() }`,
        init: {},
    },
});

/**
 * Initiate New Reservation Action
 */
export const initiateNewReservationAction = (date: Moment): TReservationActions => ({
    type: "@@maineve/INITIATE_NEW_RESERVATION",
    payload: {
        data: {
            date,
        },
    },
});

/**
 * Save Reservation Action
 *
 * @param data
 */
export const saveReservationAction = (data: IReservation): TReservationActions => ({
    type: "@@maineve/SAVE_RESERVATION",
    payload: {
        data,
    },
});

/**
 * Delete Reservation Action
 */
export const removeReservationAction = (data: IReservation, deleteAll: boolean): TReservationActions => ({
    type: "@@maineve/REMOVE_RESERVATION",
    payload: {
        data,
        deleteAll,
    },
});

/**
 * Set calendar or search source in state
 */
export const saveSourceInStateAction = (data: ICalendarSource | ISearchSource): TReservationActions => ({
    type: "@@maineve/SAVE_SOURCE_IN_STATE",
    payload: {
        data,
    },
});

/**
 * Reset Reservation Action
 */
export const resetReservationAction = (): TReservationActions => ({
    type: "@@maineve/RESET_RESERVATION",
});
