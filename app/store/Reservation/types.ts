import { Moment } from "moment";
import { IJsonResponse } from "../types";
import { IUser } from "../User/types";

export interface IReservationState {
    fetching: boolean;
    saving: boolean;
    deleting: boolean;
    reservationSource: ICalendarSource | ISearchSource;
    reservation: IReservation;
    reservations: IReservation[];
    timelineReservations: IReservation[];
    timelineReservationsLoading: boolean;
}

export interface IReservation {
    title: string;
    startdate: Moment;
    enddate: Moment;
    account_id?: number;
    conflicts?: boolean;
    created_at?: string;
    details?: string;
    id?: number;
    original_id?: number;
    recurrence?: string;
    recurrence_id?: string;
    room_id?: number;
    updated_at?: string;
    user_id?: number;
    users?: IUser[];
}

export interface IPostReservation {
    title: string;
    startdate: string;
    enddate: string;
    details: string;
    room_id: number;
    recurrence?: string;
    users?: number[];
}

export interface IPostRecurrence extends IPostReservation {
    original_enddate: string;
    original_startdate: string;
    original_id: number;
    recurrence_id: string;
}

export interface ICalendarSource {
    id: number;
    start: Moment;
    end: Moment;
    room_id: number;
    recurrence_id: string;
}

export interface ISearchSource extends ICalendarSource {
    recurrence?: string;
}

export const GET_RESERVATIONS_REQUEST = "@@maineve/GET_RESERVATIONS_REQUEST";
export const GET_RESERVATIONS_SUCCESS = "@@maineve/GET_RESERVATIONS_SUCCESS";
export const GET_RESERVATIONS_FAILURE = "@@maineve/GET_RESERVATIONS_FAILURE";

interface IGetReservationsRequest {
    type: typeof GET_RESERVATIONS_REQUEST;
}

interface IGetReservationsSuccess {
    type: typeof GET_RESERVATIONS_SUCCESS;
    payload: {
        json: IJsonResponse<IReservation[]>;
        response: Response;
    };
}

interface IGetReservationsFailure {
    type: typeof GET_RESERVATIONS_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const GET_RESERVATION_REQUEST = "@@maineve/GET_RESERVATION_REQUEST";
export const GET_RESERVATION_SUCCESS = "@@maineve/GET_RESERVATION_SUCCESS";
export const GET_RESERVATION_FAILURE = "@@maineve/GET_RESERVATION_FAILURE";

interface IGetReservationRequest {
    type: typeof GET_RESERVATION_REQUEST;
}

interface IGetReservationSuccess {
    type: typeof GET_RESERVATION_SUCCESS;
    payload: {
        json: IJsonResponse<IReservation>;
        response: Response;
    };
}

interface IGetReservationFailure {
    type: typeof GET_RESERVATION_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const POST_RESERVATION_REQUEST = "@@maineve/POST_RESERVATION_REQUEST";
export const POST_RESERVATION_SUCCESS = "@@maineve/POST_RESERVATION_SUCCESS";
export const POST_RESERVATION_FAILURE = "@@maineve/POST_RESERVATION_FAILURE";

interface IPostReservationRequest {
    type: typeof POST_RESERVATION_REQUEST;
}

interface IPostReservationSuccess {
    type: typeof POST_RESERVATION_SUCCESS;
    payload: {
        json: IJsonResponse<IReservation>;
        response: Response;
    };
}

interface IPostReservationFailure {
    type: typeof POST_RESERVATION_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const PUT_RESERVATION_REQUEST = "@@maineve/PUT_RESERVATION_REQUEST";
export const PUT_RESERVATION_SUCCESS = "@@maineve/PUT_RESERVATION_SUCCESS";
export const PUT_RESERVATION_FAILURE = "@@maineve/PUT_RESERVATION_FAILURE";

interface IPutReservationRequest {
    type: typeof PUT_RESERVATION_REQUEST;
}

interface IPutReservationSuccess {
    type: typeof PUT_RESERVATION_SUCCESS;
    payload: {
        json: IJsonResponse<IReservation>;
        response: Response;
    };
}

interface IPutReservationFailure {
    type: typeof PUT_RESERVATION_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const PUT_RECURRENCE_REQUEST = "@@maineve/PUT_RECURRENCE_REQUEST";
export const PUT_RECURRENCE_SUCCESS = "@@maineve/PUT_RECURRENCE_SUCCESS";
export const PUT_RECURRENCE_FAILURE = "@@maineve/PUT_RECURRENCE_FAILURE";

interface IPutRecurrenceRequest {
    type: typeof PUT_RECURRENCE_REQUEST;
}

interface IPutRecurrenceSuccess {
    type: typeof PUT_RECURRENCE_SUCCESS;
    payload: {
        json: IJsonResponse<IReservation>;
        response: Response;
    };
}

interface IPutRecurrenceFailure {
    type: typeof PUT_RECURRENCE_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const DELETE_RESERVATION_REQUEST = "@@maineve/DELETE_RESERVATION_REQUEST";
export const DELETE_RESERVATION_SUCCESS = "@@maineve/DELETE_RESERVATION_SUCCESS";
export const DELETE_RESERVATION_FAILURE = "@@maineve/DELETE_RESERVATION_FAILURE";

interface IDeleteReservationRequest {
    type: typeof DELETE_RESERVATION_REQUEST;
}

interface IDeleteReservationSuccess {
    type: typeof DELETE_RESERVATION_SUCCESS;
    payload: {
        response: Response;
    };
}

interface IDeleteReservationFailure {
    type: typeof DELETE_RESERVATION_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const DELETE_RECURRENCE_REQUEST = "@@maineve/DELETE_RECURRENCE_REQUEST";
export const DELETE_RECURRENCE_SUCCESS = "@@maineve/DELETE_RECURRENCE_SUCCESS";
export const DELETE_RECURRENCE_FAILURE = "@@maineve/DELETE_RECURRENCE_FAILURE";

interface IDeleteRecurrenceRequest {
    type: typeof DELETE_RECURRENCE_REQUEST;
}

interface IDeleteRecurrenceSuccess {
    type: typeof DELETE_RECURRENCE_SUCCESS;
    payload: {
        response: Response;
    };
}

interface IDeleteRecurrenceFailure {
    type: typeof DELETE_RECURRENCE_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const GET_TIMELINE_RESERVATIONS_REQUEST = "@@maineve/GET_TIMELINE_RESERVATIONS_REQUEST";
export const GET_TIMELINE_RESERVATIONS_SUCCESS = "@@maineve/GET_TIMELINE_RESERVATIONS_SUCCESS";
export const GET_TIMELINE_RESERVATIONS_FAILURE = "@@maineve/GET_TIMELINE_RESERVATIONS_FAILURE";

interface IGetTimelineReservationsRequest {
    type: typeof GET_TIMELINE_RESERVATIONS_REQUEST;
}

interface IGetTimelineReservationsSuccess {
    type: typeof GET_TIMELINE_RESERVATIONS_SUCCESS;
    payload: {
        json: IJsonResponse<IReservation[]>;
        response: Response;
    };
}

interface IGetTimelineReservationsFailure {
    type: typeof GET_TIMELINE_RESERVATIONS_FAILURE;
    payload: {
        json: IJsonResponse;
        response: Response;
    };
}

export const INITIATE_NEW_RESERVATION = "@@maineve/INITIATE_NEW_RESERVATION";

interface IInitiateNewReservation {
    type: typeof INITIATE_NEW_RESERVATION;
    payload: {
        data: {
            date: Moment;
        };
    };
}

export const INITIATE_POST_RESERVATION = "@@maineve/INITIATE_POST_RESERVATION";

interface IInitiatePostReservation {
    type: typeof INITIATE_POST_RESERVATION;
    payload: {
        data: IReservation;
    };
}

export const SAVE_SOURCE_IN_STATE = "@@maineve/SAVE_SOURCE_IN_STATE";

interface ISaveSourceInState {
    type: typeof SAVE_SOURCE_IN_STATE;
    payload: {
        data: ICalendarSource | ISearchSource;
    };
}

export const SAVE_RESERVATION = "@@maineve/SAVE_RESERVATION";

interface ISaveReservation {
    type: typeof SAVE_RESERVATION;
    payload: {
        data: IReservation;
    };
}

export const REMOVE_RESERVATION = "@@maineve/REMOVE_RESERVATION";

interface IRemoveReservation {
    type: typeof REMOVE_RESERVATION;
    payload: {
        data: IReservation;
        deleteAll: boolean;
    };
}

export const RESET_RESERVATION = "@@maineve/RESET_RESERVATION";

interface IResetReservation {
    type: typeof RESET_RESERVATION;
}

export type TReservationActions =
    IGetReservationsRequest | IGetReservationsSuccess | IGetReservationsFailure |
    IGetReservationRequest | IGetReservationSuccess | IGetReservationFailure |
    IPostReservationRequest | IPostReservationSuccess | IPostReservationFailure |
    IPutReservationRequest | IPutReservationSuccess | IPutReservationFailure |
    IPutRecurrenceRequest | IPutRecurrenceSuccess | IPutRecurrenceFailure |
    IDeleteReservationRequest | IDeleteReservationSuccess | IDeleteReservationFailure |
    IDeleteRecurrenceRequest | IDeleteRecurrenceSuccess | IDeleteRecurrenceFailure |
    IGetTimelineReservationsRequest | IGetTimelineReservationsSuccess | IGetTimelineReservationsFailure |
    IInitiateNewReservation | IResetReservation | ISaveReservation |
    IRemoveReservation | ISaveSourceInState | IInitiatePostReservation;

export type TReservationApiActions =
    typeof GET_RESERVATIONS_REQUEST | typeof GET_RESERVATIONS_SUCCESS | typeof GET_RESERVATIONS_FAILURE |
    typeof GET_TIMELINE_RESERVATIONS_REQUEST | typeof GET_TIMELINE_RESERVATIONS_SUCCESS | typeof GET_TIMELINE_RESERVATIONS_FAILURE |
    typeof GET_RESERVATION_REQUEST | typeof GET_RESERVATION_SUCCESS | typeof GET_RESERVATION_FAILURE |
    typeof POST_RESERVATION_REQUEST | typeof POST_RESERVATION_SUCCESS | typeof POST_RESERVATION_FAILURE |
    typeof PUT_RESERVATION_REQUEST | typeof PUT_RESERVATION_SUCCESS | typeof PUT_RESERVATION_FAILURE |
    typeof PUT_RECURRENCE_REQUEST | typeof PUT_RECURRENCE_SUCCESS | typeof PUT_RECURRENCE_FAILURE |
    typeof DELETE_RESERVATION_REQUEST | typeof DELETE_RESERVATION_SUCCESS | typeof DELETE_RESERVATION_FAILURE |
    typeof DELETE_RECURRENCE_REQUEST | typeof DELETE_RECURRENCE_SUCCESS | typeof DELETE_RECURRENCE_FAILURE;
