/**
 * Is the Fetch response a JSON response
 */
export const isJsonResponse = (response: Response): boolean => {

    const header = "Content-Type";
    const type = "application/json";

    return response.headers.get(header) && response.headers.get(header).indexOf(type) !== -1;
};
