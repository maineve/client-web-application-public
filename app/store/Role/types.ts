export interface IRole {
    created_at: string;
    id: number;
    name: ERoles;
    updated_at: string;
}

export enum ERoles {
    user = "user",
    admin = "admin",
    owner = "owner",
}
