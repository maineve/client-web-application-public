import { Middleware } from "redux";
import { IAppState, API_REQUEST, IApiRequest, TAppActions } from "./types";
import { isJsonResponse } from "./utils";

/**
 * Handle API Request Actions
 */
const apiRequest: Middleware<{}, IAppState> = store => next => async (action): Promise<void> => {

    if (action.type !== API_REQUEST) {
        return next(action);
    }

    const result = next(action);
    const typedAction = action as IApiRequest<TAppActions>;

    const {
        request,
        success,
        failure,
        endpoint,
        baseurl,
        payload,
    } = typedAction.meta;

    const init = {
        ...typedAction.meta.init,
        headers: {
            ...typedAction.meta.init.headers,
            Authorization: localStorage.getItem("token"),
        },
    };

    try {

        store.dispatch({ type: request, payload });

        const response = await fetch(`${ baseurl || window["__env"].apiUrl }${ endpoint }`, init);
        const json = isJsonResponse(response) ? await response.json() : null;

        if (!response.ok) {
            store.dispatch({ type: failure, payload: { ...payload || {}, json, response } });
            throw Error(response.statusText);
        }

        store.dispatch({ type: success, payload: { ...payload || {}, json, response } });

    } catch (error) {

        console.error(error);

    }

    return result;
};

export const appMiddleware: Middleware[] = [
    apiRequest,
];
