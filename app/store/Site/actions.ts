import { TSiteActions } from "./types";

/**
 * Set application to initialized
 */
export const setApplicationInitializedAction = (): TSiteActions => ({
    type: "@@maineve/SET_APPLICATION_INITIALIZED",
});

/**
 * Set application to uninitialized
 */
export const setApplicationUninitializedAction = (): TSiteActions => ({
    type: "@@maineve/SET_APPLICATION_UNINITIALIZED",
});

/**
 * Set application to locked
 */
export const setApplicationLockedAction = (): TSiteActions => ({
    type: "@@maineve/SET_APPLICATION_LOCKED",
});

/**
 * Set application to unlocked
 */
export const setApplicationActiveAction = (): TSiteActions => ({
    type: "@@maineve/SET_APPLICATION_ACTIVE",
});
