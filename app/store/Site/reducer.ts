import { ISiteState, TSiteActions } from "./types";
import { Reducer } from "redux";

export const initState: ISiteState = {
    initialized: false,
    locked: null,
};

export const siteReducer: Reducer<ISiteState, TSiteActions> = (state = initState, action) => {

    switch (action.type) {

        case "@@maineve/SET_APPLICATION_INITIALIZED":
            return {
                ...state,
                initialized: true,
            };
        case "@@maineve/SET_APPLICATION_UNINITIALIZED":
            return {
                ...state,
                initialized: false,
            };
        case "@@maineve/SET_APPLICATION_LOCKED":
            return {
                ...state,
                locked: true,
            };
        case "@@maineve/SET_APPLICATION_ACTIVE":
            return {
                ...state,
                locked: false,
            };
        default:
            return state;

    }

};
