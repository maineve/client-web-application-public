export interface ISiteState {
    initialized: boolean;
    locked: boolean;
}

export const SET_APPLICATION_INITIALIZED = "@@maineve/SET_APPLICATION_INITIALIZED";

interface ISetApplicationInitialized {
    type: typeof SET_APPLICATION_INITIALIZED;
}


export const SET_APPLICATION_UNINITIALIZED = "@@maineve/SET_APPLICATION_UNINITIALIZED";

interface ISetApplicationUninitialized {
    type: typeof SET_APPLICATION_UNINITIALIZED;
}

export const SET_APPLICATION_LOCKED = "@@maineve/SET_APPLICATION_LOCKED";

interface ISetApplicationLocked {
    type: typeof SET_APPLICATION_LOCKED;
}

export const SET_APPLICATION_ACTIVE = "@@maineve/SET_APPLICATION_ACTIVE";

interface ISetApplicationActive {
    type: typeof SET_APPLICATION_ACTIVE;
}

export type TSiteActions =
    ISetApplicationInitialized | ISetApplicationUninitialized | ISetApplicationLocked |
    ISetApplicationActive;
