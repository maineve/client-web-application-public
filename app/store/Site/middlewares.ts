import { Middleware } from "redux";
import { IAppState } from "../types";
import { setApplicationInitializedAction, setApplicationUninitializedAction } from "./actions";

/**
 * Adds authorization token to local storage
 */
const initializeApplication: Middleware<{}, IAppState> = store => next => (action): void => {

    const result = next(action);

    const authState = store.getState().auth;
    const siteState = store.getState().site;
    const userState = store.getState().user;

    // Make sure local storage has profile after login
    const localStorageProfile = localStorage.getItem("profile");

    if (!siteState.initialized) {

        const initAuthenticated =
                authState.authenticated &&
                userState.profile.data &&
                siteState.locked !== null &&
                localStorageProfile !== null;

        const initUnauthenticated =
                authState.authenticated === false;

        if (initAuthenticated || initUnauthenticated) {
            store.dispatch(setApplicationInitializedAction());
        }
    }

    return result;
};

export const siteMiddleware: Middleware[] = [
    initializeApplication,
];
