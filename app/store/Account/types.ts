export interface IAccount {
    active_until: string;
    created_at: string;
    id: number;
    name: string;
    stripe_customer_token: string;
    subbed: boolean;
    timezone: string;
    updated_at: string;
}
