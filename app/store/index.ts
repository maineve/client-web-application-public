import { createStore, Middleware, applyMiddleware, combineReducers } from "redux";
import { logger } from "redux-logger";
import { reducer as reduxFormReducer } from "redux-form";
import { connectRouter } from "connected-react-router";
import { createBrowserHistory } from "history";

import { appMiddleware } from "./middlewares";
import { authReducer } from "./Auth/reducer";
import { authMiddleware } from "./Auth/middlewares";
import { roomReducer } from "./Room/reducer";
import { buildingReducer } from "./Building/reducer";
import { reservationReducer } from "./Reservation/reducer";
import { reservationMiddleware } from "./Reservation/middlewares";
import { siteReducer } from "./Site/reducer";
import { siteMiddleware } from "./Site/middlewares";
import { userReducer } from "./User/reducer";
import { userMiddleware } from "./User/middlewares";
import { searchReducer } from "./Search/reducer";

export const storeHistory = createBrowserHistory({ basename: "/" });

const reducers = {
    router: connectRouter(storeHistory),
    form: reduxFormReducer,
    auth: authReducer,
    building: buildingReducer,
    room: roomReducer,
    reservation: reservationReducer,
    user: userReducer,
    search: searchReducer,
    site: siteReducer,
};

const middlewares: Middleware[] = [
    ...appMiddleware,
    ...reservationMiddleware,
    ...authMiddleware,
    ...siteMiddleware,
    ...userMiddleware,
];

if (process.env.NODE_ENV !== "production") {
    middlewares.push(logger);
}

export const store = createStore(combineReducers(reducers), applyMiddleware(...middlewares));
