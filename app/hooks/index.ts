import { useEffect, useRef } from "react";

/**
 * usePrevious
 * Creating this function until react implements it.
 * https://reactjs.org/docs/hooks-faq.html#how-to-get-the-previous-props-or-state
 * @param value
 */
export const usePrevious = <T>(value: T): T => {

    const ref = useRef<T>(null);

    useEffect(() => {
      ref.current = value;
    });

    return ref.current;
};
