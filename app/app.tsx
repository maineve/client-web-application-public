import React from "react";
import ReactDom from "react-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
import { Routes } from "./routes";
import { store, storeHistory } from "./store";
import { default as AppContainer } from "./react/Generic/containers/AppContainer";

import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";

import "./assets/images/layout/dropdown-arrow.svg";
import "./assets/images/icons/favicon.ico";

ReactDom.render(
    <Provider store={ store }>
        <ConnectedRouter history={ storeHistory }>
            <AppContainer>
                <Routes />
            </AppContainer>
        </ConnectedRouter>
    </Provider>,
    document.querySelector("#maineve-app")
);
