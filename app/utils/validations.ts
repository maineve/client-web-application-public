import { required as requiredValidator } from "redux-form-validators";

// Error messages
const requiredMessage = "This field is required.";
const passwordStrengthMessage = "Password entered is not strong enough.";

// The Field is required.
export const required = requiredValidator({ message: requiredMessage });

export const passwordStrengthRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])([!@#$%^&*]?)(?=.{8,})/;

export const passwordStrength = (value: string): string => (
    !value.match(passwordStrengthRegex) ? passwordStrengthMessage : undefined
);
