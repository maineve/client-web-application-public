/**
 * Use with Async/Await to delay function execution.
 */
export const sleep = (milliseconds: number): Promise<{}> => (
    new Promise(resolve => setTimeout(resolve, milliseconds))
);
