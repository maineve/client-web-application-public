import { sortBy } from "lodash";
import { ISelectOption } from "../react/Generic/wrappers/SelectField";

/**
 * Map React Select Options
 */
export const mapReactSelectOptions = <T>(options: T[], label: string, value: string, sort = true): ISelectOption[] => {
    let mappedOptions: ISelectOption[] = [];

    options.forEach(option => {
        mappedOptions.push({ label: option[label], value: option[value] });
    });

    if (sort) {
        mappedOptions = sortBy(mappedOptions, o => o.label);
    }

    return mappedOptions;
};

/**
 * Decode JWT
 */
export const jwtDecode = (token: string): object => {
    if (token) {
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }
};
