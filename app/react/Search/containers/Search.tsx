import React, { useState, Fragment } from "react";
import { connect } from "react-redux";
import { default as moment } from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { default as Reservation } from "../../Reservation/containers/Reservation";
import { ISearchState } from "../../../store/Search/types";
import { getSearchAction, resetSearchResultsAction } from "../../../store/Search/actions";
import { IAppState } from "../../../store/types";
import { Portal } from "../../Generic/components/Portal";
import { SearchModal } from "../components/SearchModal";
import { IReservation, ISearchSource } from "../../../store/Reservation/types";
import { Overlay } from "../../Generic/components/Overlay";
import "../style.scss";

interface IStateProps {
    search: ISearchState;
}

interface IDispatchProps {
    getSearch: typeof getSearchAction;
    resetSearchResults: typeof resetSearchResultsAction;
}

type TProps = IStateProps & IDispatchProps;

let timeout: number;

const Search: React.FC<TProps> = (props) => {

    const { search, getSearch, resetSearchResults } = props;

    const [showModal, setShowModal] = useState<boolean>(false);
    const [showReservation, setShowReservation] = useState<boolean>(false);
    const [searchTimeout, setSearchTimeout] = useState<boolean>(false);
    const [searchSource, setSearchSource] = useState<ISearchSource>(null);

    const handleToggleModal = (): void => {
        setShowModal(!showModal);
    };

    const handleSearchClick = (event: React.MouseEvent<HTMLAnchorElement>): void => {

        event.preventDefault();

        setShowModal(true);
        resetSearchResults();
    };

    const handleSearch = (query: string): void => {

        clearTimeout(timeout);
        setSearchTimeout(true);

        if (query.length === 0) {
            setSearchTimeout(false);
            resetSearchResults();
        }

        if (query.length > 0) {
            timeout = window.setTimeout(() => {
                getSearch(query);
                setSearchTimeout(false);
            }, 500);
        }
    };

    const handleShowReservation = (reservation: IReservation): void => {

        console.log(reservation);

        const source: ISearchSource = {
            id: reservation.id || reservation.original_id,
            start: moment(reservation.startdate),
            end: moment(reservation.enddate),
            room_id: reservation.room_id,
            recurrence: reservation.recurrence,
            recurrence_id: reservation.recurrence_id,
        };

        setShowReservation(true);
        setSearchSource(source);
    };

    const handleHideReservation = (): void => {
        setShowReservation(false);
        setSearchSource(null);
    };

    return (
        <div className="search">

            <a className="search__icon" href="#" onClick={ handleSearchClick }>
                <FontAwesomeIcon icon={ faSearch } />
            </a>

            <Portal id="maineve-app" element="div">
                {
                    showModal

                    &&

                    <SearchModal
                        search={ search }
                        onSearch={ handleSearch }
                        toggleModal={ handleToggleModal }
                        toggleReservation={ handleShowReservation }
                        timeout={ searchTimeout }
                    />
                }

                {
                    showReservation

                    &&

                    <Fragment>
                        <Overlay />
                        <Reservation
                            source={ searchSource }
                            closeReservationForm={ handleHideReservation }
                        />
                    </Fragment>
                }
            </Portal>

        </div>
    );
};

export default connect<IStateProps, IDispatchProps, {}, IAppState>(
    (state) => ({
        search: state.search,
    }),
    {
        getSearch: getSearchAction,
        resetSearchResults: resetSearchResultsAction,
    },
)(Search);
