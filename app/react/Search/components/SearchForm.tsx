import React, { useEffect, useRef, useState, Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faAngleDown, faAngleUp } from "@fortawesome/free-solid-svg-icons";

interface IProps {
    query: string;
    toggleModal: () => void;
    onChange: (query: string) => void;
}

export const SearchForm: React.FC<IProps> = (props) => {

    const { query, toggleModal, onChange } = props;

    const [inputFocused, setInputFocused] = useState<boolean>(false);
    const [showSearchOptions, setShowSearchOptions] = useState<boolean>(false);
    const inputRef = useRef<HTMLInputElement>(null);

    useEffect(() => {
        inputRef.current.focus();
    }, []);

    const toggleInputFocus = (): void => {
        setInputFocused(!inputFocused);
    };

    const toggleSearchOptions = (): void => {
        setShowSearchOptions(!showSearchOptions);
    };

    const handleCancelClick = (event: React.MouseEvent<HTMLAnchorElement>): void => {
        event.preventDefault();
        toggleModal();
    };

    const handleFocus = (): void => {
        inputRef.current.select();
        toggleInputFocus();
    };

    const handleBlur = (): void => {
        toggleInputFocus();
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
        event.preventDefault();
    };

    const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        onChange(event.currentTarget.value);
    };

    return (
        <div className="search-form">
            <form className="search-form__form" onSubmit={ handleSubmit }>
                <div className="search-form__input-wrapper">
                    <div className={ `search-form__input-icon ${ inputFocused && `search-form__input-icon--focused` }` }>
                        <FontAwesomeIcon icon={ faSearch } />
                    </div>
                    <input
                        value={ query }
                        ref={ inputRef }
                        className="search-form__input"
                        type="text"
                        onFocus={ handleFocus }
                        onBlur={ handleBlur }
                        onChange={ handleOnChange }
                    />
                    <a className="search-form__cancel-btn" role="button" href="#" onClick={ handleCancelClick }>
                        <span>Cancel</span>
                    </a>
                </div>
            </form>
        </div>
    );
};
