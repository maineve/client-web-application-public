import React from "react";
import { SearchResult } from "./SearchResult";
import { IReservation } from "../../../store/Reservation/types";

interface IProps {
    query: string;
    loading: boolean;
    selectedIndex: number;
    results: IReservation[];
    toggleReservation: (reservation: IReservation) => void;
}

export const SearchResults: React.FC<IProps> = (props) => {

    const { query, results, selectedIndex, loading, toggleReservation } = props;

    const handleResultClick = (reservation: IReservation): void => {
        toggleReservation(reservation);
    };

    return (
        <div className="search-results">
            <hr className="search-results__hr" />

            { (query.length === 0 && results.length === 0 && !loading) &&
                <div className="search-results__help">
                    <p>To begin a searching, enter the title of the booking you&rsquo;re looking for.</p>
                    <p>Advanced searches are available with the <strong>title</strong> and <strong>date</strong> keywords.</p>
                    <p>Advanced searches might look like:</p>
                    <ul>
                        <li>&lsquo;title: My Booking&rsquo;</li>
                        <li>&lsquo;date: May 2020&rsquo;</li>
                        <li>&lsquo;title: My Booking date: May 2020&rsquo;</li>
                    </ul>
                </div>
            }

            { (query.length > 0 && results.length === 0 && !loading) &&
                <div className="search-results__help">
                    <p>No results found for your search. 😞</p>
                </div>
            }

            { loading &&
                <div className="loader loader--search ball-pulse">
                    <div />
                    <div />
                    <div />
                </div>
            }

            { (query.length > 0 && results.length > 0 && !loading) &&
                results.slice(0, 8).map((result: IReservation, index: number) => (
                    <SearchResult
                        key={ `${ result.id }-${ index }` }
                        result={ result }
                        className={ index === selectedIndex && `search-result--selected` }
                        onResultClick={ handleResultClick }
                    />
                ))
            }

        </div>
    );
};
