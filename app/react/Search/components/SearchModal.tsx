import React, { useState, useEffect, useCallback } from "react";
import { SearchForm } from "./SearchForm";
import { SearchResults } from "./SearchResults";
import { ISearchState } from "../../../store/Search/types";
import { IReservation } from "../../../store/Reservation/types";

interface IProps {
    search: ISearchState;
    timeout: boolean;
    toggleModal: () => void;
    toggleReservation: (reservation: IReservation) => void;
    onSearch: (query: string) => void;
}

export const SearchModal: React.FC<IProps> = (props) => {

    const { toggleModal, onSearch, search, timeout, toggleReservation } = props;

    const [queryState, setQueryState] = useState<string>("");
    const [selectedIndex, setSelectedIndex] = useState<number>(0);

    const handleKeyPress = useCallback(
        (event: KeyboardEvent): void => {

            if (event.key === "ArrowUp") {
                const prevIndex = selectedIndex > 0 ? selectedIndex - 1 : selectedIndex;
                setSelectedIndex(prevIndex);
            }

            if (event.key === "ArrowDown") {
                const nextIndex = selectedIndex < search.results.data.length - 1 ? selectedIndex + 1 : selectedIndex;
                setSelectedIndex(nextIndex);
            }

            if (event.key === "Enter") {
                const selectedElement = document.getElementsByClassName("search-result--selected")[0] as HTMLDivElement;
                selectedElement.click();
            }
        },
        [search, selectedIndex]
    );

    useEffect(() => {

        window.addEventListener("keydown", handleKeyPress);

        return (): void => {
            window.removeEventListener("keydown", handleKeyPress);
        };
    }, [handleKeyPress]);

    const handleOnChange = (query: string): void => {
        setQueryState(query);
        onSearch(query);
        setSelectedIndex(0);
    };

    return (
        <div className="search-modal">
            <div className="search-modal__inner">

                <SearchForm
                    query={ queryState }
                    toggleModal={ toggleModal }
                    onChange={ handleOnChange }
                />

                <SearchResults
                    query={ queryState }
                    selectedIndex={ selectedIndex }
                    results={ search.results.data }
                    loading={ timeout || search.results.loading }
                    toggleReservation={ toggleReservation }
                />

            </div>
        </div>
    );
};
