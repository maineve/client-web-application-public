import React from "react";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookOpen, faSync } from "@fortawesome/free-solid-svg-icons";
import { IReservation } from "../../../store/Reservation/types";

interface IProps {
    result: IReservation;
    className?: string;
    onResultClick: (reservation: IReservation) => void;
}

export const SearchResult: React.FC<IProps> = (props) => {

    const { result, className, onResultClick } = props;

    const subTitle = `${ moment(result.startdate).utc().format(`MMMM Do, YYYY`) } — ${ moment(result.startdate).utc().format(`h:mma`) } to ${ moment(result.enddate).utc().format(`h:mma`) }`;

    const handleClick = (): void => {
        onResultClick(result);
    };

    return (
        <div className={ `search-result ${ className || `` }` } onClick={ handleClick }>
            <div className="search-result__icon">
                <FontAwesomeIcon icon={ faBookOpen } />
            </div>
            <div className="search-result__details">
                <h2 className="search-result__header">
                    <span className="search-result__header-span">{ result.title }</span>
                    { result.original_id &&
                        <div className="search-result__header-icon">
                            <FontAwesomeIcon icon={ faSync } />
                        </div>
                    }
                </h2>
                <span className="search-result__time">{ subTitle }</span>
            </div>
        </div>
    );
};
