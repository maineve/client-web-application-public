import React from "react";
import { AngularWidget } from "../../Generic/components/AngularWidget";
import { Layout } from "../../Generic/components/Layout";
import { SubscriptionTemplate } from "../templates/SubscriptionTemplate";

class Subscription extends React.Component {

    public render(): JSX.Element {

        return (
            <Layout>
                <AngularWidget
                    template={ SubscriptionTemplate }
                />
            </Layout>
        );
    }
}

export default Subscription;
