export const SubscriptionTemplate = `
    <section data-ng-controller="subscriptionController as subscriptionCtrl" data-ng-init="subscriptionCtrl.initiate()">

        <div class="loader-wrapper loader-wrapper--opac" data-ng-show="!subscriptionCtrl.getInitiated()">
            <div class="loader ball-pulse">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>

        <div data-ng-if="subscriptionCtrl.getInitiated()">

            <div class="header">
                <div class="header__left">
                    <a class="icon-links icon-links--button" data-ng-click="subscriptionCtrl.editAccountInfo()">
                        <span class="icon-links__text icon-links__text--show"><i class="fas fa-edit"></i> Edit Account Info</span>
                    </a>
                    <a class="icon-links icon-links--button" data-ng-click="subscriptionCtrl.addSubscription()" data-ng-show="!subscriptionCtrl.subService.hasSub(subscriptionCtrl.account)">
                        <span class="icon-links__text icon-links__text--show"><i class="fas fa-plus-circle"></i> Setup Subscription</span>
                    </a>
                </div>
                <div class="header__center">
                    <h1 class="section-title">Subscription &amp; Payment</h1>
                </div>
                <div class="header__right">

                </div>
            </div>

            <div class="subscription">
                <div class="subscription-details" data-ng-hide="subscriptionCtrl.subService.hasSub(subscriptionCtrl.account)">
                    <h2>Your Account Status</h2>
                    <p data-ng-show="subscriptionCtrl.subService.trialing(subscriptionCtrl.account)">
                        You do not have an active subscription. Your 15 day trial period expires
                        on {{ subscriptionCtrl.account.account.active_until | date: 'longDate' }}. <a data-ng-click="subscriptionCtrl.addSubscription()">Setup a subscription</a>.
                    </p>

                    <p data-ng-show="subscriptionCtrl.subService.trialExpired(subscriptionCtrl.account)">
                        Subscribe to a monthly plan to continue using Maineve.
                    </p>
                </div>

                <div class="subscription-details" data-ng-show="subscriptionCtrl.subService.hasSub(subscriptionCtrl.account)" data-ng-repeat="sub in subscriptionCtrl.account.stripe.subscriptions.data">
                    <div class="subscription-details__chunk">
                        <h2>Your Subscription</h2>
                        <p data-ng-hide="subscriptionCtrl.subService.canceled(subscriptionCtrl.account)">
                            You are subscribed to the Maineve. Your account will be charged<br />
                            <strong>{{ (sub.plan.amount * 0.01) | currency }} Per Space</strong> each month while
                            the subscription is active.<br /> Your next payment for <strong>{{ subscriptionCtrl.account.upcoming.amount_due / 100 | currency }}</strong>
                            will be charged on <strong>{{ (sub.current_period_end * 1000) | date: 'longDate' }}</strong>.
                        </p>

                        <p data-ng-show="subscriptionCtrl.subService.canceled(subscriptionCtrl.account)">
                            You have cancelled your subscription. Your account will remain active until the end of the current
                            billing period. <a data-ng-click="subscriptionCtrl.reactivateSubscription()">Re-activate Subscription</a>.
                        </p>
                    </div>

                    <!--<div class="subscription-details__chunk">
                        <h3>Account Owners</h3>
                        <ul>
                            <li>Adam Brown</li>
                        </ul>
                    </div>-->

                    <div class="subscription-details__chunk">
                        <h3>Status</h3>
                        <ul>
                            <li>
                                <span class="subscription-details__status subscription-details__status--active">
                                    <figure class="fas fa-check-circle"></figure> {{ sub.status }}
                                </span>
                            </li>
                        </ul>
                    </div>

                    <div class="subscription-details__chunk">
                        <h3>Card Info.</h3>
                        <ul data-ng-show="subscriptionCtrl.account.stripe.sources" data-ng-repeat="card in subscriptionCtrl.account.stripe.sources.data">
                            <li class="icon-links icon-links--cc fab fa-cc-{{ card.brand | lowercase }}"><span class="icon-links__text">{{ card.brand }}</span></li>
                            <li>Card: **** **** **** {{ card.last4 }}</li>
                            <li>Expiry: {{ card.exp_month }} / {{ card.exp_year }}</li>
                        </ul>
                    </div>

                    <div class="subscription-details__chunk" data-ng-hide="subscriptionCtrl.subService.canceled(subscriptionCtrl.account)">
                        <h3>Subscription Options</h3>
                        <ul data-ng-show="subscriptionCtrl.account.stripe.sources" data-ng-repeat="card in subscriptionCtrl.account.stripe.sources.data">
                            <li><a href="" data-ng-click="subscriptionCtrl.addSubscription()">Update Credit Card Information</a></li>
                            <li><a href="" data-ng-click="subscriptionCtrl.cancelSubscription()">Cancel Subscription</a></li>
                        </ul>
                    </div>
                </div>

                <div class="payment-history">
                    <h2>Payment History</h2>
                    <ul>
                        <li class="payment-history__li" data-ng-repeat="invoice in subscriptionCtrl.account.invoices.data">
                            <div class="payment-history__col payment-history__col--date">{{ (invoice.created * 1000) | date: 'longDate' }}</div>
                            <div class="payment-history__col">{{ (invoice.total * 0.01) | currency }} <span class="cap">{{ invoice.currency }}</span></div>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </section>
`;
