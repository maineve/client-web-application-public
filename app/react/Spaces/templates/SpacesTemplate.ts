export const SpacesTemplate = `
    <section data-ng-controller="buildingController as buildingCtrl" data-ng-init="buildingCtrl.spacesInit()">

        <div class="loader-wrapper loader-wrapper--opac" data-ng-show="!buildingCtrl.getInitiated()">
            <div class="loader ball-pulse">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>

        <div data-ng-if="buildingCtrl.getInitiated()">

            <div class="header">
                <div class="header__left">
                    <a class="icon-links icon-links--button" role="button" data-ng-click="buildingCtrl.newBuildingForm()">
                        <span class="icon-links__text icon-links__text--show"><i class="fas fa-plus-circle"></i> Add building</span>
                    </a>
                </div>
                <div class="header__center">
                    <h1 class="section-title">Manage Spaces</h1>
                </div>
            </div>
            <ul class="list">
                <li class="list__item" data-ng-repeat="building in buildingCtrl.buildings | orderBy: 'name'">
                    <div data-mve-dropdown="dropdownmenu" data-mve-dropdown-toggle="list__item-title" data-mve-dropdown-in-list="true">
                        <div class="list__item-title-wrapper">
                            <h2 class="list__item-title list__item-title--link">{{ building.name }} <i class="fas fa-angle-down"></i></h2>
                            <div class="dropdownmenu dropdownmenu--spaces">
                                <img class="dropdownmenu__arrow" src="assets/dropdown-arrow.svg" width="18" height="7">
                                <ul class="dropdownmenu__list">
                                <li class="dropdownmenu__list-item dropdownmenu__list-item--border-bottom"><a data-ng-controller="roomController as roomCtrl" data-ng-click="roomCtrl.newRoomForm(building)" class="dropdownmenu__list-link">Add a space</a></li>
                                <li class="dropdownmenu__list-item"><a data-ng-click="buildingCtrl.editBuildingForm(building.id)" class="dropdownmenu__list-link">Edit</a></li>
                                <li class="dropdownmenu__list-item"><a data-ng-click="buildingCtrl.deleteBuilding(building.id)" class="dropdownmenu__list-link">Delete</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <ul class="sub-list" data-ng-show="building.rooms.length > 0">
                        <li class="sub-list__item" data-ng-repeat="room in building.rooms | orderBy: 'name'">
                            <span class="sub-list__item-title">{{ room.name }}</span>
                            <div class="sub-list__item-options">
                                <span class="sub-list__item-option"><a class="fas fa-edit" title="Edit Room" data-ng-controller="roomController as roomCtrl" data-ng-click="roomCtrl.editRoomForm(room); $event.stopPropagation()"><span class="icon-links__text">Edit</span></a></span>
                                <span class="sub-list__item-option"><a class="fas fa-trash-alt" title="Delete Room" data-ng-controller="roomController as roomCtrl" data-ng-click="roomCtrl.deleteRoom(room.id, building.id); $event.stopPropagation()"><span class="icon-links__text">Delete</span></a></span>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </section>
`;
