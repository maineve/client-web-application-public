import React from "react";
import { AngularWidget } from "../../Generic/components/AngularWidget";
import { Layout } from "../../Generic/components/Layout";
import { SpacesTemplate } from "../templates/SpacesTemplate";

class Spaces extends React.Component {

    public render(): JSX.Element {

        return (
            <Layout>
                <AngularWidget
                    template={ SpacesTemplate }
                />
            </Layout>
        );
    }
}

export default Spaces;
