import React from "react";
import { connect } from "react-redux";
import { AngularWidget } from "../../Generic/components/AngularWidget";
import { Layout } from "../../Generic/components/Layout";
import { SubscriptionTemplate } from "../../Subscription/templates/SubscriptionTemplate";
import { LockedTemplate } from "../templates/LockedTemplate";
import { IAppState } from "app/store/types";
import { IUserState } from "app/store/User/types";

import "../../../assets/images/logo/green.svg";

interface IStateProps {
    userState: IUserState;
}

type TProps = IStateProps;

class Locked extends React.Component<TProps> {

    public render(): JSX.Element {

        const { profile } = this.props.userState;
        const { users_accounts: userAccounts } = this.props.userState.profile.data;

        const currentAccount = userAccounts.find(ua => ua.account_id === profile.data.current_account_id);
        const template = currentAccount.role_id === 1 ? SubscriptionTemplate : LockedTemplate;

        return (
            <Layout>
                <AngularWidget
                    template={ template }
                />
            </Layout>
        );
    }
}

export default connect<IStateProps, {}, {}, IAppState>(
    (state) => ({
        userState: state.user,
    })
)(Locked);
