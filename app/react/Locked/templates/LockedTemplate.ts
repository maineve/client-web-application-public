export const LockedTemplate = `
    <div class="account-locked">
        <div class="account-locked__box">
            <img class="account-locked__logo" src="assets/green.svg" width="50" alt="">
            <h1 class="account-locked__title">Account Locked</h1>
            <p class="account-locked__desc">This Maineve account does not have an active subscription.  Please get in touch with your account owner for more information.</p>
            <p class="account-locked__desc">If you're having unforeseen issues with your account, you can always get in touch with us at <a href="mailto:support@maineve.com">support@maineve.com</a>.</p>
        </div>
    </div>
`;
