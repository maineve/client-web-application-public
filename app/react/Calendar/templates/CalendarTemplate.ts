export const CalendarTemplate = `
    <div data-ng-controller="dashboardController as dashboardCtrl" data-ng-init="dashboardCtrl.calendarInit()">

        <div class="loader-wrapper loader-wrapper--opac" data-ng-show="!dashboardCtrl.getInitiated()">
            <div class="loader ball-pulse">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>

        <div data-ng-if="dashboardCtrl.getInitiated()">

            <div class="calendar-header">

                <div class="calendar-header__left">
                    <div class="calendar-header__dropdown" data-mve-dropdown="dropdownmenu" data-mve-dropdown-toggle="calendar-header__choose-format">
                        <span class="calendar-header__choose-format">
                            <a class="icon-links icon-links--button far fa-calendar-alt">
                                <span class="icon-links__text">Calendar View</span>
                            </a>
                        </span>
                        <div class="dropdownmenu dropdownmenu--calendar-view">
                            <img class="dropdownmenu__arrow" src="assets/dropdown-arrow.svg" width="18" height="7">
                            <ul class="dropdownmenu__list">
                                <li class="dropdownmenu__list-item">
                                    <a data-ng-click="dashboardCtrl.dashboardCal.calendar.changeView('month')" data-ng-class="{ 'dropdownmenu__list-link--active' : dashboardCtrl.whichCalView() === 'month' }" class="dropdownmenu__list-link">Month</a>
                                </li>
                                <li class="dropdownmenu__list-item">
                                    <a data-ng-click="dashboardCtrl.dashboardCal.calendar.changeView('basicWeek')" data-ng-class="{ 'dropdownmenu__list-link--active' : dashboardCtrl.whichCalView() === 'basicWeek' }" class="dropdownmenu__list-link">Week</a>
                                </li>
                                <li class="dropdownmenu__list-item">
                                    <a data-ng-click="dashboardCtrl.dashboardCal.calendar.changeView('basicDay')" data-ng-class="{ 'dropdownmenu__list-link--active' : dashboardCtrl.whichCalView() === 'basicDay' }" class="dropdownmenu__list-link">Day</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="calendar-header__dropdown" data-mve-dropdown="dropdownmenu" data-mve-dropdown-toggle="calendar-header__choose-format">

                        <span class="calendar-header__choose-format">
                            <a class="icon-links icon-links--button fas fa-filter" data-ng-class="{ 'icon-links--active': dashboardCtrl.activeFilter() }">
                                <span class="icon-links__text">Filter</span>
                            </a>
                        </span>

                        <div class="dropdownmenu dropdownmenu--calendar-filter">
                            <img class="dropdownmenu__arrow" src="assets/dropdown-arrow.svg" width="18" height="7">
                            <ul class="dropdownmenu__list">
                                <li class="dropdownmenu__list-item dropdownmenu__list-item--border-bottom dropdown__list-item--dont-close">
                                    <select
                                        class="select__select"
                                        data-mve-select-two
                                        data-width="100%"
                                        data-theme="dropdown"
                                        data-minimum-results-for-search="-1"
                                        data-ng-model="dashboardCtrl.selectedRoom"
                                        data-ng-options="room.id as room.name for room in dashboardCtrl.rooms"
                                        data-ng-change="dashboardCtrl.getCurrentReservations()">
                                        <option value="">Filter By Room</option>
                                    </select>
                                </li>
                                <li class="dropdownmenu__list-item">
                                    <a class="dropdownmenu__list-link" data-ng-click="dashboardCtrl.clearFilters()">Clear all filters</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="calendar-header__dropdown" data-mve-dropdown="dropdownmenu" data-mve-dropdown-toggle="calendar-header__choose-format">
                        <span class="calendar-header__choose-format">
                            <a class="icon-links icon-links--button icon-links--button-adjust fas fa-rss">
                                <span class="icon-links__text">Export</span>
                            </a>
                        </span>
                        <div class="dropdownmenu dropdownmenu--export">
                            <img class="dropdownmenu__arrow" src="assets/dropdown-arrow.svg" width="18" height="7">
                            <p>Subscribe to Maineve. Copy your private url below and sync it with your preferred calendar app.</p>
                            <ul class="dropdownmenu__list">
                                <li class="dropdownmenu__list-item dropdown__list-item--dont-close">
                                    <input readonly mve-select-text class="input__text input__text--readonly" type="text" name="name" value="{{ dashboardCtrl.apiUrl }}/reservations/feed/calendar.ics?token={{ dashboardCtrl.getToken().token }}">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="calendar-header__center">
                    <h1 class="calendar-header__date">
                        <span class="calendar-header__month">{{ dashboardCtrl.dashboardCal.calendar.getDate().format('MMMM') }}</span>
                        <span class="calendar-header__year">{{ dashboardCtrl.dashboardCal.calendar.getDate().format('YYYY') }}</span>
                    </h1>
                </div>

                <div class="calendar-header__right">
                    <a class="icon-links icon-links--button fas fa-chevron-left" data-ng-click="dashboardCtrl.dashboardCal.calendar.prev(); dashboardCtrl.getCurrentReservations()"><span class="icon-links__text">Previous</span></a>
                    <a class="icon-links icon-links--button fas fa-home" data-ng-click="dashboardCtrl.dashboardCal.calendar.today(); dashboardCtrl.getCurrentReservations()"><span class="icon-links__text">Today</span></a>
                    <a class="icon-links icon-links--button fas fa-chevron-right" data-ng-click="dashboardCtrl.dashboardCal.calendar.next(); dashboardCtrl.getCurrentReservations()"><span class="icon-links__text">Next</span></a>
                </div>

            </div>

            <div class="dashboard-calendar">
                <div
                    data-mve-dashboard-calendar="dashboardCtrl.sources"
                    data-reservations="dashboardCtrl.reservations"
                    data-account="dashboardCtrl.account"
                    data-calendar="dashboardCtrl.dashboardCal"
                    data-toggle-reservation-form="dashboardCtrl.toggleReservationForm(calEvent)">
                </div>

                <mve-reservation
                    ng-show="dashboardCtrl.showReservationForm"
                    calendar-source="dashboardCtrl.calEvent"
                    show-reservation-form="dashboardCtrl.showReservationForm"
                    toggle-reservation-form="dashboardCtrl.toggleReservationForm()"
                    get-current-reservations="dashboardCtrl.getCurrentReservations()">
                </mve-reservation>
            </div>

        </div>
    </div>
`;
