import React from "react";
import { AngularWidget } from "../../Generic/components/AngularWidget";
import { Layout } from "../../Generic/components/Layout";
import { CalendarTemplate } from "../templates/CalendarTemplate";

class Calendar extends React.Component {

    public render(): JSX.Element {

        return (
            <Layout>
                <AngularWidget
                    template={ CalendarTemplate }
                />
            </Layout>
        );
    }
}

export default Calendar;
