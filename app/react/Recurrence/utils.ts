import { IRecurrence, ERuleType } from "./types";

export const defaultRecurrence = (start: string, end: string): IRecurrence => {
    return {
        "start_time": {
            time: start,
            zone: "UTC",
        },
        "end_time": {
            time: end,
            zone: "UTC",
        },
        rrules: [
            {
                "rule_type": ERuleType.weekly,
                interval: 1,
                until: null,
                validations: {
                    day: [0],
                },
            },
        ],
        rtimes: [],
        extimes: [],
    };
};
