import React from "react";
import { IRecurrence, ERuleType } from "../types";
import { Form } from "../components/Form";
import { defaultRecurrence } from "../utils";
import { IReservation } from "app/store/Reservation/types";
import "./../style.scss";

interface IRecurrenceProps {
    startdate: string;
    enddate: string;
    originalId?: number;
    recurrence?: IRecurrence;
    reservation: IReservation;
    close: () => void;
    updateAngularScope: (recurrence: string) => void;
}

interface IRecurrenceState {
    recurrence: IRecurrence;
    recurrenceReset: IRecurrence;
    interval: number | string;
    until: string;
    loading: boolean;
}

export class Recurrence extends React.Component<IRecurrenceProps, IRecurrenceState> {

    constructor(props: IRecurrenceProps) {
        super(props);

        this.state = {
            recurrence: props.recurrence,
            recurrenceReset: props.recurrence,
            interval: props.recurrence && props.recurrence.rrules[0].interval,
            until: props.recurrence && props.recurrence.rrules[0].until,
            loading: false,
        };

        this.handleUpdateAngular = this.handleUpdateAngular.bind(this);
        this.handleRuleTypeChange = this.handleRuleTypeChange.bind(this);
        this.handleIntervalChange = this.handleIntervalChange.bind(this);
        this.handleWeekdayChange = this.handleWeekdayChange.bind(this);
        this.handleAddRecurrence = this.handleAddRecurrence.bind(this);
        this.handleRemoveRecurrence = this.handleRemoveRecurrence.bind(this);
        this.handleResetRecurrence = this.handleResetRecurrence.bind(this);
        this.handleUntilChange = this.handleUntilChange.bind(this);
    }

    public componentDidUpdate(nextProps: IRecurrenceProps, nextState: IRecurrenceState): void {
        if (JSON.stringify(this.state.recurrence) !== JSON.stringify(nextState.recurrence)) {
            this.handleUpdateAngular();
        }
    }

    public render(): JSX.Element {

        const { originalId, close } = this.props;
        const { recurrence, interval, until, loading } = this.state;

        return (
            <Form
                ruleTypeChange={ this.handleRuleTypeChange }
                intervalChange={ this.handleIntervalChange }
                weekdayChange={ this.handleWeekdayChange }
                addRecurrence={ this.handleAddRecurrence }
                removeRecurrence={ this.handleRemoveRecurrence }
                resetRecurrence={ this.handleResetRecurrence }
                untilChange={ this.handleUntilChange }
                intervalValue={ interval }
                untilValue={ until }
                recurrence={ recurrence }
                originalId={ originalId }
                loading={ loading }
                close={ close }
            />
        );
    }

    private handleUpdateAngular(): void {
        const { updateAngularScope } = this.props;
        const recurrence = this.state.recurrence ? JSON.stringify(this.state.recurrence) : null;

        updateAngularScope(recurrence);
    }

    private handleAddRecurrence(): void {

        const { startdate, enddate } = this.props;

        this.setState({
            interval: 1,
            recurrence: defaultRecurrence(startdate, enddate),
        });
    }

    private handleRemoveRecurrence(): void {
        this.setState({
            recurrence: null,
        });
    }

    private handleResetRecurrence(): void {

        const { recurrenceReset } = this.state;

        this.setState({
            recurrence: recurrenceReset,
            until: recurrenceReset.rrules[0].until,
            interval: recurrenceReset.rrules[0].interval,
        });
    }

    private handleRuleTypeChange(event: React.FormEvent<HTMLInputElement>): void {

        const { recurrence } = this.state;

        if (recurrence) {
            this.setState({
                recurrence: {
                    ...this.state.recurrence,
                    rrules: [
                        {
                            ...this.state.recurrence.rrules[0],
                            "rule_type": event.currentTarget.value as ERuleType,
                            validations: {
                                ...this.state.recurrence.rrules[0].validations,
                                day: [],
                            },
                        },
                    ],
                },
            });
        }
    }

    private handleWeekdayChange(event: React.FormEvent<HTMLInputElement>): void {

        const { recurrence } = this.state;

        const rules = this.state.recurrence.rrules[0];
        const checked = event.currentTarget.checked;
        const value = parseInt(event.currentTarget.value, null);
        const add = [...rules.validations.day, value];
        const remove = rules.validations.day.filter(day => day !== value);

        if (recurrence) {
            this.setState({
                recurrence: {
                    ...this.state.recurrence,
                    rrules: [
                        {
                            ...rules,
                            validations: {
                                ...rules.validations,
                                day: checked ? add : remove,
                            },
                        },
                    ],
                },
            });
        }
    }

    private handleIntervalChange(event: React.FormEvent<HTMLInputElement>): void {

        const { recurrence } = this.state;

        if (recurrence) {
            this.setState({
                interval: event.currentTarget.value,
                recurrence: {
                    ...this.state.recurrence,
                    rrules: [
                        {
                            ...this.state.recurrence.rrules[0],
                            interval: parseInt(event.currentTarget.value, null) || 1,
                        },
                    ],
                },
            });
        }
    }

    private handleUntilChange(event: React.FormEvent<HTMLInputElement>): void {

        const { recurrence } = this.state;
        const rules = this.state.recurrence.rrules[0];
        const date = event.currentTarget.value;

        if (recurrence) {
            this.setState({
                until: date,
                recurrence: {
                    ...this.state.recurrence,
                    rrules: [
                        {
                            ...rules,
                            until: date || null,
                        },
                    ],
                },
            });
        }
    }
}
