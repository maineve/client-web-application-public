export interface IRecurrence {
    start_time: ITime;
    end_time: ITime;
    rrules: IRules[];
    rtimes: any[];
    extimes: any[];
}

export interface ITime {
    time: string;
    zone: "UTC";
}

export interface IRules {
    interval: number;
    week_start?: number;
    rule_type: ERuleType;
    until: string | null;
    validations: {
        day: number[];
    };
}

export enum ERuleType {
    daily = "IceCube::DailyRule",
    weekly = "IceCube::WeeklyRule",
    monthly = "IceCube::MonthlyRule",
}

export enum EWeekday {
    sun,
    mon,
    tue,
    wed,
    thu,
    fri,
    sat,
}
