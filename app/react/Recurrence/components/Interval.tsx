import React from "react";
import { IRecurrence, ERuleType } from "../types";

interface IIntervalProps {
    value: number;
    recurrence: IRecurrence;
    onChange: (event: React.FormEvent<HTMLInputElement>) => void;
}

export const Interval: React.FC<IIntervalProps> = (props) => {

    const { value, onChange, recurrence } = props;

    const handleIntervalTimeDisplay = (): string => {

        switch (recurrence.rrules[0].rule_type) {
        case ERuleType.daily:
            return "Days";
        case ERuleType.weekly:
            return "Weeks";
        case ERuleType.monthly:
            return "Months";
        }
    };

    return (
        <div className="interval">
            <label className="interval__label" htmlFor="interval">Every</label>
            <input
                className="interval__input"
                id="interval"
                value={ value }
                onChange={ onChange }
                maxLength={ 3 }
            />
            <span className="interval__time">{ handleIntervalTimeDisplay() }</span>
        </div>

    );
};
