import React from "react";
import { includes } from "lodash";
import { IRecurrence } from "../types";
import { words } from "capitalize";

interface ICheckboxProps {
    onChange: (event: React.FormEvent<HTMLInputElement>) => void;
    recurrence: IRecurrence;
    label: string;
    name: string;
    value: number;
}

export const Checkbox: React.FC<ICheckboxProps> = (props) => {

    const { recurrence, onChange, label, name, value } = props;

    const checked = recurrence && includes(recurrence.rrules[0].validations.day, value);

    return (
        <div className="weekday-checkbox">
            <label className="weekday-checkbox__label">
                <input
                    type="checkbox"
                    name={ name }
                    value={ value }
                    onChange={ onChange }
                    checked={ checked }
                    className="weekday-checkbox__input"
                />
                <div className={ checked ? `weekday-checkbox__btn weekday-checkbox__btn--active` : `weekday-checkbox__btn` }>{ words(label) }</div>
            </label>
        </div>
    );
};
