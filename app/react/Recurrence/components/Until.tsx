import React from "react";
import Cleave from "cleave.js/react";

interface IUntilProps {
    value?: string;
    onChange: (event: React.FormEvent<HTMLInputElement>) => void;
}

export const Until: React.FC<IUntilProps> = (props) => {

    const { value, onChange } = props;

    return (
        <div className="until">
            <label htmlFor="until" className="until__label">Ends</label>
            <Cleave
                id="until"
                options={ {date: true, datePattern: ["Y", "m", "d"]} }
                placeholder="YYYY/MM/DD"
                value={ value || "" }
                onChange={ onChange }
                className="until__input"
            />
        </div>
    );
};
