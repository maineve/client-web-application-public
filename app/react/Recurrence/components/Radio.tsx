import React from "react";
import { IRecurrence, ERuleType } from "../types";

interface IRadioProps {
    onChange: (event: React.FormEvent<HTMLInputElement>) => void;
    recurrence: IRecurrence;
    label: string;
    name: string;
    value: ERuleType;
}

export const Radio: React.FC<IRadioProps> = (props) => {

    const { recurrence, onChange, label, name, value } = props;

    const checked = recurrence && recurrence.rrules[0].rule_type === value;

    return (
        <div className="recurrence-radio">
            <label className="recurrence-radio__label">
                <input
                    className="recurrence-radio__input"
                    type="radio"
                    name={ name }
                    value={ value }
                    onChange={ onChange }
                    checked={ checked }
                />
                <div className={ checked ? `recurrence-radio__btn recurrence-radio__btn--active` : `recurrence-radio__btn` }>{ label }</div>
            </label>
        </div>
    );
};
