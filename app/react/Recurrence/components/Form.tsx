import React from "react";
import { IRecurrence, ERuleType, EWeekday } from "../types";
import { Radio } from "./Radio";
import { Checkbox } from "./Checkbox";
import { Interval } from "./Interval";
import { Until } from "./Until";
import { Actions } from "./Actions";
import { faCaretUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

interface IFormProps {
    recurrence: IRecurrence;
    intervalValue: number | string;
    untilValue: string;
    originalId: number;
    loading: boolean;
    close: () => void;
    ruleTypeChange: (event: React.FormEvent<HTMLInputElement>) => void;
    intervalChange: (event: React.FormEvent<HTMLInputElement>) => void;
    weekdayChange: (event: React.FormEvent<HTMLInputElement>) => void;
    untilChange: (event: React.FormEvent<HTMLInputElement>) => void;
    addRecurrence: () => void;
    removeRecurrence: () => void;
    resetRecurrence: () => void;
}

export const Form: React.FC<IFormProps> = (props) => {

    const {
        recurrence,
        loading,
        ruleTypeChange,
        intervalChange,
        intervalValue,
        weekdayChange,
        untilValue,
        untilChange,
        addRecurrence,
        removeRecurrence,
        resetRecurrence,
        originalId,
        close,
    } = props;

    const weekdays = Object.keys(EWeekday).filter(weekday => isNaN(Number(EWeekday[weekday])));

    return (
        <div className={ `recurrence-form ${ recurrence ? `recurrence-form--margin` : `` }` }>

            <span className="recurrence-form__arrow">
                <FontAwesomeIcon icon={ faCaretUp } />
            </span>

            {
                !recurrence

                ?

                    <ul className="dropdownmenu__list">
                        <li className="dropdownmenu__list-item">
                            <a
                                className="dropdownmenu__list-link"
                                role="button"
                                onClick={ addRecurrence }
                                id="add-repeat-link"
                            >
                                Add Repeat
                            </a>
                        </li>
                    </ul>

                :

                    <div className="recurrence-form__form-wrapper">

                        <div className="recurrence-form__ruletype">
                            <Radio
                                name="rule_type"
                                label="Daily"
                                value={ ERuleType.daily }
                                onChange={ ruleTypeChange }
                                recurrence={ recurrence }
                            />
                            <Radio
                                name="rule_type"
                                label="Weekly"
                                value={ ERuleType.weekly }
                                onChange={ ruleTypeChange }
                                recurrence={ recurrence }
                            />
                            <Radio
                                name="rule_type"
                                label="Monthly"
                                value={ ERuleType.monthly }
                                onChange={ ruleTypeChange }
                                recurrence={ recurrence }
                            />
                        </div>

                        {
                            (recurrence && recurrence.rrules[0].rule_type === ERuleType.weekly)

                            &&

                            <div className="recurrence-form__weekday">
                                {
                                    weekdays.map(weekday => (
                                        <Checkbox
                                            key={ EWeekday[weekday] }
                                            name="day"
                                            label={ EWeekday[weekday] }
                                            value={ parseInt(weekday, null) }
                                            onChange={ weekdayChange }
                                            recurrence={ recurrence }
                                        />
                                    ))
                                }
                            </div>
                        }

                        <hr />

                        <Interval
                            value={ intervalValue as number }
                            recurrence={ recurrence }
                            onChange={ intervalChange }
                        />

                        <Until
                            value={ untilValue }
                            onChange={ untilChange }
                        />

                        <hr />

                        <Actions
                            loading={ loading }
                            removeRecurrence={ removeRecurrence }
                            resetRecurrence={ resetRecurrence }
                            originalId={ originalId }
                            close={ close }
                        />
                    </div>
            }
        </div>
    );
};
