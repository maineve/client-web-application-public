import React from "react";

interface IActionProps {
    originalId: number;
    loading: boolean;
    close: () => void;
    removeRecurrence: () => void;
    resetRecurrence: () => void;
}

export const Actions: React.FC<IActionProps> = (props) => {

    const { removeRecurrence, resetRecurrence, originalId, close } = props;

    return (
        <div className="interval__btn-group">
            {
                !originalId

                ?

                    <button
                        type="button"
                        className="interval__btn"
                        onClick={ removeRecurrence }
                        id="remove-repeat-link"
                    >
                        Remove Repeat
                    </button>

                :

                    <button
                        type="button"
                        className="interval__btn"
                        onClick={ resetRecurrence }
                        data-dropdown-close={ true }
                    >
                        Cancel
                    </button>
            }

            <button
                type="button"
                className="interval__btn interval__btn--right"
                data-dropdown-close={ true }
                onClick={ close }
            >
                Done
            </button>
        </div>
    );
};
