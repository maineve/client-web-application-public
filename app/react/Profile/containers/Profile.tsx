import React from "react";
import { connect } from "react-redux";
import { reset } from "redux-form";
import { ProfileTabs } from "../components/ProfileTabs";
import { IUserState } from "../../../store/User/types";
import { updateUserProfileAction } from "../../../store/User/actions";
import { IAppState } from "../../../store/types";
import { IAuthState } from "../../../store/Auth/types";
import { putUpdatePasswordAction } from "../../../store/Auth/actions";

interface IStateProps {
    auth: IAuthState;
    user: IUserState;
}

interface IDispatchProps {
    updateUserProfile: typeof updateUserProfileAction;
    updatePassword: typeof putUpdatePasswordAction;
    resetPasswordForm: typeof reset;
}

interface IProps {
    tabIndex: number;
    onToggleModal: (value?: boolean) => void;
    setTabIndex: (index: number) => void;
}

type TProps = IStateProps & IDispatchProps & IProps;

const Profile: React.FC<TProps> = (props) => {

    const {
        tabIndex,
        setTabIndex,
        user,
        auth,
        onToggleModal,
        updateUserProfile,
        updatePassword,
        resetPasswordForm,
    } = props;

    return (
        <ProfileTabs
            tabIndex={ tabIndex }
            setTabIndex={ setTabIndex }
            user={ user.profile.data }
            updateUserProfile={ updateUserProfile }
            savingProfile={ user.profile.saving }
            savingPassword={ auth.fetching }
            updatePassword={ updatePassword }
            onToggleModal={ onToggleModal }
            resetPasswordForm={ resetPasswordForm }
        />
    );
};

export default connect<IStateProps, IDispatchProps, IProps, IAppState>(
    (state) => ({
        auth: state.auth,
        user: state.user,
    }),
    {
        updateUserProfile: updateUserProfileAction,
        updatePassword: putUpdatePasswordAction,
        resetPasswordForm: reset,
    }
)(Profile);
