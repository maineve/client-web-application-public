import React from "react";
import { Tabs, Tab, TabList, TabPanel } from "react-tabs";
import { reset } from "redux-form";
import { default as ProfileForm } from "./ProfileForm";
import { default as ChangePasswordForm } from "./ChangePasswordForm";
import { IUser } from "../../../store/User/types";
import { updateUserProfileAction } from "../../../store/User/actions";
import { putUpdatePasswordAction } from "../../../store/Auth/actions";
import { IUpdatePassword } from "../../../store/Auth/types";

interface IProps {
    tabIndex: number;
    savingProfile: boolean;
    savingPassword: boolean;
    user: IUser;
    updateUserProfile: typeof updateUserProfileAction;
    updatePassword: typeof putUpdatePasswordAction;
    resetPasswordForm: typeof reset;
    onToggleModal: (value?: boolean) => void;
    setTabIndex: (index: number) => void;
}

interface IUpdatePasswordValues {
    currentPassword: string;
    newPassword: string;
    confirmPassword: string;
}

export const ProfileTabs: React.FC<IProps> = (props) => {

    const {
        tabIndex,
        setTabIndex,
        user,
        onToggleModal,
        updateUserProfile,
        savingProfile,
        updatePassword,
        savingPassword,
        resetPasswordForm,
    } = props;

    const handleOnSelect = (index: number): void => {
        setTabIndex(index);
    };

    const handleProfileSubmit = (values: IUser): void => {
        updateUserProfile(user.id, values);
    };

    const handleChangePasswordSubmit = (values: IUpdatePasswordValues): void => {

        const credentialsDto: IUpdatePassword = {
            current_password: values.currentPassword,
            password: values.newPassword,
        };

        updatePassword(credentialsDto);
    };

    const handleSaveSuccess = (): void => {
        resetPasswordForm("changepassword");
    };

    return (
        <div className="profile-tabs">
            <Tabs className="profile-tabs__tabs" selectedIndex={ tabIndex } onSelect={ handleOnSelect }>
                <TabList className="profile-tabs__tablist">
                    <Tab className="profile-tabs__tab" selectedClassName="profile-tabs__tab--selected">Profile</Tab>
                    <Tab className="profile-tabs__tab" selectedClassName="profile-tabs__tab--selected">Change Password</Tab>
                </TabList>

                <TabPanel className="profile-tabs__tabpanel">
                    <ProfileForm
                        initialValues={ user }
                        onSubmit={ handleProfileSubmit }
                        onToggleModal={ onToggleModal }
                        saving={ savingProfile }
                    />
                </TabPanel>

                <TabPanel className="profile-tabs__tabpanel">
                    <ChangePasswordForm
                        onSubmit={ handleChangePasswordSubmit }
                        onSaveSuccess={ handleSaveSuccess }
                        onToggleModal={ onToggleModal }
                        saving={ savingPassword }
                    />
                </TabPanel>
            </Tabs>
        </div>
    );
};
