import React from "react";

interface IProps {
    onCancelClick: () => void;
}

export const Buttons: React.FC<IProps> = (props) => {

    const { onCancelClick, children } = props;

    const handleCancelClick = (): void => {
        onCancelClick();
    };

    return (
        <div className="profile-btns button">
            { children }
            <a
                className="profile-btns__cancel button__cancel"
                role="button"
                onClick={ handleCancelClick }
            >
                Cancel
            </a>
        </div>
    );
};
