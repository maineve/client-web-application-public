import React, { useState, useEffect } from "react";
import { reduxForm, Form, Field, InjectedFormProps } from "redux-form";
import { passwordStrength, required } from "../../../utils/validations";
import { TextField } from "../../Generic/wrappers/TextField";
import { PasswordField } from "../../Generic/wrappers/PasswordField";
import { usePrevious } from "../../../hooks";
import { Buttons } from "./Buttons";

interface IProps {
    saving: boolean;
    onSaveSuccess: () => void;
    onToggleModal: (value?: boolean) => void;
}

interface IFormData {
    currentPassword: string;
    newPassword: string;
    confirmPassword: string;
}

type TProps = IProps & InjectedFormProps<IFormData, IProps>;

const validate = (values: IFormData): IFormData => {

    const errors: IFormData = {
        currentPassword: null,
        newPassword: null,
        confirmPassword: null,
    };

    if (values.newPassword || values.confirmPassword) {
        if (values.newPassword !== values.confirmPassword) {
            errors.confirmPassword = "Password confirmation must match the password inserted above.";
        }
    }

    return errors;
};


const ChangePasswordForm: React.FC<TProps> = (props) => {

    const { handleSubmit, onToggleModal, saving, onSaveSuccess } = props;

    const [showSaved, setShowSaved] = useState<boolean>(false);
    const prevSaving = usePrevious<boolean>(saving);

    useEffect(() => {
        if (prevSaving && !saving) {
            setShowSaved(true);
            onSaveSuccess();

            setTimeout(() => {
                setShowSaved(false);
            }, 5000);
        }
    }, [saving, prevSaving]);

    const handleCancelClick = (): void => {
        onToggleModal();
    };

    return (
        <div className="profile-form">
            <Form onSubmit={ handleSubmit }>

                <fieldset>
                    <Field
                        name="currentPassword"
                        type="password"
                        id="current-password"
                        size="small"
                        component={ TextField }
                        label="Current Password"
                        disabled={ saving }
                        validate={ [required] }
                    />
                    <Field
                        name="newPassword"
                        type="password"
                        id="new-password"
                        size="small"
                        component={ PasswordField }
                        label="New Password"
                        disabled={ saving }
                        validate={ [required, passwordStrength] }
                    />
                    <Field
                        name="confirmPassword"
                        type="password"
                        id="confirm-password"
                        size="small"
                        component={ TextField }
                        label="Confirm Password"
                        disabled={ saving }
                        validate={ [required] }
                    />
                </fieldset>

                <Buttons onCancelClick={ handleCancelClick }>
                    <button type="submit" className="profile-btns__btn button__button">
                        { saving ? "Saving ..." : "Save" }
                    </button>
                    { showSaved && <span className="profile-btns__saved">Password Changed</span> }
                </Buttons>

            </Form>
        </div>
    );
};

export default reduxForm<{}, IProps>({
    form: "changepassword",
    validate,
})(ChangePasswordForm);
