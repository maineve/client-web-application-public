import React, { useState, useEffect } from "react";
import { reduxForm, Form, Field, InjectedFormProps } from "redux-form";
import { required } from "../../../utils/validations";
import { TextField } from "../../Generic/wrappers/TextField";
import { IUser } from "../../../store/User/types";
import { Buttons } from "./Buttons";
import { usePrevious } from "../../../hooks";

interface IProps {
    saving: boolean;
    onToggleModal: (value?: boolean) => void;
}

type TProps = IProps & InjectedFormProps<IUser, IProps>;

const ProfileForm: React.FC<TProps> = (props) => {

    const { handleSubmit, onToggleModal, saving } = props;

    const [showSaved, setShowSaved] = useState<boolean>(false);
    const prevSaving = usePrevious<boolean>(saving);

    useEffect(() => {
        if (prevSaving && !saving) {
            setShowSaved(true);

            setTimeout(() => {
                setShowSaved(false);
            }, 5000);
        }
    }, [saving, prevSaving]);

    const handleCancelClick = (): void => {
        onToggleModal();
    };

    return (
        <div className="profile-form">
            <Form onSubmit={ handleSubmit }>

                <fieldset>
                    <Field
                        name="firstname"
                        id="profile-firstname"
                        size="small"
                        component={ TextField }
                        label="First Name"
                        disabled={ saving }
                        validate={ [required] }
                    />
                    <Field
                        name="lastname"
                        id="profile-lastname"
                        size="small"
                        component={ TextField }
                        label="Last Name"
                        disabled={ saving }
                        validate={ [required] }
                    />
                </fieldset>

                <Buttons onCancelClick={ handleCancelClick }>
                    <button type="submit" className="profile-btns__btn button__button">
                        <span>{ saving ? "Saving ..." : "Save" }</span>
                    </button>
                    { showSaved && <span className="profile-btns__saved">Profile Saved</span> }
                </Buttons>


            </Form>
        </div>
    );
};

export default reduxForm<IUser, IProps>({
    form: "profile",
})(ProfileForm);
