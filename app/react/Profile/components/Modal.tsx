import React, { Fragment } from "react";
import { Overlay } from "../../Generic/components/Overlay";
import { Portal } from "../../Generic/components/Portal";
import { default as Profile } from "../containers/Profile";

import "../../../assets/images/icons/notepad.svg";
import "../styles.scss";

interface IProps {
    showModal: boolean;
    tabIndex: number;
    setTabIndex: (index: number) => void;
    onToggleModal: (value?: boolean) => void;
}

export const Modal: React.FC<IProps> = (props) => {

    const { showModal, onToggleModal, tabIndex, setTabIndex } = props;

    return (
        <Portal id="maineve-app" element="div">
            {
                showModal

                &&

                <Fragment>
                    <Overlay />
                    <div className="profile-modal">
                        <div className="profile-modal__modal">
                            <img className="profile-modal__icon" src="assets/notepad.svg" width="40" height="40" />
                            <h2 className="profile-modal__header">Profile</h2>
                            <div className="profile-modal__inner">
                                <Profile
                                    tabIndex={ tabIndex }
                                    setTabIndex={ setTabIndex }
                                    onToggleModal={ onToggleModal }
                                />
                            </div>
                        </div>
                    </div>
                </Fragment>
            }
        </Portal>
    );
};
