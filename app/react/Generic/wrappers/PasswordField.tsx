import React from "react";
import { WrappedFieldProps } from "redux-form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamationTriangle, faInfoCircle, faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { passwordStrengthRegex } from "../../../utils/validations";
import { Tooltip } from "../../Anon/components/Tooltip";

interface IProps {
    id: string;
    label: string;
    className?: string;
    size?: "small" | "large";
}

interface IState {
    showTooltip: boolean;
}

type TProps = IProps & WrappedFieldProps;

export class PasswordField extends React.Component<TProps, IState> {

    constructor(props: TProps) {
        super(props);

        this.state = {
            showTooltip: false,
        };

        this.handleToggleTooltip = this.handleToggleTooltip.bind(this);
    }

    public render(): JSX.Element {

        const {
            meta: {
                error,
                touched,
                submitFailed,
            },
            input,
            id,
            label,
            className,
            size,
        } = this.props;

        const { showTooltip } = this.state;

        const handleOnChange = (event: React.FormEvent<HTMLInputElement>): void => {
            this.props.input.onChange(event.currentTarget.value);
        };

        const handleOnFocus = (event: React.FocusEvent<HTMLInputElement>): void => {
            this.props.input.onFocus(event, this.props.input.name);
        };

        const handleOnBlur = (event: React.FormEvent<HTMLInputElement>): void => {
            this.props.input.onBlur(event.currentTarget.value);
        };

        return (
            <div className={ `text-field ${ className ? className : `` }` }>

                <label className="text-field__label" htmlFor={ id }>
                    { label }
                </label>

                <input
                    type="password"
                    className={ `text-field__input ${ size === "small" && `text-field__input--small` }` }
                    id={ id }
                    name={ input.name }
                    value={ input.value }
                    onChange={ handleOnChange }
                    onFocus={ handleOnFocus }
                    onBlur={ handleOnBlur }
                />

                {
                    (submitFailed && touched && error)

                    ?

                        <div
                            className={ `text-field__icon ${ size === "small" && `text-field__icon--small` } text-field__icon--error` }
                            onMouseEnter={ this.handleToggleTooltip }
                            onMouseLeave={ this.handleToggleTooltip }
                        >
                            <FontAwesomeIcon icon={ faExclamationTriangle } />
                        </div>

                    :

                        <div
                            className={ `text-field__icon ${ size === "small" && `text-field__icon--small` } ${ input.value.match(passwordStrengthRegex) ? `text-field__icon--check` : `` }` }
                            onMouseEnter={ this.handleToggleTooltip }
                            onMouseLeave={ this.handleToggleTooltip }
                        >
                            <FontAwesomeIcon icon={ input.value.match(passwordStrengthRegex) ? faCheckCircle : faInfoCircle } />
                        </div>
                }

                {
                    !input.value.match(passwordStrengthRegex)

                    &&

                    <Tooltip show={ showTooltip } type={ (submitFailed && touched && error) ? "error" : null } pointer="icon">
                        <div className="password-help">
                            <strong>Password must contain the following:</strong>
                            <ul>
                                <li>At least 1 number.</li>
                                <li>At least 1 capital letter.</li>
                                <li>At least 8 characters in length.</li>
                            </ul>
                            <span>Icon will change to a green checkmark when password<br />criteria is met.</span>
                        </div>
                    </Tooltip>
                }
            </div>
        );
    }

    private handleToggleTooltip(): void {

        const { showTooltip } = this.state;

        this.setState({
            showTooltip: !showTooltip,
        });
    }
}
