import React from "react";
import { WrappedFieldProps } from "redux-form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";
import { Tooltip } from "../../Anon/components/Tooltip";

interface IProps {
    id: string;
    label: string;
    type?: string;
    className?: string;
    disabled?: boolean;
    size?: "small" | "large";
}

interface IState {
    showTooltip: boolean;
}

type TProps = IProps & WrappedFieldProps;

export class TextField extends React.Component<TProps, IState> {

    constructor(props: TProps) {
        super(props);

        this.state = {
            showTooltip: false,
        };

        this.handleToggleTooltip = this.handleToggleTooltip.bind(this);
    }

    public render(): JSX.Element {
        const {
            meta: {
                error,
                touched,
                submitFailed,
            },
            input,
            id,
            label,
            type,
            className,
            disabled,
            size,
        } = this.props;

        const { showTooltip } = this.state;

        const handleOnChange = (event: React.FormEvent<HTMLInputElement>): void => {
            this.props.input.onChange(event.currentTarget.value);
        };

        const handleOnFocus = (event: React.FocusEvent<HTMLInputElement>): void => {
            this.props.input.onFocus(event, this.props.input.name);
        };

        const handleOnBlur = (event: React.FormEvent<HTMLInputElement>): void => {
            this.props.input.onBlur(event.currentTarget.value);
        };

        return (
            <div className={ `text-field ${ className ? className : `` }` }>

                <label className="text-field__label" htmlFor={ id }>
                    { label }
                </label>

                <input
                    className={ `text-field__input ${ size === "small" && `text-field__input--small` }` }
                    id={ id }
                    name={ input.name }
                    type={ type || `text` }
                    value={ input.value }
                    onChange={ handleOnChange }
                    onFocus={ handleOnFocus }
                    onBlur={ handleOnBlur }
                    disabled={ disabled }
                />

                {
                    (submitFailed && touched && error)

                    &&

                    <div
                        className={ `text-field__icon ${ size === "small" && `text-field__icon--small` } text-field__icon--error` }
                        onMouseEnter={ this.handleToggleTooltip }
                        onMouseLeave={ this.handleToggleTooltip }
                    >
                        <FontAwesomeIcon icon={ faExclamationTriangle } />
                    </div>
                }

                <Tooltip type="error" show={ showTooltip } pointer="icon">
                    { error }
                </Tooltip>
            </div>
        );
    }

    private handleToggleTooltip(): void {

        const { showTooltip } = this.state;

        this.setState({
            showTooltip: !showTooltip,
        });
    }
}
