import React from "react";
import { WrappedFieldProps } from "redux-form";
import Select from "react-select";
import { Styles } from "react-select/src/styles";

export interface ISelectOption {
    label: string;
    value: string;
}

interface IProps {
    options: ISelectOption[];
    label?: string;
    loading?: boolean;
    disabled?: boolean;
    required?: boolean;
}

export type TProps =
        WrappedFieldProps &
        IProps;

const selectStyles: Partial<Styles> = {
    control: (styles, { isFocused, isDisabled }) => ({
        ...styles,
        "width": "100%",
        "borderColor": isFocused ? `#1cbbb4` : `#ced4da`,
        "background": isDisabled && `#e9ecef`,
        "boxShadow": "none",
        ":hover": {
            "borderColor": isFocused ? `#1cbbb4` : `#ced4da`,
        },
        ":focus": {
            "borderColor": `#1cbbb4`,
        },
    }),
    menu: (styles) => ({
        ...styles,
        "zIndex": 999,
    }),
    option: (styles, { isSelected }) => ({
        ...styles,
        "background": isSelected ? "#1cbbb4" : "white",
        ":hover": {
            "background": "#4bcdc7",
            "color": "white",
        },
    }),
};

export class SelectField extends React.Component<TProps> {

    private select: Select<ISelectOption>;

    constructor(props: TProps) {

        super(props);

        this.handleOnBlur = this.handleOnBlur.bind(this);
        this.handleOnChange = this.handleOnChange.bind(this);
        this.handleOnFocus = this.handleOnFocus.bind(this);
        this.handleLabelFocus = this.handleLabelFocus.bind(this);
        this.setSelectRef = this.setSelectRef.bind(this);
    }

    public render(): JSX.Element {

        const {
            options,
            input,
            loading,
            disabled,
            required,
            label,
        } = this.props;

        const value = options.filter(option => input.value === option.value);

        return (
            <div className="select-field form-group">

                {
                    label

                    &&

                    <label
                        htmlFor={ input.name }
                        className="select-field__label"
                        onClick={ this.handleLabelFocus }
                    >
                        <span className="select-field__label-text">{ label }</span>
                        { required && <small className="select-field__required">(Required)</small> }
                    </label>
                }

                <Select
                    { ...input }
                    ref={ this.setSelectRef }
                    value={ value }
                    options={ options }
                    onChange={ this.handleOnChange }
                    onFocus={ this.handleOnFocus }
                    onBlur={ this.handleOnBlur }
                    isLoading={ loading }
                    isDisabled={ disabled }
                    styles={ selectStyles }
                />
            </div>
        );
    }

    private setSelectRef(ref: Select<ISelectOption>): void {
        this.select = ref;
    }

    private handleLabelFocus(): void {
        this.select.focus();
    }

    private handleOnChange(data: ISelectOption): void {
        const { input } = this.props;

        input.onChange(data.value);
    }

    private handleOnFocus(event: React.FocusEvent<HTMLElement>): void {
        const { input } = this.props;

        input.onFocus(event);
    }

    private handleOnBlur(): void {
        const { input } = this.props;

        input.onBlur(input.value);
    }
}
