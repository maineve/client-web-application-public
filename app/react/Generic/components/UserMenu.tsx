import React, { Fragment, useEffect, useRef, useState, useCallback } from "react";
import { default as Select } from "react-select";
import { Modal as ProfileModal } from "../../Profile/components/Modal";
import { Link } from "react-router-dom";
import { routes } from "../../../routes";
import { IUser } from "../../../store/User/types";
import "../../../assets/images/layout/dropdown-arrow.svg";
import { ERoles } from "../../../store/Role/types";

interface IProps {
    show: boolean;
    locked: boolean;
    profile: IUser;
    onAccountChange: (id: number) => void;
    onOutsideClick: () => void;
}

export const UserMenu: React.FC<IProps> = (props) => {

    const { show, locked, onOutsideClick, onAccountChange } = props;
    const { users_accounts: userAccounts, current_account_id: currentAccountId } = props.profile;

    const selectUserAccounts = userAccounts?.map(ua => ({ value: ua.account?.id, label: ua.account?.name }));
    const selectedAccount = selectUserAccounts?.find(ua => ua.value === currentAccountId);

    const isOwner = userAccounts.find(ua => ua.account.id === currentAccountId).role.name === ERoles.owner;

    const [showProfileModal, setShowProfileModal] = useState<boolean>(false);
    const [profileTabIndex, setProfileTabIndex] = useState<number>(0);

    const menuEl = useRef<HTMLDivElement>(null);

    const handleOutsideClick = useCallback(
        (event: MouseEvent): void => {

            const target: HTMLElement = event.target as HTMLElement;

            if (show && menuEl && !menuEl.current?.contains(target) && !target.closest("#avatar")) {
                onOutsideClick();
            }
        },
        [onOutsideClick, show]
    );

    const handleSetTabIndex = (index: number): void => {
        setProfileTabIndex(index);
    };

    const handleProfileClick = (event: React.MouseEvent<HTMLAnchorElement>): void => {
        event.preventDefault();
        setShowProfileModal(true);
        setProfileTabIndex(0);
        onOutsideClick();
    };

    const handleChangePasswordClick = (event: React.MouseEvent<HTMLAnchorElement>): void => {
        event.preventDefault();
        setShowProfileModal(true);
        setProfileTabIndex(1);
        onOutsideClick();
    };

    const handleToggleModal = (value = !showProfileModal): void => {
        setShowProfileModal(value);
    };

    const handleAccountChange = (selection: { label: string; value: number }): void => {
        onAccountChange(selection.value);
    };

    const handleLogout = (event: React.MouseEvent<HTMLAnchorElement>): void => {
        event.preventDefault();
        localStorage.clear();
        window.location.replace("/");
    };

    useEffect(() => {
        window.addEventListener("click", handleOutsideClick);

        return (): void => window.removeEventListener("click", handleOutsideClick);
    }, [handleOutsideClick]);

    return (
        <Fragment>
            {
                show

                &&

                <div ref={ menuEl } className="dropdownmenu dropdownmenu--user dropdownmenu--show">
                    <img className="dropdownmenu__arrow dropdownmenu__arrow--user" src="assets/dropdown-arrow.svg" width="18" height="7" />
                    <ul className="dropdownmenu__list">
                        <li className="dropdownmenu__list-item">
                            <a href="#" onClick={ handleProfileClick } className="dropdownmenu__list-link">Profile</a>
                        </li>
                        <li className={ `dropdownmenu__list-item ${ (!isOwner || locked) && `dropdownmenu__list-item--border-bottom` }` }>
                            <a href="#" onClick={ handleChangePasswordClick } className="dropdownmenu__list-link">Change Password</a>
                        </li>
                        {
                            (isOwner && !locked)

                            &&

                            <li className="dropdownmenu__list-item dropdownmenu__list-item--border-bottom">
                                <Link to={ routes.account } href="#" className="dropdownmenu__list-link">Manage Account</Link>
                            </li>
                        }
                        {
                            selectUserAccounts.length > 1

                            &&

                            <li className="dropdownmenu__list-item dropdownmenu__list-item--border-bottom">
                                <Select
                                    className={ `dropdownmenu__select` }
                                    value={ selectedAccount }
                                    options={ selectUserAccounts }
                                    onChange={ handleAccountChange }
                                />
                            </li>
                        }
                        <li className="dropdownmenu__list-item">
                            <a href="#" onClick={ handleLogout } className="dropdownmenu__list-link">Logout</a>
                        </li>
                    </ul>
                </div>
            }

            <ProfileModal
                tabIndex={ profileTabIndex }
                setTabIndex={ handleSetTabIndex }
                showModal={ showProfileModal }
                onToggleModal={ handleToggleModal }
            />

        </Fragment>
    );
};
