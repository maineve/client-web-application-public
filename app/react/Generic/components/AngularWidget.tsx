import React from "react";
import angular from "angular";
import { IBuilding } from "app/store/Building/types";

import "angular-payments";
import "angular-capitalize-filter";
import "restangular";
import "ng-dialog/js/ngDialog";
import "select2";
import "moment-timezone";

import { default as AppConfig } from "../../../app.config";
import { default as Components } from "../../../angular/components/components";
import { default as Services } from "../../../angular/services/services";
import { default as Directives } from "../../../angular/directives/directives";

import "fullcalendar/dist/fullcalendar.css";
import "ng-dialog/css/ngDialog.css";
import "select2/dist/css/select2.css";

import "../../../assets/styles/generic/external.scss";
import "../../../app.scss";

interface IProps {
    template: string;
    buildings?: IBuilding[];
}

export class AngularWidget extends React.Component<IProps> {

    private component: HTMLElement;

    constructor(props: IProps) {

        super(props);

        this.setComponentRef = this.setComponentRef.bind(this);
    }

    public componentDidMount(): void {

        /**
         *  Setup Environment
         */
        const env = {};

        if (window) {
            Object.assign(env, window["__env"]);
        }

        angular
            .module("maineve", [
                "angularPayments",
                "ngDialog",
                "restangular",
                "puigcerber.capitalize",
                Components.name,
                Directives.name,
                Services.name,
            ])
            .constant('__env', env)
            .config(AppConfig);

        angular.bootstrap(this.component, ["maineve"]);
    }

    public render(): JSX.Element {

        const { template } = this.props;

        return (
            <div
                className="app-angular"
                ref={ this.setComponentRef }
                dangerouslySetInnerHTML={ { __html: template } }
            />
        );
    }

    private setComponentRef(component: HTMLElement): void {

        this.component = component;
    }
}
