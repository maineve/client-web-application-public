import React, { MouseEvent } from "react";

interface IProps {
    avatarUrl: string;
    menuOpen: boolean;
    onClick: () => void;
}

export const UserAvatar: React.FC<IProps> = (props) => {

    const { onClick, avatarUrl, menuOpen } = props;

    const handleClick = (event: MouseEvent<HTMLAnchorElement>): void => {

        event.preventDefault();
        onClick();
    };

    return (
        <a id="avatar" className="avatar" onClick={ handleClick }>
            <i className={ `avatar__arrow ${ menuOpen && `avatar__arrow--active` } fas fa-bars` }></i>
            <div className="avatar__wrapper">
                <img src={ avatarUrl } />
            </div>
        </a>
    );
};
