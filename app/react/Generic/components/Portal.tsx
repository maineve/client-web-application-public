import React from "react";
import { createPortal } from "react-dom";

/**
 * This component is based off documentation found
 * here https://reactjs.org/docs/portals.html for
 * React Portals.
 */

interface IProps {
    id: string;
    element: string;
    className?: string;
}

export class Portal extends React.Component<IProps> {

    private el: HTMLElement;

    constructor(props: IProps) {
        super(props);

        this.el = document.createElement(props.element);
        this.el.className = props.className || "";
    }

    public componentDidMount(): void {
        const { id } = this.props;

        document.getElementById(id).appendChild(this.el);
    }

    public componentWillUnmount(): void {
        const { id } = this.props;

        document.getElementById(id).removeChild(this.el);
    }

    public render(): React.ReactPortal {
        return (
            createPortal(
                this.props.children,
                this.el,
            )
        );
    }
}
