import React from "react";
import "../style.scss";

export const LayoutAnon: React.FC = (props) => (
    <div className="layout-anon">
        <div className="layout-anon__form">
            <h1 className="logo">Maineve</h1>
            { props.children }
        </div>
        <div className="layout-anon__footer">
            <a className="layout-anon__footer-link" href="mailto:support@maineve.com">Support</a>
            <a className="layout-anon__footer-link" href="https://maineve.com/docs/terms.html">Terms of Use</a>
            <a className="layout-anon__footer-link" href="https://maineve.com/docs/privacy.html">Privacy Policy</a>
        </div>
    </div>
);
