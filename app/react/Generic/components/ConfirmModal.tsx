import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";

interface IProps {
    confirmText?: string;
    cancelText?: string;
    showToggle?: boolean;
    toggleText?: string;
    confirm: (toggle: boolean) => void;
    cancel: () => void;
}

export const ConfirmModal: React.FC<IProps> = (props) => {

    const {
        confirmText,
        cancelText,
        showToggle,
        toggleText,
        confirm,
        cancel,
    } = props;

    const [toggleAction, setToggleAction] = React.useState(false);

    const handleCheckboxClick = (): void => {
        setToggleAction(!toggleAction);
    };

    const handleConfirmClick = (): void => {
        confirm(toggleAction);
    };

    return (
        <div className="confirm-modal">
            <div className="confirm-modal__inner">
                <h2>Confirm</h2>
                <p>{ props.children }</p>

                <div className="confirm-modal__btns">

                    <button
                        className="confirm-modal__btn confirm-modal__btn--red"
                        type="button"
                        onClick={ handleConfirmClick }
                    >
                        { confirmText || `Confirm` }
                    </button>

                    {
                        showToggle

                        &&

                        <label htmlFor="toggle-action" className="confirm-modal__label">
                            <input
                                className="confirm-modal__input"
                                type="checkbox"
                                id="toggle-action"
                                onChange={ handleCheckboxClick }
                            />
                            <span className="confirm-modal__check"><FontAwesomeIcon icon={ faCheck } /></span>
                            <span>{ toggleText }</span>
                        </label>
                    }

                    <a
                        className="confirm-modal__cancel"
                        role="button"
                        onClick={ cancel }
                    >
                        { cancelText || `Cancel` }
                    </a>

                </div>
            </div>
        </div>
    );
};

ConfirmModal.defaultProps = {
    showToggle: false,
};
