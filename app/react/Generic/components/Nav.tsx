import React from "react";
import { Link } from "react-router-dom";
import { routes } from "../../../routes";

export const Nav: React.FC = () => {

    const route = window.location.pathname;

    const getLinkClass = (navRoute: string): string => {
        return `app-nav__link ${ route === navRoute && `app-nav__link--active` }`;
    };

    return (
        <div className="app-nav">
            <ul className="app-nav__list">
                <li className="app-nav__list-item">
                    <Link className={ getLinkClass(routes.dashboard) } to={ routes.dashboard }>Calendar</Link>
                </li>
                <li className="app-nav__list-item">
                    <Link className={ getLinkClass(routes.spaces) } to={ routes.spaces }>Spaces</Link>
                </li>
                <li className="app-nav__list-item">
                    <Link className={ getLinkClass(routes.people) } to={ routes.people }>People</Link>
                </li>
            </ul>
        </div>
    );
};
