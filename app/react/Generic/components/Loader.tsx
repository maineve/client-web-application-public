import React from "react";

export const Loader: React.FC = () => (
    <div className="loader ball-pulse">
        <div></div>
        <div></div>
        <div></div>
    </div>
);
