import React, { Fragment } from "react";
import { withRouter, RouteComponentProps } from "react-router";
import { routes } from "../../../routes";

const NotFoundRedirect: React.FC<RouteComponentProps> = (props) => {

    const { history } = props;

    history.push(routes.dashboard);

    return <Fragment />;
};

export default withRouter(NotFoundRedirect);
