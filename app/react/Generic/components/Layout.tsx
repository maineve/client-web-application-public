import React from "react";
import { default as NavContainer } from "../containers/NavContainer";
import { default as UserMenuContainer } from "../containers/UserMenuContainer";
import { default as Search } from "../../Search/containers/Search";

export const Layout: React.FC = (props) => {

    return (
        <div className="app">

            <header className="app-header">

                <UserMenuContainer />
                <NavContainer />
                <Search />

            </header>

            <main className="app-main">
                { props.children }
            </main>

        </div>
    );
};
