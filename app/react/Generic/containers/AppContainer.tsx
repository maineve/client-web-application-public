import React, { Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter, RouteComponentProps } from "react-router";
import { jwtDecode } from "../../../utils";
import { publicRoutes, adminRoutes, ownerRoutes, routes } from "../../../routes";
import { IAppState } from "../../../store/types";
import { IAuthState } from "../../../store/Auth/types";
import { postValidateAction } from "../../../store/Auth/actions";
import { ISiteState } from "../../../store/Site/types";
import { setApplicationLockedAction, setApplicationActiveAction } from "../../../store/Site/actions";
import { IUserState } from "../../../store/User/types";
import { getUserProfileAction } from "../../../store/User/actions";
import { Loader } from "../components/Loader";
import { ERoles } from "../../../store/Role/types";

interface IStateProps {
    authState: IAuthState;
    siteState: ISiteState;
    userState: IUserState;
}

interface IDispatchProps {
    postValidate: typeof postValidateAction;
    getUserProfile: typeof getUserProfileAction;
    setApplicationLocked: typeof setApplicationLockedAction;
    setApplicationActive: typeof setApplicationActiveAction;
}

type TProps = IStateProps & IDispatchProps & RouteComponentProps<{}>;

class AppContainer extends React.Component<TProps> {

    public componentDidMount(): void {

        const { postValidate } = this.props;

        postValidate();

        if (window["__env"].environment === "development") {
            document.title = "(Dev) Maineve";
        }
    }

    public componentDidUpdate(prevProps: TProps): void {

        const { authenticated: prevAuthenticated } = prevProps.authState;
        const { authenticated } = this.props.authState;
        const { profile: prevProfile, changingAccount: prevChangingAccount } = prevProps.userState;
        const { profile, changingAccount } = this.props.userState;
        const { history, getUserProfile, setApplicationLocked, setApplicationActive } = this.props;

        const isPublicRoute = publicRoutes.some(route => route === window.location.pathname);
        const isAdminRoute = adminRoutes.some(route => route === window.location.pathname);
        const isOwnerRoute = ownerRoutes.some(route => route === window.location.pathname);

        // Handle anonymous user
        if (prevAuthenticated === null && authenticated === false) {

            if (!isPublicRoute) {
                history.replace(routes.login);
            }
        }

        // Handle authenticated user
        if (prevAuthenticated === null && authenticated) {

            const jwt = jwtDecode(localStorage.getItem("token"));

            getUserProfile(jwt["email"]);

            if (isPublicRoute) {
                history.replace(routes.dashboard);
            }
        }

        // Check if authenticated user's account is active or locked
        if (authenticated && !prevProfile.data && profile.data) {

            const currentAccount = profile.data.users_accounts.find(ua => ua.account_id === profile.data.current_account_id);
            const currentDate = new Date().getTime();
            const activeUntilDate = new Date(currentAccount.account.active_until).getTime();

            const isOwner = currentAccount.role.name === ERoles.owner;
            const isAdmin = currentAccount.role.name === ERoles.admin || isOwner;

            if (isAdminRoute && !isAdmin) {
                history.push(routes.dashboard);
            }

            if (isOwnerRoute && !isOwner) {
                history.push(routes.dashboard);
            }

            if (currentDate > activeUntilDate) {
                setApplicationLocked();

                if (history.location.pathname !== routes.locked) {
                    history.replace(routes.locked);
                }
            } else {
                setApplicationActive();

                if (history.location.pathname === routes.locked) {
                    history.replace(routes.dashboard);
                }
            }
        }

        // Reload window when account has been changed
        if (prevChangingAccount && !changingAccount) {
            window.location.reload();
        }
    }

    public render(): JSX.Element {

        const { initialized } = this.props.siteState;

        return (
            <Fragment>
                { initialized ? this.props.children : <Loader /> }
            </Fragment>
        );
    }
}

export default compose(
    withRouter,
    connect<IStateProps, IDispatchProps, {}, IAppState>(
        (state) => ({
            authState: state.auth,
            siteState: state.site,
            userState: state.user,
        }),
        {
            postValidate: postValidateAction,
            getUserProfile: getUserProfileAction,
            setApplicationLocked: setApplicationLockedAction,
            setApplicationActive: setApplicationActiveAction,
        },
    ),
)(AppContainer);
