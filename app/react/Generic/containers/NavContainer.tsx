import React, { Fragment } from "react";
import { connect } from "react-redux";
import { IAppState } from "../../../store/types";
import { ISiteState } from "../../../store/Site/types";
import { Nav } from "../components/Nav";
import { IUserState } from "../../../store/User/types";
import { ERoles } from "../../../store/Role/types";

interface IStateProps {
    siteState: ISiteState;
    userState: IUserState;
}

type TProps = IStateProps;

class NavContainer extends React.Component<TProps> {

    public render(): JSX.Element {

        const { locked } = this.props.siteState;
        const { profile } = this.props.userState;

        const currentAccount = profile.data.users_accounts.find(ua => ua.account_id === profile.data.current_account_id);
        const isAdmin = currentAccount.role.name !== ERoles.user;

        return (
            <Fragment>
                { (!locked && isAdmin) && <Nav /> }
            </Fragment>
        );
    }
}

export default connect<IStateProps, {}, {}, IAppState>(
    (state) => ({
        siteState: state.site,
        userState: state.user,
    })
)(NavContainer);
