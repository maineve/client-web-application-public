import React, { useState, Fragment } from "react";
import { connect } from "react-redux";
import { UserAvatar } from "../components/UserAvatar";
import { UserMenu } from "../components/UserMenu";
import { IAppState } from "../../../store/types";
import { IUserState, IUser } from "../../../store/User/types";
import { updateUserProfileAction, changingAccountAction } from "../../../store/User/actions";
import { ISiteState } from "../../../store/Site/types";

interface IStateProps {
    userState: IUserState;
    siteState: ISiteState;
}

interface IDispatchProps {
    updateUserProfile: typeof updateUserProfileAction;
    changingAccount: typeof changingAccountAction;
}

type TProps = IStateProps & IDispatchProps;

const UserMenuContainer: React.FC<TProps> = (props) => {

    const { userState, siteState, updateUserProfile, changingAccount } = props;

    const [ menuOpen, setMenuOpen ] = useState<boolean>(false);

    const handleMenuOpenToggle = (): void => {
        setMenuOpen(!menuOpen);
    };

    const handleAccountChange = (accountId: number): void => {

        const modifiedProfile: IUser = {
            ...userState.profile.data,
            current_account_id: accountId,
        };

        updateUserProfile(userState.profile.data.id, modifiedProfile);
        changingAccount();
    };

    return (
        <Fragment>

            <UserAvatar
                avatarUrl={ userState.profile.data.image }
                menuOpen={ menuOpen }
                onClick={ handleMenuOpenToggle }
            />

            <UserMenu
                show={ menuOpen }
                locked={ siteState.locked }
                onOutsideClick={ handleMenuOpenToggle }
                onAccountChange={ handleAccountChange }
                profile={ userState.profile.data }
            />

        </Fragment>
    );
};

export default connect<IStateProps, IDispatchProps, {}, IAppState>(
    (state) => ({
        userState: state.user,
        siteState: state.site,
    }),
    {
        updateUserProfile: updateUserProfileAction,
        changingAccount: changingAccountAction,
    }
)(UserMenuContainer);
