export const PeopleTemplate = `
    <section data-ng-controller="userController as userCtrl" data-ng-init="userCtrl.initiate()">

        <div class="loader-wrapper loader-wrapper--opac" data-ng-show="!userCtrl.getInitiated()">
            <div class="loader ball-pulse">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>

        <div data-ng-if="userCtrl.getInitiated()">

            <div class="header">
                <div class="header__left">
                    <a class="icon-links icon-links--button" data-ng-click="userCtrl.openInviteForm()">
                        <span class="icon-links__text icon-links__text--show"><i class="fas fa-plus-circle"></i> Invite someone</span>
                    </a>
                </div>
                <div class="header__center">
                    <h1 class="section-title">Manage People</h1>
                </div>
            </div>

            <ul class="list">
                <li class="list__item">
                    <h2 class="list__item-title">Active</h2>
                    <ul class="sub-list">
                        <li class="sub-list__item" data-ng-repeat="user in userCtrl.users | orderBy: ['firstname', 'lastname', 'role_id']" data-ng-show="user.user">
                            <span class="sub-list__item-title">{{ user.user.firstname }} {{ user.user.lastname }}</span>
                            <span class="sub-list__item-title--detail">({{ user.role.name | capitalize }})</span>
                            <div class="sub-list__item-options" data-ng-show="userCtrl.userRole.is_owner(userCtrl.currentUser)">
                                <span data-mve-dropdown="dropdownmenu" data-mve-dropdown-toggle="sub-list__item-option" data-mve-dropdown-in-list="true" data-ng-if="user.user.id !== userCtrl.currentUser.id">
                                    <span class="sub-list__item-option">
                                        <a class="fas fa-user-lock" title="Role">
                                            <span class="icon-links__text">Role</span>
                                        </a>
                                    </span>

                                    <div class="dropdownmenu dropdownmenu--list">
                                        <img class="dropdownmenu__arrow dropdownmenu__arrow--right" src="assets/dropdown-arrow.svg" width="18" height="7">
                                        <ul class="dropdownmenu__list">
                                            <li class="dropdownmenu__list-item" data-ng-repeat="role in userCtrl.roles">
                                                <a
                                                    class="dropdownmenu__list-link"
                                                    data-ng-class="{ 'dropdownmenu__list-link--active' : user.role.id === role.id }"
                                                    data-ng-click="userCtrl.updateUserRole(user, role.id)"
                                                >
                                                    {{ role.name | capitalize }}
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </span>

                                <span class="sub-list__item-option" data-ng-if="user.user.id !== userCtrl.currentUser.id">
                                    <a
                                        class="fas fa-trash-alt"
                                        title="Delete User"
                                        data-ng-click="userCtrl.deleteUser(user.id); $event.stopPropagation()"
                                    >
                                        <span class="icon-links__text">Delete</span>
                                    </a>
                                </span>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="list__item">
                    <h2 class="list__item-title">Pending Invitations</h2>
                    <ul class="sub-list">
                        <li class="sub-list__item" data-ng-repeat="user in userCtrl.users | orderBy: 'email'" data-ng-show="!user.user">
                            <span class="sub-list__item-title">{{ user.email }}</span>
                            <div class="sub-list__item-options" data-ng-controller="invitationController as invitationCtrl">
                                <span class="sub-list__item-option sub-list__item-option--message" data-ng-show="invitationCtrl.messageInline">Invitation sent</span>
                                <span class="sub-list__item-option"><a class="fas fa-share-square" title="Resend Invitation" data-ng-click="invitationCtrl.resendInvite(user.email); $event.stopPropagation()"><span class="icon-links__text">Resend Invitation</span></a></span>
                                <span class="sub-list__item-option"><a class="fas fa-trash-alt" title="Delete Invitation" data-ng-click="userCtrl.deleteUser(user.id); $event.stopPropagation()"><span class="icon-links__text">Delete Invitation</span></a></span>
                            </div>
                        </li>
                        <li class="sub-list__item sub-list__item--note" data-ng-hide="userCtrl.checkPendingInvitations()">
                            No pending invitations, <a data-ng-click="userCtrl.openInviteForm()">invite someone</a>
                        </li>
                    </ul>
                </li>
            </ul>

        </div>
    </section>
`;
