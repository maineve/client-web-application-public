import React from "react";
import { AngularWidget } from "../../Generic/components/AngularWidget";
import { Layout } from "../../Generic/components/Layout";
import { PeopleTemplate } from "../templates/PeopleTemplate";

class Spaces extends React.Component {

    public render(): JSX.Element {

        return (
            <Layout>
                <AngularWidget
                    template={ PeopleTemplate }
                />
            </Layout>
        );
    }
}

export default Spaces;
