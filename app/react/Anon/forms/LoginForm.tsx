import React from "react";
import { Field, InjectedFormProps, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { ILogin } from "../../../store/Auth/types";
import { TextField } from "../../Generic/wrappers/TextField";
import { required } from "../../../utils/validations";
import { Tooltip } from "../components/Tooltip";
import "../style.scss";

interface IProps {
    loading: boolean;
    showError: boolean;
    errorMsg: string;
}

type TProps = IProps & InjectedFormProps<ILogin, IProps>;

const LoginForm: React.FC<TProps> = (props) => {

    const {
        loading,
        handleSubmit,
        showError,
        errorMsg,
    } = props;

    return (
        <div className="anon-form">
            <form className="anon-form__form" onSubmit={ handleSubmit }>
                <fieldset>
                    <Field
                        name="email"
                        type="email"
                        id="login-email"
                        component={ TextField }
                        label="Email"
                        validate={ required }
                    />
                    <Field
                        name="password"
                        id="login-password"
                        component={ TextField }
                        label="Password"
                        type="password"
                        validate={ required }
                    />
                </fieldset>

                <div className="anon-form__buttons">
                    <button className={ `anon-button ${ loading && `anon-button--loading` }` } disabled={ loading } type="submit">
                        { loading ? `Authenticating ...` : `Login` }
                    </button>

                    <Link className="reset-password" to="/forgot">Forgot your password?</Link>

                    <Tooltip show={ showError } type="error" pointer="button">
                        { errorMsg }
                    </Tooltip>
                </div>
            </form>

            <span className="alt-option">Don&rsquo;t have an account? <Link to="/signup">Create one now</Link>.</span>
        </div>
    );
};

export default reduxForm<ILogin, IProps>({
    form: `login`,
})(LoginForm);
