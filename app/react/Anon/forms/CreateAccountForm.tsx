import React from "react";
import { Field, InjectedFormProps, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { ICreateAccount } from "../../../store/Auth/types";
import { TextField } from "../../Generic/wrappers/TextField";
import { PasswordField } from "../../Generic/wrappers/PasswordField";
import { required, passwordStrength } from "../../../utils/validations";
import { Message } from "../components/Message";
import { Tooltip } from "../components/Tooltip";
import "../style.scss";

interface IProps {
    loading: boolean;
    showError: boolean;
    successMsg: string;
    errorMsg: string;
}

type TProps = IProps & InjectedFormProps<ICreateAccount, IProps>;

const CreateAccountForm: React.FC<TProps> = (props) => {

    const {
        loading,
        handleSubmit,
        showError,
        successMsg,
        errorMsg,
        initialValues: {
            email,
        },
    } = props;

    return (
        <div className="anon-form">
            {
                successMsg

                ?

                    <Message
                        type="success"
                        message={ successMsg }
                    />

                :

                    <form className="anon-form__form" onSubmit={ handleSubmit }>
                        <fieldset>
                            <Field
                                name="firstname"
                                id="create-firstname"
                                component={ TextField }
                                label="First Name"
                                validate={ required }
                            />

                            <Field
                                name="lastname"
                                id="create-lastname"
                                component={ TextField }
                                label="Last Name"
                                validate={ required }
                            />

                            <Field
                                name="email"
                                type="email"
                                id="create-email"
                                component={ TextField }
                                label="Email"
                                validate={ required }
                                disabled={ email !== undefined }
                            />

                            <Field
                                name="password"
                                id="create-password"
                                type="password"
                                component={ PasswordField }
                                label="Password"
                                validate={ [required, passwordStrength] }
                            />

                            <Field
                                name="company"
                                id="create-company"
                                component={ TextField }
                                label="Company"
                                className="text-field--pot"
                            />
                        </fieldset>

                        <div className="anon-form__buttons">
                            <button className={ `anon-button ${ loading && `anon-button--loading` }` } disabled={ loading } type="submit">
                                { loading ? `Creating ...` : `Create Account` }
                            </button>

                            <Tooltip show={ showError } type="error" pointer="button">
                                { errorMsg }
                            </Tooltip>
                        </div>
                    </form>
            }

            <span className="alt-option">Already have an account? <Link to="/">Login now</Link>.</span>
        </div>
    );
};

export default reduxForm<ICreateAccount, IProps>({
    form: `createAccount`,
    initialValues: { company: "" },
})(CreateAccountForm);
