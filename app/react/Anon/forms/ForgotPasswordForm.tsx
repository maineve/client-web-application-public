import React from "react";
import { Field, InjectedFormProps, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { IForgotPassword } from "../../../store/Auth/types";
import { TextField } from "../../Generic/wrappers/TextField";
import { required } from "../../../utils/validations";
import { Message } from "../components/Message";
import { Tooltip } from "../components/Tooltip";
import "../style.scss";

interface IProps {
    loading: boolean;
    showError: boolean;
    successMsg: string;
    errorMsg: string;
}

type TProps = IProps & InjectedFormProps<IForgotPassword, IProps>;

const ForgotPasswordForm: React.FC<TProps> = (props) => {

    const {
        loading,
        handleSubmit,
        showError,
        successMsg,
        errorMsg,
    } = props;

    return (
        <div className="anon-form">
            {
                successMsg

                ?

                    <Message
                        type="success"
                        message={ successMsg }
                    />

                :

                    <form className="anon-form__form" onSubmit={ handleSubmit }>
                        <fieldset>
                            <Field
                                name="email"
                                type="email"
                                id="login-email"
                                component={ TextField }
                                label="Enter your email"
                                validate={ required }
                            />
                        </fieldset>

                        <div className="anon-form__buttons">
                            <button className={ `anon-button ${ loading && `anon-button--loading` }` } disabled={ loading } type="submit">
                                { loading ? `Resetting ...` : `Reset Password` }
                            </button>

                            <Tooltip show={ showError } type="error" pointer="button">
                                { errorMsg }
                            </Tooltip>
                        </div>
                    </form>
            }

            <span className="alt-option">Remembered your password? <Link to="/">Login now</Link>.</span>
        </div>
    );
};

export default reduxForm<IForgotPassword, IProps>({
    form: `forgotPassword`,
})(ForgotPasswordForm);
