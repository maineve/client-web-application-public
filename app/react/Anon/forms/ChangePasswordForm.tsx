import React from "react";
import { Field, InjectedFormProps, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { IChangePasswordValues } from "../../../store/Auth/types";
import { PasswordField } from "../../Generic/wrappers/PasswordField";
import { TextField } from "../../Generic/wrappers/TextField";
import { required, passwordStrength } from "../../../utils/validations";
import { Message } from "../components/Message";
import { Tooltip } from "../components/Tooltip";
import "../style.scss";

interface IProps {
    loading: boolean;
    showError: boolean;
    successMsg: string;
    errorMsg: string;
}

type TProps = IProps & InjectedFormProps<IChangePasswordValues, IProps>;

const ChangePasswordForm: React.FC<TProps> = (props) => {

    const {
        loading,
        handleSubmit,
        showError,
        successMsg,
        errorMsg,
    } = props;

    return (
        <div className="anon-form">
            {
                successMsg

                ?

                    <Message
                        type="success"
                        message={ successMsg }
                    />

                :

                    <form className="anon-form__form" onSubmit={ handleSubmit }>
                        <fieldset>
                            <Field
                                name="password"
                                id="change-password"
                                type="password"
                                component={ PasswordField }
                                label="Password"
                                validate={ [required, passwordStrength] }
                            />

                            <Field
                                name="passwordConfirm"
                                id="change-password-confirm"
                                type="password"
                                component={ TextField }
                                label="Confirm Password"
                                validate={ required }
                            />
                        </fieldset>

                        <div className="anon-form__buttons">
                            <button className={ `anon-button ${ loading && `anon-button--loading` }` } disabled={ loading } type="submit">
                                { loading ? `Updating ...` : `Change Password` }
                            </button>

                            <Tooltip show={ showError } type="error" pointer="button">
                                { errorMsg }
                            </Tooltip>
                        </div>
                    </form>
            }

            <span className="alt-option">Password updated? <Link to="/">Login now</Link>.</span>
        </div>
    );
};

export default reduxForm<IChangePasswordValues, IProps>({
    form: `changePassword`,
})(ChangePasswordForm);
