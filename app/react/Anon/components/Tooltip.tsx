import React, { Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";

interface IProps {
    show: boolean;
    type?: "success" | "error";
    pointer: "button" | "icon" | "reservation";
}

export const Tooltip: React.FC<IProps> = (props) => {

    const { show, type, children, pointer } = props;

    return (
        <Fragment>
            {
                show

                &&

                <div className={ `anon-tooltip anon-tooltip--${ type || "standard" } anon-tooltip--${ pointer }` }>
                    <span className="anon-tooltip__msg">{ children }</span>
                    <span className="anon-tooltip__arrow">
                        <FontAwesomeIcon icon={ faCaretDown } />
                    </span>
                </div>
            }
        </Fragment>
    );
};
