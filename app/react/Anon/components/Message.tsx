import React from "react";

interface IProps {
    type: "success" | "error";
    message: string;
}

export const Message: React.FC<IProps> = (props) => {

    const { message, type } = props;

    return (
        <div className={ `anon-tooltip anon-tooltip--${ type }` }>
            <span className="anon-tooltip__msg">{ message }</span>
        </div>
    );
};
