import React from "react";
import { Link } from "react-router-dom";
import { LayoutAnon } from "../../Generic/components/LayoutAnon";
import { IAuthState } from "../../../store/Auth/types";
import { connect } from "react-redux";
import { IAppState } from "../../../store/types";
import { getAccountConfirmationAction } from "../../../store/Auth/actions";
import { Message } from "../components/Message";

interface IStateProps {
    authState: IAuthState;
}

interface IDispatchProps {
    getAccountConfirmation: typeof getAccountConfirmationAction;
}

type TProps =
    IStateProps &
    IDispatchProps;

class AccountConfirmation extends React.Component<TProps> {

    public componentDidMount(): void {

        const { getAccountConfirmation } = this.props;
        const url = new URL(window.location.href);
        const token = url.searchParams.get("token");

        getAccountConfirmation(token);
    }

    public render(): JSX.Element {

        const { success, error, fetching } = this.props.authState;

        return (
            <LayoutAnon>
                <div className="anon-form">
                    {
                        !fetching

                        &&

                        <Message
                            type={ success ? "success" : "error" }
                            message={ success ? success : error }
                        />
                    }
                </div>

                {
                    <span className="alt-option">
                        Ready to login? <Link to="/">Login now</Link>.
                    </span>
                }
            </LayoutAnon>
        );
    }
}

export default connect<IStateProps, IDispatchProps, {}, IAppState>(
    (state) => ({
        authState: state.auth,
    }),
    {
        getAccountConfirmation: getAccountConfirmationAction,
    },
)(AccountConfirmation);
