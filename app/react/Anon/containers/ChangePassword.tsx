import React from "react";
import * as QueryString from "query-string";
import { LayoutAnon } from "../../Generic/components/LayoutAnon";
import { default as ChangePasswordForm } from "../forms/ChangePasswordForm";
import { IAuthState, IChangePasswordValues } from "../../../store/Auth/types";
import { connect } from "react-redux";
import { IAppState } from "../../../store/types";
import { putChangePasswordAction, passwordMatchErrorAction } from "../../../store/Auth/actions";

interface IStateProps {
    authState: IAuthState;
}

interface IDispatchProps {
    putChangePassword: typeof putChangePasswordAction;
    passwordMatchError: typeof passwordMatchErrorAction;
}

interface IState {
    showError: boolean;
}

type TProps =
    IStateProps &
    IDispatchProps;

class ChangePassword extends React.Component<TProps, IState> {

    constructor(props: TProps) {
        super(props);

        this.state = {
            showError: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleOutsideErrorClick = this.handleOutsideErrorClick.bind(this);
        this.handleToggleError = this.handleToggleError.bind(this);
    }

    public componentDidMount(): void {
        document.addEventListener("mousedown", this.handleOutsideErrorClick);
    }

    public componentDidUpdate(prevProps: TProps): void {
        const { fetching, error } = this.props.authState;

        if (prevProps.authState.fetching && !fetching && error) {
            this.handleToggleError();
        }
    }

    public componentWillUnmount(): void {
        document.removeEventListener("mousedown", this.handleOutsideErrorClick);
    }

    public render(): JSX.Element {

        const { authState } = this.props;
        const { showError } = this.state;

        return (
            <LayoutAnon>
                <ChangePasswordForm
                    loading={ authState.fetching }
                    onSubmit={ this.handleSubmit }
                    showError={ showError }
                    successMsg={ authState.success }
                    errorMsg={ authState.error }
                />
            </LayoutAnon>
        );
    }

    private handleSubmit(values: IChangePasswordValues): void {

        const { putChangePassword, passwordMatchError } = this.props;

        const token = QueryString.parse(window.location.search).token?.toString();

        if (values.password !== values.passwordConfirm) {
            passwordMatchError();
            this.handleToggleError();
        } else {
            putChangePassword({ "password": values.password, "reset_password_token": token });
        }
    }

    private handleOutsideErrorClick(): void {

        const { showError } = this.state;

        if (showError) {
            this.setState({
                showError: false,
            });
        }
    }

    private handleToggleError(): void {

        const { showError } = this.state;

        this.setState({
            showError: !showError,
        });
    }
}

export default connect<IStateProps, IDispatchProps, {}, IAppState>(
    (state) => ({
        authState: state.auth,
    }),
    {
        putChangePassword: putChangePasswordAction,
        passwordMatchError: passwordMatchErrorAction,
    },
)(ChangePassword);
