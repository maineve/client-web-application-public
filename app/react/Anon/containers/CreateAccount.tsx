import React from "react";
import * as QueryString from "query-string";
import { LayoutAnon } from "../../Generic/components/LayoutAnon";
import CreateAccountForm from "../forms/CreateAccountForm";
import { IAuthState, ICreateAccount } from "../../../store/Auth/types";
import { connect } from "react-redux";
import { IAppState } from "../../../store/types";
import { postCreateAccountAction } from "../../../store/Auth/actions";

interface IStateProps {
    authState: IAuthState;
}

interface IDispatchProps {
    postCreateAccount: typeof postCreateAccountAction;
}

interface IState {
    showError: boolean;
}

type TProps =
    IStateProps &
    IDispatchProps;

class CreateAccount extends React.Component<TProps, IState> {

    constructor(props: TProps) {
        super(props);

        this.state = {
            showError: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleOutsideErrorClick = this.handleOutsideErrorClick.bind(this);
        this.handleToggleError = this.handleToggleError.bind(this);
    }

    public componentDidMount(): void {
        document.addEventListener("mousedown", this.handleOutsideErrorClick);
    }

    public componentDidUpdate(prevProps: TProps): void {
        const { fetching, error } = this.props.authState;

        if (prevProps.authState.fetching && !fetching && error) {
            this.handleToggleError();
        }
    }

    public componentWillUnmount(): void {
        document.removeEventListener("mousedown", this.handleOutsideErrorClick);
    }

    public render(): JSX.Element {

        const { authState } = this.props;
        const { showError } = this.state;

        const email = QueryString.parse(window.location.search).email?.toString();

        return (
            <LayoutAnon>
                <CreateAccountForm
                    loading={ authState.fetching }
                    onSubmit={ this.handleSubmit }
                    showError={ showError }
                    successMsg={ authState.success }
                    errorMsg={ authState.error }
                    initialValues={ { email } }
                />
            </LayoutAnon>
        );
    }

    private handleSubmit(values: ICreateAccount): void {

        const { postCreateAccount } = this.props;

        const token = QueryString.parse(window.location.search).invite_token?.toString();

        postCreateAccount(values, token);
    }

    private handleOutsideErrorClick(): void {
        const { showError } = this.state;

        if (showError) {
            this.setState({
                showError: false,
            });
        }
    }

    private handleToggleError(): void {
        const { showError } = this.state;

        this.setState({
            showError: !showError,
        });
    }
}

export default connect<IStateProps, IDispatchProps, {}, IAppState>(
    (state) => ({
        authState: state.auth,
    }),
    {
        postCreateAccount: postCreateAccountAction,
    },
)(CreateAccount);
