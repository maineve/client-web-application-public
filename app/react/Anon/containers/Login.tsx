import React from "react";
import { LayoutAnon } from "../../Generic/components/LayoutAnon";
import { default as LoginForm } from "../forms/LoginForm";
import { ILogin, IAuthState } from "../../../store/Auth/types";
import { connect } from "react-redux";
import { IAppState } from "../../../store/types";
import { postLoginAction } from "../../../store/Auth/actions";
import { routes } from "../../../routes";

interface IStateProps {
    authState: IAuthState;
}

interface IDispatchProps {
    postLogin: typeof postLoginAction;
}

interface IState {
    showError: boolean;
}

type TProps =
    IStateProps &
    IDispatchProps;

class Login extends React.Component<TProps, IState> {

    constructor(props: TProps) {
        super(props);

        this.state = {
            showError: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleOutsideErrorClick = this.handleOutsideErrorClick.bind(this);
        this.toggleError = this.toggleError.bind(this);
    }

    public componentDidMount(): void {
        document.addEventListener("mousedown", this.handleOutsideErrorClick);
    }

    public componentDidUpdate(prevProps: TProps): void {
        const { authenticated, fetching, error } = this.props.authState;

        if (authenticated) {
            window.location.replace(routes.dashboard);
        }

        if (prevProps.authState.fetching && !fetching && error) {
            this.toggleError();
        }
    }

    public componentWillUnmount(): void {
        document.removeEventListener("mousedown", this.handleOutsideErrorClick);
    }

    public render(): JSX.Element {

        const { authState } = this.props;
        const { showError } = this.state;

        return (
            <LayoutAnon>
                <LoginForm
                    loading={ authState.fetching }
                    onSubmit={ this.handleSubmit }
                    errorMsg={ authState.error }
                    showError={ showError }
                />
            </LayoutAnon>
        );
    }

    private handleSubmit(values: ILogin): void {
        const { postLogin } = this.props;

        postLogin(values);
    }

    private toggleError(): void {
        const { showError } = this.state;

        this.setState({
            showError: !showError,
        });
    }

    private handleOutsideErrorClick(): void {
        const { showError } = this.state;

        if (showError) {
            this.setState({
                showError: false,
            });
        }
    }
}

export default connect<IStateProps, IDispatchProps, {}, IAppState>(
    (state) => ({
        authState: state.auth,
    }),
    {
        postLogin: postLoginAction,
    },
)(Login);
