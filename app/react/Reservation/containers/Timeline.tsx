import React from "react";
import { connect } from "react-redux";
import { change, getFormValues } from "redux-form";
import { Moment } from "moment";
import { hoursArray } from "../utils";
import { TimelineSection } from "../components/TimelineSection";
import { TimelineBlock } from "../components/TimelineBlock";
import { TimelineCompareBlock } from "../components/TimelineCompareBlock";
import { IReservationState, IReservation } from "../../../store/Reservation/types";
import { IAppState } from "../../../store/types";

interface IStateProps {
    reservationState: IReservationState;
    formValues: IReservation;
}

interface IDispatchProps {
    formChange: typeof change;
}

interface IProps {
    disabled: boolean;
}

type TProps =
        IProps &
        IStateProps &
        IDispatchProps;

class Timeline extends React.Component<TProps> {

    constructor(props: TProps) {
        super(props);

        this.handleOnChange = this.handleOnChange.bind(this);
    }

    public render(): JSX.Element {

        const { timelineReservations } = this.props.reservationState;
        const { formValues, disabled } = this.props;

        return (
            <div id="timeline" className="timeline">
                <div id="timeline-container" className="timeline__container">
                    {
                        hoursArray.map((hour, index) => (
                            <TimelineSection
                                key={ index + hour }
                                formValues={ formValues }
                                label={ hour }
                                index={ index }
                                onChange={ this.handleOnChange }
                                disabled={ disabled }
                            />
                        ))
                    }
                </div>
                <div id="timeline-blk-track" className="timeline__blk-track">
                    <TimelineBlock
                        formValues={ formValues }
                        timelineReservations={ timelineReservations }
                        grid={ 9.771 }
                        onChange={ this.handleOnChange }
                        disabled={ disabled }
                    />

                    {
                        timelineReservations.map(timelineReservation => (
                            <TimelineCompareBlock
                                key={ timelineReservation.recurrence_id || timelineReservation.id }
                                reservation={ timelineReservation }
                                grid={ 9.771 }
                            />
                        ))
                    }
                </div>
            </div>
        );
    }

    private handleOnChange(startdate: Moment, enddate: Moment): void {

        const { formChange } = this.props;

        formChange("reservation", "startdate", startdate);
        formChange("reservation", "enddate", enddate);
    }
}

export default connect<IStateProps, IDispatchProps, {}, IAppState>(
    (state) => ({
        reservationState: state.reservation,
        formValues: getFormValues("reservation")(state) as IReservation,
    }),
    {
        formChange: change,
    },
)(Timeline);
