import React from "react";
import { connect } from "react-redux";
import { getFormValues, change, getFormSubmitErrors, FormErrors, clearSubmitErrors } from "redux-form";
import moment from "moment";
import { ReservationModal } from "../components/ReservationModal";
import { ICalendarSource, IReservation, ISearchSource } from "../../../store/Reservation/types";
import { IAppState } from "../../../store/types";
import { IReservationState } from "../../../store/Reservation/types";
import { getRoomsAction } from "../../../store/Room/actions";
import { getUserProfileAction } from "../../../store/User/actions";
import { IRoomState } from "../../../store/Room/types";
import { IUserState } from "../../../store/User/types";
import { ERoles } from "../../../store/Role/types";

import {
    getReservationAction,
    resetReservationAction,
    initiateNewReservationAction,
    getTimelineReservationsAction,
    saveSourceInStateAction,
    saveReservationAction,
    removeReservationAction,
} from "../../../store/Reservation/actions";

import "../style.scss";

interface IStateProps {
    reservationState: IReservationState;
    reservationLoading: boolean;
    roomState: IRoomState;
    userState: IUserState;
    roomsLoading: boolean;
    formValues: IReservation;
    formErrors: FormErrors<IReservation>;
}

interface IDispatchProps {
    getReservation: typeof getReservationAction;
    getTimelineReservations: typeof getTimelineReservationsAction;
    initiateNewReservation: typeof initiateNewReservationAction;
    removeReservation: typeof removeReservationAction;
    resetReservation: typeof resetReservationAction;
    saveSourceInState: typeof saveSourceInStateAction;
    saveReservation: typeof saveReservationAction;
    getRooms: typeof getRoomsAction;
    getUserProfile: typeof getUserProfileAction;
    formChange: typeof change;
    formClearSubmitErrors: typeof clearSubmitErrors;
}

interface IProps {
    source: ICalendarSource | ISearchSource;
    closeReservationForm: () => void;
}

interface IState {
    forbidden: boolean;
}

type TProps = IStateProps & IDispatchProps & IProps;

class Reservation extends React.Component<TProps, IState> {

    constructor(props: TProps) {
        super(props);

        this.state = {
            forbidden: false,
        };

        this.handleCloseDialog = this.handleCloseDialog.bind(this);
        this.handleRecurrenceChange = this.handleRecurrenceChange.bind(this);
        this.authorizeUser = this.authorizeUser.bind(this);
    }

    public componentDidMount(): void {

        const { source, saveSourceInState, getRooms, userState } = this.props;

        saveSourceInState(source);
        getUserProfileAction(userState.profile.data.email);
        getRooms();
    }

    public componentDidUpdate(prevProps: TProps): void {

        const { reservation: prevReservation, reservationSource: prevReservationSource } = prevProps.reservationState;
        const { reservation, reservationSource, saving, deleting } = this.props.reservationState;
        const { formValues: prevFormValues } = prevProps;
        const { getReservation, initiateNewReservation, getTimelineReservations, closeReservationForm, formValues } = this.props;

        if (!prevReservationSource && reservationSource) {
            if (reservationSource.id) {
                getReservation(reservationSource.id);
            } else {
                initiateNewReservation(moment(reservationSource.start));
            }
        }

        if (!prevReservation && reservation?.room_id && reservationSource) {
            getTimelineReservations(reservation.room_id, moment(reservationSource.start));
            this.authorizeUser();
        }

        if ((prevProps.reservationState.saving && !saving) || (prevProps.reservationState.deleting && !deleting)) {
            closeReservationForm();
        }

        if (formValues && prevFormValues) {
            if (!formValues.startdate.isSame(prevFormValues.startdate) || !formValues.enddate.isSame(prevFormValues.enddate)) {
                if (formValues.recurrence) {
                    const recurrence = JSON.parse(formValues.recurrence);

                    const updatedRecurrence = {
                        ...recurrence,
                        "start_time": {
                            ...recurrence.start_time,
                            time: moment(formValues.startdate).format(),
                        },
                        "end_time": {
                            ...recurrence.end_time,
                            time: moment(formValues.enddate).format(),
                        },
                    };

                    this.handleRecurrenceChange(JSON.stringify(updatedRecurrence));
                }
            }
        }
    }

    public componentWillUnmount(): void {

        const { resetReservation } = this.props;

        resetReservation();
    }

    public render(): JSX.Element {

        const { reservationLoading, roomsLoading, formValues, formErrors, saveReservation, removeReservation, getTimelineReservations, formClearSubmitErrors } = this.props;
        const { reservation, saving, deleting, timelineReservations } = this.props.reservationState;
        const { rooms } = this.props.roomState;
        const { forbidden } = this.state;

        return (
            <div className="reservation">
                <ReservationModal
                    reservation={ reservation }
                    reservationLoading={ reservationLoading }
                    saveReservation={ saveReservation }
                    savingReservation={ saving }
                    removeReservation={ removeReservation }
                    deletingReservation={ deleting }
                    getTimelineReservations={ getTimelineReservations }
                    timelineReservations={ timelineReservations }
                    recurrenceChange={ this.handleRecurrenceChange }
                    rooms={ rooms }
                    roomsLoading={ roomsLoading }
                    closeDialog={ this.handleCloseDialog }
                    formValues={ formValues }
                    formErrors={ formErrors }
                    forbidden={ forbidden }
                    formClearSubmitErrors={ formClearSubmitErrors }
                />
            </div>
        );
    }

    private handleCloseDialog(): void {

        const { closeReservationForm } = this.props;

        closeReservationForm();
    }

    private handleRecurrenceChange(recurrence: string): void {

        const { formChange } = this.props;

        formChange("reservation", "recurrence", recurrence);
    }

    private authorizeUser(): void {

        const { profile } = this.props.userState;
        const { reservation } = this.props.reservationState;

        if (profile) {
            const role = profile.data.users_accounts.find(a => a.account.id === profile.data.current_account_id).role.name;

            this.setState({
                forbidden: role === ERoles.user && reservation.user_id !== profile.data.id,
            });
        }
    }
}

export default connect<IStateProps, IDispatchProps, IProps, IAppState>(
    (state) => ({
        reservationState: state.reservation,
        reservationLoading: state.reservation.reservation === null,
        roomState: state.room,
        roomsLoading: state.room.rooms === undefined,
        userState: state.user,
        formValues: getFormValues("reservation")(state) as IReservation,
        formErrors: getFormSubmitErrors("reservation")(state),
    }),
    {
        getReservation: getReservationAction,
        getTimelineReservations: getTimelineReservationsAction,
        initiateNewReservation: initiateNewReservationAction,
        removeReservation: removeReservationAction,
        saveSourceInState: saveSourceInStateAction,
        saveReservation: saveReservationAction,
        resetReservation: resetReservationAction,
        getRooms: getRoomsAction,
        getUserProfile: getUserProfileAction,
        formChange: change,
        formClearSubmitErrors: clearSubmitErrors,
    },
)(Reservation);
