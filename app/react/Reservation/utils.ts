import moment, { Moment } from "moment";

/**
 * Returns an array of times in a 24 hour period
 */
export const hoursArray = Array.from(Array(24), (value, index) => {
    const hourOfDay = moment.utc(index * 3600 * 1000);
    return (hourOfDay.format("h") === "12")
                ? hourOfDay.format("hA")
                : hourOfDay.format("h:mm");
});

/**
 * Calculates the block position on the timeline based on the start and end date
 * and the track's grid size.
 *
 * @param startdate
 * @param enddate
 * @param grid
 */
export const calculateBlockPosition = (startdate: Moment, enddate: Moment, grid: number): { width: number; left: number } => {

    const duration = moment.duration(enddate.diff(startdate));
    const partitionSize = grid * 4;
    const left = moment.duration(startdate.format("HH:mm")).asHours() * partitionSize;
    const width = duration.asHours() * partitionSize;

    return {
        width,
        left,
    };
};
