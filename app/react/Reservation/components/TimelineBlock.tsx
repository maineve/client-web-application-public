import React, { Fragment } from "react";
import moment, { Moment } from "moment";
import { isEqual } from "lodash";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGripLinesVertical, faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";
import { IReservation } from "app/store/Reservation/types";
import { calculateBlockPosition } from "../utils";

interface IProps {
    grid: number;
    minWidth?: number;
    formValues: IReservation;
    timelineReservations: IReservation[];
    disabled: boolean;
    onChange: (startdate: Moment, enddate: Moment) => void;
}

interface IState {
    width: number;
    left: number;
    drag: boolean;
    conflict: boolean;
}

export class TimelineBlock extends React.Component<IProps, IState> {

    private mousedown = 0;
    private offset = 0;
    private blockWidth = 0;
    private trackWidth = 0;

    constructor(props: IProps) {
        super(props);

        this.state = {
            width: 0,
            left: 0,
            drag: false,
            conflict: false,
        };

        this.handleMouseDown = this.handleMouseDown.bind(this);
        this.handleMouseUp = this.handleMouseUp.bind(this);
        this.draggable = this.draggable.bind(this);
        this.resizeableL = this.resizeableL.bind(this);
        this.resizeableR = this.resizeableR.bind(this);
        this.blockPosition = this.blockPosition.bind(this);
        this.calculateTime = this.calculateTime.bind(this);
        this.checkForConflict = this.checkForConflict.bind(this);
    }

    public componentDidMount(): void {

        document.addEventListener("mousedown", this.handleMouseDown);
        document.addEventListener("mouseup", this.handleMouseUp);

        const blockPosition = this.blockPosition();

        this.setState({
            ...blockPosition,
        });

        this.checkForConflict();
    }

    public componentDidUpdate(prevProps: IProps, prevState: IState): void {

        const { timelineReservations } = this.props;
        const { startdate, enddate } = this.props.formValues;
        const { startdate: prevStartdate, enddate: prevEnddate } = prevProps.formValues;
        const { left, width, drag } = this.state;

        if (!startdate.isSame(prevStartdate) || !enddate.isSame(prevEnddate)) {
            const blockPosition = this.blockPosition();

            this.setState({
                ...blockPosition,
            });

            this.checkForConflict();
        }

        if (!isEqual(prevProps.timelineReservations, timelineReservations)) {
            this.checkForConflict();
        }

        if ((left !== prevState.left || width !== prevState.width) && drag) {
            this.calculateTime();
        }
    }

    public componentWillUnmount(): void {

        document.removeEventListener("mousedown", this.handleMouseDown);
        document.removeEventListener("mouseup", this.handleMouseUp);
    }

    public render(): JSX.Element {

        const { width, left, conflict } = this.state;
        const { disabled } = this.props;

        return (
            <div
                id="timeline-blk"
                className={ `timeline__blk ${ conflict && `timeline__blk--warn` } ${ disabled && `timeline__blk--disabled` }` }
                style={ { width, left } }
            >
                {
                    (conflict && width > 35)

                    &&

                    <div className="timeline__blk-warning">
                        <FontAwesomeIcon icon={ faExclamationTriangle } />
                    </div>
                }
                {
                    !disabled

                    &&

                    <Fragment>
                        <div id="timeline-blk-l" className="timeline__blk-resize timeline__blk-resize--l">
                            <FontAwesomeIcon icon={ faGripLinesVertical } />
                        </div>
                        <div id="timeline-blk-r" className="timeline__blk-resize timeline__blk-resize--r">
                            <FontAwesomeIcon icon={ faGripLinesVertical } />
                        </div>
                    </Fragment>
                }
            </div>
        );
    }

    private handleMouseDown(event: MouseEvent): void {

        const { disabled } = this.props;

        // Only accept left clicks
        if (event.button !== 0 || disabled) {
            return;
        }

        this.setState({
            drag: true,
        });

        const target = event.target as HTMLDivElement;
        const bodyTag = document.getElementsByTagName("body")[0];
        const timelineBlock = document.getElementById("timeline-blk");
        const timelineTrack = document.getElementById("timeline-blk-track");

        this.mousedown = event.x;
        this.offset = timelineBlock.offsetLeft;
        this.blockWidth = timelineBlock.clientWidth;
        this.trackWidth = timelineTrack.clientWidth;

        if (target.id === "timeline-blk") {
            document.addEventListener("mousemove", this.draggable);
            bodyTag.style.cursor = "ew-resize";
        }

        if (target.closest("#timeline-blk-l")) {
            document.addEventListener("mousemove", this.resizeableL);
            bodyTag.style.cursor = "col-resize";
        }

        if (target.closest("#timeline-blk-r")) {
            document.addEventListener("mousemove", this.resizeableR);
            bodyTag.style.cursor = "col-resize";
        }
    }

    private handleMouseUp(): void {

        document.removeEventListener("mousemove", this.draggable);
        document.removeEventListener("mousemove", this.resizeableL);
        document.removeEventListener("mousemove", this.resizeableR);

        document.getElementsByTagName("body")[0].style.cursor = "";

        this.setState({
            drag: false,
        });
    }

    private draggable(event: MouseEvent): void {

        const { grid } = this.props;

        const newLeftPosition = this.offset + (Math.round((event.x - this.mousedown) / grid) * grid);

        if (newLeftPosition >= 0 && (newLeftPosition + this.blockWidth) <= this.trackWidth) {
            this.setState({
                left: newLeftPosition,
            });
        } else {
            this.setState({
                left: newLeftPosition < 0 ? 0 : this.trackWidth - this.blockWidth,
            });
        }
    }

    private resizeableL(event: MouseEvent): void {

        const { grid, minWidth: minWidthProp } = this.props;

        const minWidth = minWidthProp ? minWidthProp : grid;
        const calculation = this.blockWidth + (Math.round((this.mousedown - event.x) / grid) * grid);
        const newWidth = calculation > minWidth ? calculation : minWidth;
        const newLeftPosition = this.offset - (newWidth - this.blockWidth);

        const block = document.getElementById("timeline-blk");

        if (newLeftPosition >= 0) {
            this.setState({
                left: newLeftPosition,
                width: newWidth,
            });
        } else {
            this.setState({
                width: block.offsetLeft + block.clientWidth,
                left: 0,
            });
        }
    }

    private resizeableR(event: MouseEvent): void {

        const { grid, minWidth: minWidthProp } = this.props;

        const minWidth = minWidthProp ? minWidthProp : grid;
        const calculation = this.blockWidth + (Math.round((event.x - this.mousedown) / grid) * grid);
        const newWidth = calculation > minWidth ? calculation : minWidth;

        const block = document.getElementById("timeline-blk");

        if (block.offsetLeft + newWidth <= this.trackWidth) {
            this.setState({
                width: newWidth,
            });
        } else {
            this.setState({
                width: this.trackWidth - block.offsetLeft,
            });
        }
    }

    private blockPosition(): { width: number; left: number } {

        const { grid } = this.props;
        const { startdate, enddate } = this.props.formValues;

        return calculateBlockPosition(startdate, enddate, grid);
    }

    private calculateTime(): void {

        const { left, width } = this.state;
        const { grid, onChange } = this.props;
        const { startdate } = this.props.formValues;

        const leftNumber = left / (grid * 4);
        const widthNumber = (left + width) / (grid * 4);

        const leftDuration = (Math.round(leftNumber * 4) / 4).toFixed(2);
        const widthDuration = (Math.round(widthNumber * 4) / 4).toFixed(2);

        const newStartDate = moment(startdate).startOf("day").add(leftDuration, "hour");
        const newEndDate = moment(startdate).startOf("day").add(widthDuration, "hour");

        onChange(newStartDate, newEndDate);
    }

    private checkForConflict(): void {

        const { formValues, timelineReservations } = this.props;

        const conflict = timelineReservations.some(timelineReservation => {
            return (
                formValues.startdate.isBefore(timelineReservation.enddate) &&
                formValues.enddate.isAfter(timelineReservation.startdate)
            );
        });

        this.setState({
            conflict,
        });
    }
}
