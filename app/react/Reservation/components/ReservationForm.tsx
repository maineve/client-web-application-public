import React from "react";
import moment from "moment";
import { IReservation } from "app/store/Reservation/types";
import { default as Timeline } from "../containers/Timeline";
import { IRoom } from "app/store/Room/types";
import { reduxForm, InjectedFormProps, Field, FormErrors } from "redux-form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faSync, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { ConfirmModal } from "../../../react/Generic/components/ConfirmModal";
import { removeReservationAction, getTimelineReservationsAction } from "app/store/Reservation/actions";
import { Recurrence } from "../../Recurrence/containers/Recurrence";
import { SelectField } from "../../Generic/wrappers/SelectField";
import { Tooltip } from "../../Anon/components/Tooltip";
import { mapReactSelectOptions } from "../../../utils";

interface IProps {
    saving: boolean;
    deleting: boolean;
    forbidden: boolean;
    rooms: IRoom[];
    formValues: IReservation;
    formErrors: FormErrors<IReservation>;
    removeReservation: typeof removeReservationAction;
    getTimelineReservations: typeof getTimelineReservationsAction;
    recurrenceChange: (recurrence: string) => void;
    close: () => void;
}

type TProps = IProps & InjectedFormProps<IReservation, IProps>;

const ReservationForm: React.FC<TProps> = (props) => {

    const {
        close,
        removeReservation,
        rooms,
        formValues,
        formErrors,
        handleSubmit,
        saving,
        deleting,
        recurrenceChange,
        getTimelineReservations,
        forbidden,
    } = props;

    const [confirmDeleteOpen, setConfirmDeleteOpen] = React.useState(false);
    const [recurrenceModalOpen, setRecurrenceModalOpen] = React.useState(false);

    const ref = React.useRef(null);

    const isRecurrence = formValues && (formValues.recurrence_id || formValues.original_id);
    const isNewReservation = formValues && (!formValues.id && !formValues.recurrence_id);

    const roomSelectOptions = mapReactSelectOptions<IRoom>(rooms, `name`, `id`);

    React.useEffect(() => {

        const handleClick = (event: MouseEvent): void => {

            const node = event.target as HTMLElement;

            if (node.id !== "repeat-link" && node.id !== "add-repeat-link" && ref.current) {
                if (!ref.current.contains(event.target)) {
                    setRecurrenceModalOpen(false);
                }
            }
        };

        window.addEventListener("click", handleClick);

        return (): void => {
            window.removeEventListener("click", handleClick);
        };
    }, []);

    const handleRoomChange = (event: React.ChangeEvent<HTMLInputElement>, id: number): void => {
        console.log(id);
        getTimelineReservations(id, moment(formValues.startdate));
    };

    const handleToggleRecurrenceModal = (): void => {
        setRecurrenceModalOpen(!recurrenceModalOpen);
    };

    const handleToggleDeleteModal = (): void => {
        setConfirmDeleteOpen(!confirmDeleteOpen);
    };

    const handleDeleteReservation = (deleteAll: boolean): void => {
        removeReservation(formValues, deleteAll);
        setConfirmDeleteOpen(false);
    };

    return (
        <form onSubmit={ handleSubmit }>
            <div className="reservation-form">
                <div className="reservation-form__title-section">

                    <Field
                        name="title"
                        component="input"
                        className="reservation-form__title-input"
                        disabled={ forbidden }
                        placeholder="Booking title &hellip;"
                    />

                    <div className="reservation-form__options">
                        <ul className="reservation-form__options-list">
                            <li className="reservation-form__options-item">
                                {
                                    isRecurrence || forbidden

                                    ?

                                        <span className="reservation-form__options-span">
                                            <FontAwesomeIcon
                                                icon={ faSync }
                                                className={ `reservation-form__repeat-icon` }
                                            />
                                        </span>

                                    :

                                        <a
                                            role="button"
                                            id="repeat-link"
                                            className="reservation-form__options-link"
                                            title="Repeat"
                                            onClick={ handleToggleRecurrenceModal }
                                        >
                                            <FontAwesomeIcon
                                                icon={ faSync }
                                                className={ `reservation-form__repeat-icon ${ (formValues && formValues.recurrence) && `reservation-form__repeat-icon--active` }` }
                                            />
                                        </a>
                                }
                            </li>
                            <li className="reservation-form__options-item">
                                {
                                    isNewReservation || forbidden

                                    ?

                                        <span className="reservation-form__options-span">
                                            <FontAwesomeIcon
                                                icon={ faTrashAlt }
                                            />
                                        </span>

                                    :

                                        <a role="button" className="reservation-form__options-link" title="Delete" onClick={ handleToggleDeleteModal }>
                                            <FontAwesomeIcon
                                                icon={ faTrashAlt }
                                            />
                                        </a>
                                }

                            </li>
                            <li className="reservation-form__options-item reservation-form__options-item--save">
                                <button
                                    className={ `reservation-form__options-btn ${ forbidden && `reservation-form__options-btn--disabled` }` }
                                    type="submit"
                                    disabled={ saving || forbidden }
                                >
                                    { saving ? `Saving...` : `Save` }
                                </button>

                                <Tooltip show={ !!Object.keys(formErrors).length } type="error" pointer="reservation">
                                    { formErrors[Object.keys(formErrors)[0]] }
                                </Tooltip>
                            </li>
                            <li className="reservation-form__options-item">
                                <a role="button" className="reservation-form__options-link reservation-form__options-link--close" title="Close" onClick={ close }>
                                    <FontAwesomeIcon
                                        icon={ faTimes }
                                    />
                                </a>
                            </li>
                        </ul>
                    </div>

                    {
                        recurrenceModalOpen

                        &&

                        <div className="reservation-form__recurrence" ref={ ref }>
                            <Recurrence
                                startdate={ moment(formValues.startdate).format() }
                                enddate={ moment(formValues.enddate).format() }
                                reservation={ formValues }
                                recurrence={ formValues.recurrence && JSON.parse(formValues.recurrence) }
                                updateAngularScope={ recurrenceChange }
                                close={ handleToggleRecurrenceModal }
                            />
                        </div>
                    }
                </div>

                <div className="reservation-form__timeline">
                    <Timeline disabled={ forbidden } />
                </div>

                <div className="reservation-form__details">
                    <div className="reservation-form__info">

                        <div className="reservation-form__info-section">
                            <div className="reservation-form__info-inner">
                                <label className="reservation-form__info-label">Time</label>
                                { formValues && formValues.startdate.format("h:mm a") } to { formValues && formValues.enddate.format("h:mm a") }
                            </div>
                        </div>

                        <div className="reservation-form__info-section">
                            <div className="reservation-form__info-inner">
                                <label className="reservation-form__info-label">Room</label>
                                <Field
                                    name="room_id"
                                    options={ roomSelectOptions }
                                    component={ SelectField as "input" & typeof SelectField }
                                    onChange={ handleRoomChange }
                                    disabled={ forbidden }
                                />
                            </div>
                        </div>

                        <div className="reservation-form__info-section">
                            <div className="reservation-form__info-inner">
                                <label className="reservation-form__info-label">Date</label>
                                { formValues && formValues.startdate.format("dddd, MMMM D, YYYY") }
                            </div>
                        </div>

                    </div>

                    <div className="reservation-form__comments">
                        <label htmlFor="reservation-comments">Comments &amp; Details</label>
                        <Field
                            id="reservation-comments"
                            name="details"
                            component="textarea"
                            disabled={ forbidden }
                        />
                    </div>
                </div>
            </div>

            {
                confirmDeleteOpen

                &&

                <ConfirmModal
                    confirmText={ `Delete` }
                    toggleText={ `Delete All` }
                    confirm={ handleDeleteReservation }
                    cancel={ handleToggleDeleteModal }
                    showToggle={ formValues.original_id !== null || formValues.recurrence_id !== null }
                >
                    Would you like to delete this booking? This action cannot be undone.
                </ConfirmModal>
            }

            {
                deleting

                &&

                <div>
                    <h1>Deleting &hellip;</h1>
                </div>
            }
        </form>
    );
};

export default reduxForm<IReservation, IProps>({
    form: `reservation`,
})(ReservationForm);
