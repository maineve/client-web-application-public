import React from "react";
import { IReservation } from "app/store/Reservation/types";
import moment, { Moment } from "moment";

interface IProps {
    label: string;
    index: number;
    formValues: IReservation;
    disabled: boolean;
    onChange: (startdate: Moment, enddate: Moment) => void;
}

export const TimelineSection: React.FC<IProps> = ({
    formValues,
    label,
    index,
    onChange,
    disabled,
}) => {

    const handleSectionClick = (): void => {

        if (disabled) {
            return;
        }

        const { startdate, enddate } = formValues;
        const duration = moment.duration(enddate.diff(startdate));
        const newStartDate = moment(formValues.startdate).set({ hour: index }).startOf("hour");

        let newEndDate = moment(newStartDate).add(duration);

        if (moment(newEndDate).isAfter(startdate, "day")) {
            newEndDate = moment(newEndDate).startOf("day");
        }

        onChange(newStartDate, newEndDate);
    };

    return (
        <div className={ `timeline__section ${ disabled && `timeline__section--disabled` }` }>
            <div className="timeline__section-header">{ label }</div>
            <div className="timeline__section-time" onClick={ handleSectionClick } />
        </div>
    );
};
