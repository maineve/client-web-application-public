import React from "react";
import moment from "moment";
import { IReservation } from "../../../store/Reservation/types";
import { calculateBlockPosition } from "../utils";

interface IProps {
    grid: number;
    reservation: IReservation;
}

export const TimelineCompareBlock: React.FC<IProps> = ({ reservation, grid }) => {

    const start = moment.utc(reservation.startdate);
    const end = moment.utc(reservation.enddate);

    const blockPosition = calculateBlockPosition(start, end, grid);

    return (
        <div
            className="timeline__blk-compare"
            style={ { left: blockPosition.left, width: blockPosition.width } }
        />
    );
};
