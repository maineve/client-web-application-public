import React from "react";
import { SubmissionError, FormErrors, clearSubmitErrors } from "redux-form";
import { IReservation } from "../../../store/Reservation/types";
import { default as ReservationForm } from "../components/ReservationForm";
import { IRoom } from "app/store/Room/types";
import { saveReservationAction, removeReservationAction, getTimelineReservationsAction } from "app/store/Reservation/actions";
import "../../../assets/images/icons/book-1.svg";

interface IProps {
    reservation: IReservation;
    timelineReservations: IReservation[];
    reservationLoading: boolean;
    forbidden: boolean;
    rooms: IRoom[];
    roomsLoading: boolean;
    formValues: IReservation;
    formErrors: FormErrors<IReservation>;
    saveReservation: typeof saveReservationAction;
    savingReservation: boolean;
    removeReservation: typeof removeReservationAction;
    deletingReservation: boolean;
    getTimelineReservations: typeof getTimelineReservationsAction;
    formClearSubmitErrors: typeof clearSubmitErrors;
    recurrenceChange: (recurrence: string) => void;
    closeDialog: () => void;
}

export const ReservationModal: React.FC<IProps> = (props) => {

    const {
        closeDialog,
        reservationLoading,
        reservation,
        saveReservation,
        savingReservation,
        removeReservation,
        deletingReservation,
        getTimelineReservations,
        formValues,
        formErrors,
        rooms,
        roomsLoading,
        recurrenceChange,
        forbidden,
        timelineReservations,
        formClearSubmitErrors,
    } = props;

    React.useEffect(() => {

        const handleClick = (): void => {

            formClearSubmitErrors("reservation");
        };

        window.addEventListener("click", handleClick);

        return (): void => {
            window.removeEventListener("click", handleClick);
        };
    }, [formClearSubmitErrors]);

    const handleSubmit = (values: IReservation): void => {

        if (!values.title) {
            throw new SubmissionError({ "title": "Title field is required." });
        }

        if (!values.room_id) {
            throw new SubmissionError({ "room_id": "Choose a room for your reservation." });
        }

        const conflict = timelineReservations.some(timelineReservation => {
            return (
                values.startdate.isBefore(timelineReservation.enddate) &&
                values.enddate.isAfter(timelineReservation.startdate)
            );
        });

        if (conflict) {
            throw new SubmissionError({ "base": "The room is currently not available for the requested time." });
        }

        saveReservation(values);
    };

    const handleClose = (): void => {

        closeDialog();
    };

    return (
        <div className="reservation-modal">
            <div className="reservation-modal__icon">
                <img className="reservation-modal__icon-img" src="assets/book-1.svg" width="40" height="40" />
            </div>
            {
                (reservationLoading || roomsLoading)

                ?

                    <div className="reservation-modal__loader">
                        <div className="loader ball-pulse">
                            <div />
                            <div />
                            <div />
                        </div>
                    </div>

                :

                    <ReservationForm
                        onSubmit={ handleSubmit }
                        rooms={ rooms }
                        close={ handleClose }
                        saving={ savingReservation }
                        formValues={ formValues }
                        formErrors={ formErrors }
                        initialValues={ reservation }
                        removeReservation={ removeReservation }
                        deleting={ deletingReservation }
                        getTimelineReservations={ getTimelineReservations }
                        recurrenceChange={ recurrenceChange }
                        forbidden={ forbidden }
                    />
            }
        </div>
    );
};
