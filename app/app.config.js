'use strict';

/**
 *  Config function for Angular
 */
export default function config($httpProvider, RestangularProvider, ngDialogProvider, __env) {

    'ngInject';

    /**
     *  Add custom $http interceptor
     */
    $httpProvider.interceptors.push(($q) => {
        return {
            'request': (config) => {
                config.headers['Authorization'] = localStorage.getItem('token');
                return config;
            },

            'response': (response) => {
                return response;
            }
        };
    });

    /**
     *  Restangualar configuration
     */
    RestangularProvider.setBaseUrl(__env.apiUrl);

    /**
     *  Request Interceptor
     */
    RestangularProvider.addRequestInterceptor((element, operation, what, url) => {
        var data = { data: element };
        return data;
    });

    /**
     *  Response Interceptor
     */
    RestangularProvider.addResponseInterceptor((data, operation, what, url, response, deferred) => {
        return data.data;
    });

    /**
     *  NG Dialog configuration
     */
    ngDialogProvider.setDefaults({
        className: 'modal',
        showClose: false,
        closeByDocument: false,
    });

}
