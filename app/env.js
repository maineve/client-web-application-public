(function (window) {
    // Set object
    window.__env = window.__env || {};

    // Set Environment
    window.__env.environment = 'development';

    // API url
    window.__env.apiUrl = 'http://localhost:3000';
    window.__env.authUrl = 'http://localhost:4000';

    // Stripe
    window.__env.stripePublishableKey = 'pk_test_63RqBzQgOluYakV3Ka95RlxI';
}(this));
